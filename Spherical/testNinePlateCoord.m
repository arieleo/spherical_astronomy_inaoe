function [Pos, E]= testNPlateCoordinates(realPos, plateCord, C)

xi= realPos(:,1);
eta= realPos(:,2);

xR= plateCord(:,1);
yR= plateCord(:,2);

n= length(C);

xiB= C(1)*xR + C(2)*yR + C(3) + C(5)*xR + C(6)*yR + C(7)*xR.^2 + C(8)*xR.*yR + C(9)*xR.*(xR.^2 + yR.^2);
etaB=-C(2)*xR + C(1)*yR + C(4) + C(5)*xR + C(6)*xR - C(5)*yR + C(7)*xR.*yR + C(8)*yR.^2 + C(9)*yR.*(xR.^2 + yR.^2);



% N=-1;
% con=1;
% while(n>0)
%     n=n-con;
%     N=N+1;
%     con=con+1;
% end
% 
% x=zeros(size(xi));
% y=zeros(size(eta));
% con=1;
% for ii=0: N
%     g= linspace(ii,0,ii+1);
%     g2= linspace(0,ii,ii+1);
%     for jj=1: length(g)
%         x=x+(((xi.^(g(jj))).*(eta.^(g2(jj))))*C(con));
%         y=y+(((eta.^(g(jj))).*(xi.^(g2(jj))))*C(con));
%         con=con+1;
%     end
%     
% end
% 
% x= -(x-xi);
% y= -(y-eta);

E1= sqrt(mean((xi-xiB).^2));
E2= sqrt(mean((eta-etaB).^2));
E3= sqrt(E1.^2 + E2.^2);

E   = [E1 E2 E3]; % Using Root mean square error (RMSE) for Right ascension, Declination and general error.
Pos = [xiB etaB]; % Estimated Right ascension and Declination according to selected plate model.