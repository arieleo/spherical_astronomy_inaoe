function [NPOS]= correctZernike(Zx, Zy, sz, center, POS);

NPOS= zeros(size(POS));

POS(:,1)= POS(:,1)+center(1);
POS(:,2)= POS(:,2)+center(2);

Zersz= size(Zx);

facx= Zersz(1)/ sz(1);
facy= Zersz(2)/ sz(2);

indX= round(POS(:,1)*facx);
indY= round(POS(:,2)*facy);

for i=1: length(indX)
    if (indY(i)<=0)
        indY(i)=1;
    elseif (indY(i)>Zersz(1))
        indY(i)= Zersz(1);
    end
    
    if (indX(i)<=0)
        indX(i)=1;
    elseif (indX(i)>Zersz(2))
        indX(i)= Zersz(2);
    end
    
    NPOS(i,1)= POS(i,1) - Zx(indY(i), indX(i));
    NPOS(i,2)= POS(i,2) - Zy(indY(i), indX(i));
end

NPOS(:,1)= NPOS(:,1)-center(1);
NPOS(:,2)= NPOS(:,2)-center(2);
end