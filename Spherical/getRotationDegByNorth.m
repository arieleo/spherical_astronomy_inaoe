function [ A ] = getRotationDegByNorth( U,V )

O = [0,0];
a = dist2dots(U,V);
b = dist2dots(O,U);
c = dist2dots(O,V);
uC = definePlace(U);
vC = definePlace(V);

cosA = (b^2+c^2-a^2)/(2*b*c);
A =  acos(cosA);

interior = false;
interior = interior || (uC == 1 && vC == 2);
interior = interior || (uC == 2 && vC == 3);
interior = interior || (uC == 3 && vC == 4);
interior = interior || (uC == 4 && vC == 1);


exterior = false;
exterior = exterior || (uC == 4 && vC == 3);
exterior = exterior || (uC == 3 && vC == 2);
exterior = exterior || (uC == 2 && vC == 1);
exterior = exterior || (uC == 1 && vC == 4);


if exterior
    A = 6.2832 - A;
end







end

