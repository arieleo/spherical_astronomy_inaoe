function [ R,D ] = str2RD( input_args )

P = strsplit(input_args);

rahr = str2double(P{1});
ramin = str2double(P{2});
raseg = str2double(P{3});

decdeg = str2double(P{4});
decmin = str2double(P{5});
decseg = str2double(P{6});

R = (rahr *15) + (ramin *0.2500) + (raseg * 0.004166666666667);
D = (abs(decdeg) + (decmin / 60) + (decseg / 3600))*sign(decdeg);



end

