function [ alOrigenRotados ] = functionRotacion( xs,ys,centro,theta)
%functionRotacion
%   Para la rotacion de un conjunto de puntos con respecto al punto central
%   O al que se requiera rotar tantos grados que se necesite

n = length(xs);
%Punto central
xc = centro(1);
yc = centro(2);

%Puntos al centro, se necesitan 3 dimenciones para la
%Multiplicacion de la matriz
alCentro = ones(3,n);
alCentro(1,:) = (-1 * xc) + xs;
alCentro(2,:) = (-1 * yc) + ys;

%Matriz de rotacion
tform = affine2d([cosd(theta) -sind(theta) 0;
    sind(theta)  cosd(theta) 0;
    0            0           1]);
%Puntos al centro y multiplicados por la matriz de rotacion
centroRotada = (tform.T * alCentro);

alOrigenRotados = ones(3,n);
alOrigenRotados(1,:) = centroRotada(1,:)+xc;
alOrigenRotados(2,:) = centroRotada(2,:)+yc;


end

