function [E, Pos]= testPlateModels(plateCord, realPos, C, f, center)

x= plateCord(:,1);
y= plateCord(:,2);

n= length(C);

N=-1;
con=1;
while(n>0)
    n=n-con;
    N=N+1;
    con=con+1;
end

xi=zeros(size(x));
eta=zeros(size(y));
con=1;
for ii=0: N
    g= linspace(ii,0,ii+1);
    g2= linspace(0,ii,ii+1);
    for jj=1: length(g)
        xi=xi+(((x.^(g(jj))).*(y.^(g2(jj))))*C(con));
        eta=eta+(((y.^(g(jj))).*(x.^(g2(jj))))*C(con));
        con=con+1;
    end
    
end

xi= -(xi-x);
eta= -(eta-y);

[AR, DE]= getSkyPosition(f, center, [xi eta]);

E1= sqrt(mean((realPos(:,1)-AR).^2));
E2= sqrt(mean((realPos(:,2)-DE).^2));
E3= sqrt(E1.^2 + E2.^2);

E   = [E1 E2 E3]; % Using Root mean square error (RMSE) for Right ascension, Declination and general error.
Pos = [AR DE]; % Estimated Right ascension and Declination according to selected plate model.


