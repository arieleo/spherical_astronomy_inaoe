function [Pos, E]= testToPlateCoordinates(realPos, plateCord, C, f, center)

xi= realPos(:,1);
eta= realPos(:,2);

xR= plateCord(:,1);
yR= plateCord(:,2);


n= length(C);

switch n
    case 4 % Four plate model
        x= -(C(1)-1)*xi -C(2)*eta -C(3);
        y= C(2)*xi -(C(1)-1)*eta -C(4);
    case 6 % Six plate model
        x=  C(1)*xi + C(2)*eta + C(3) + C(5)*xi + C(6)*eta;
        y= C(1)*eta - C(2)*xi + C(4) + C(5)*eta + C(6)*xi;
    case 10 % Ten plate model
        x  = -(C(1) + C(2)*xi + C(3)*eta + C(4)*xi.*xi + C(5)*xi.*eta + C(6)*eta.*eta + C(7)*xi.*xi.*xi + C(8)*xi.*xi.*eta + C(9)*xi.*eta.*eta + C(10)*eta.*eta.*eta - xi);
        y = -(C(1) + C(2)*eta + C(3)*xi + C(4)*eta.*eta + C(5)*xi.*eta + C(6)*xi.*xi + C(7)*eta.*eta.*eta + C(8)*eta.*eta.*xi + C(9)*xi.*xi.*eta + C(10)*xi.*xi.*xi - eta);
    otherwise
        disp('not implemented yet');
    
end

E1= sqrt(mean((xR-x).^2));
E2= sqrt(mean((yR-y).^2));
E3= sqrt(E1.^2 + E2.^2);

E   = [E1 E2 E3]; % Using Root mean square error (RMSE) for Right ascension, Declination and general error.
Pos = [x y]; % Estimated Right ascension and Declination according to selected plate model.


