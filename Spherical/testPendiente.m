clc;
clear;

% Polaris
polaris = '01 48 47.78 +89 01 43.6';
sigOct = '20 15 03.45 -89 08 18.4';


% Importamos los datos de las im�genes y los metemos a las ...
% variables AR, DEC, X y Y. Tambi�n definimos la escala de placa ps
A=importdata('plate_files/AC4943Normal');

[A(51,1),A(51,2)] = str2RD(polaris);
[A(52,1),A(52,2)] = str2RD(sigOct);

A(51,3)=0;
A(51,4)=0;
A(52,3)=0;
A(52,4)=0;


DRA=A(:,1);
DDE=A(:,2);
X = A(:,3);
Y = A(:,4);


DRA=deg2rad(DRA);
DDE=deg2rad(DDE);
focal_length= 2158.8;
centerIndex = 21;
ppmm= 63.0417;
ppmm2= 63.125;

theta = deg2rad(0);
figure(1);
hold on;
for k=1:length(theta)
    
    
    center = [DRA(centerIndex),DDE(centerIndex)];
    centerXY = [X(centerIndex) Y(centerIndex)];
    
    [Dxi,Deta]= getStandardCoordinates(focal_length, center, [DRA DDE]);
    tform = affine2d([ cos(-theta(k)), -sin(-theta(k)), 0;
        sin(-theta(k)), cos(-theta(k)), 0;
        0, 0, 1]);
    trasl= [Dxi(centerIndex)-centerXY(1), Deta(centerIndex)-centerXY(2)];
    
    X= X+trasl(1);
    Y= Y+trasl(2);
    
    [X,Y] = transformPointsForward(tform,X,Y);
    
    plot(Dxi,Deta,'pr');
    %     plot(X,Y,'pb');
    
    plot([Dxi(centerIndex),Dxi(51)]...
        ,[Deta(centerIndex),Deta(51)],'b--');
    
    Ym = Deta(51) - X(centerIndex);
    Xm = Dxi(51) - Y(centerIndex);
    
    m = Ym / Xm;
    
    grados = rad2deg(atan(m));
    display(m);
    display(grados);
    
    
end
axis square;
axis equal;
grid minor;













