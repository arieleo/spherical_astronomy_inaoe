%Author: Andrea Burgos
%February 2018

%Read the magnitude of a star from the astronomical database SIMBAD 
%Assumptions: There is not negative magnitudes
%             All the input files are the only in the carpet that ends in xls 
%             Make shure is in the matlab path the carpet ls..
%Comments: When the values of Ra or Dec are NaN, in the new file will be 0
%Inputs: File with Star names, Ra and Dec
%Output: Magnitudes U B V R I G J H K

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Get the files to be processed
% command = 'ls ..'; %navigate to that carpet
% [status,cmdout] = system(command); %cmdout is the file list
% result = textscan( cmdout, '%s', 'delimiter', {'\n','~'} );%to split the lines
% fileList = result{1};
list=dir('../*.xls')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ops=['UBVRIGJHK']; %where there is no magnitude theres a 0 but that does not mean that the magnitude is 0
head={'star','U','B','V','R','I','G','J','H','K','Ra','Dec','x','y'};
for f=1:length(list)
    clear b;
    clear temp3;
    clear star;
    clear num;
    %if not(isempty(regexp(fileList{f,1},'.xls$')))
       % name=fileList{f,1};
        name=list(f,1).name
        %name='AC1212_USNO.xlsx';

        [num,star]=xlsread(name);
        temp3=head;
        %star='HD 46265;
        for i=3:length(star) %The first star is in the third position in the file (because of headers)  
            URL=strcat('http://simbad.u-strasbg.fr/simbad/sim-basic?Ident=',star(i,2),'&submit=SIMBAD+search');
            search=webread(URL{1,1}); %Read content from RESTful web service
            for j=1:length(ops)
                [startIndex,endIndex]=regexp(search,strcat(ops(j),'\s+[0-9]*[.])?[0-9]+ '));%looks for the magnitude in variable search
                a=search(startIndex+7:endIndex);
                if not(isempty(a))
                    b(i-2,j)=str2num(a);%creates the content of the file (magnitudes)
                end
            end
            temp3(i-1,1)=star(i,2);        
        end 
        temp3(2:length(temp3),2:10)=num2cell(b);
        [s1,s2]=size(num);

        %get the values for Ra and Dec
        
        if length(num)==length(star)
            temp3(2:length(temp3),11:12)=num2cell(num(3:length(num),11:12));
            if s2==15
                temp3(2:length(temp3),13:14)=num2cell(num(3:length(num),14:15));
            end
        else
            
            temp3(2:length(temp3),11:12)=num2cell(num(:,11:12));
            if s2==15
                temp3(2:length(temp3),13:14)=num2cell(num(:,14:15));
            end
        end
        
        outputName=strcat(name,'magnitudes.csv');%save in a file
            
        cell2csv(outputName,temp3,',')
        fclose('all')
    %end
end  
