clc;
clear all;


validationFolds= 10; % Number of folds for validation test. 48 and 49 examples for .dat files
plateModel= 6; % Degree of equation used.

option=1; % Using different leastsquare implementations. Default 1
tunevalue= 1; % Default= 1.

focal_length= 2158.8; %focal length value of Camera Schmidt in mm. 2158.8 +- 1.4

name='AC0359.txt(ep-B1950,eq-1947.0466,SRef-FK4).xlsmagnitudes.csv';
plate_name = strsplit(name,'.');
values=csvread(name,1,1); %file that contain RA DEC x,y and D
ord=2; %limit of the power serie 
[values falses]= eraseNAN(values);
%add ids of each star, this will help to identify which stars are then discarded
ids=[1:length(values)];
ids=ids';
values=horzcat(ids,values);
values2=values;
id=values(:,1);

%set the center in the center of the plate
for i=1:length(values)
    if(values(i,13)<=0)
        values(i,13)=values(i,13)-1;
    end
    if(values(i,14)<=0)
        values(i,14)=(values(i,14)-1)*(-1);
    end
end
%pixeles to mmm
values(:,13:14)=(values(:,13:14)/60)-100; % in mm
% AR & DE in rad
values(:,11:12)=deg2rad(values(:,11:12));

[CAR CDE]= findCenter(values(:,11), values(:,12), values(:,13), values(:,14), [0 0]);% Central point of plate.

[XI ETA]= getStandardCoordinates(focal_length, [CAR CDE], [values(:,11) values(:,12)]);    %Ideal positions for train Constants plate

[xmmn, ymmn, bestAng, min_e]= getFirstAdj(values(:,13), values(:,14), XI, ETA, 0.1);  
values(:,13)=xmmn;
values(:,14)=ymmn;



E1= sqrt(mean((XI - xmmn).^2));
E2= sqrt(mean((ETA - ymmn).^2));
E3= sqrt(E1.^2 + E2.^2) % Initial Error.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% First Plot Positions

figure(1)

scatter(xmmn, ymmn, 'r*');
hold on;
% scatter(xmmn, ymmn, 'r*');
% figure(2)
scatter(XI, ETA, 'b*');
title('With best angle')
% scatter(Xn, Yn, 'b*');
xlim([-100 100])
ylim([-100 100])
hold off;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PLot Quiver
[fx0, fy0]= makeQuiver_2([XI ETA], [xmmn ymmn], [100,100], ' ', 'With best Angle', 'Error in mm', 2, [200 200]); %12000/60=200 D
               



c = cvpartition(length(values(:,1)),'KFold', validationFolds);

numPerFold= floor(length(values(:,1))/validationFolds); % Number of observations for each Fold.
histConstants= zeros(validationFolds, sum(linspace(1,plateModel+1,plateModel+1)));      %History for Constant plate model.
histError= zeros(validationFolds, 3);                   %History for folds error.
all_pos=[];
for ii=1: validationFolds
    data3= values;
    
    prev=0;
    if ii>1
        prev= sum(c.TestSize(1:ii-1));
    end
    
    objTest= [prev+1: prev+c.TestSize(ii)];
    test= data3(objTest,:);                             %Testing set
    
    data3(objTest, :)=[];                               %Training set
    
    TRA = test(:,11);                                    %Right ascension for tests
    TDE = test(:,12);                                    %Declination for tests
    [Txi Teta]= getStandardCoordinates(focal_length, [CAR CDE], [TRA TDE]); 
    Tx= test(:,13);                                      %Plate x positions for test
    Ty= test(:,14);                                      %Plate y positions for test
    for j=1:c.TestSize(ii)
        Td(j,1)=sum(test(j,2:10))/sum(test(j,2:10) ~= 0); 
    end
    
    DRA = data3(:,11);                                   %Right ascension for train Constants plate
    DDE = data3(:,12);                                 %Declination for train Constants plate
    [Dxi Deta]= getStandardCoordinates(focal_length,[CAR CDE], [DRA DDE]);    %Ideal positions for train Constants plate
    Dx= data3(:,13);                                     %Plate x positions for train Constants plate.
    Dy= data3(:,14);                                     %Plate y positions for train Constants plate.
    
    [comb]=getSummations(0,0,0,ord);%get the number of combinations
    
    A= zeros(length(Dx)*2, length(comb)+6);
    b= zeros(length(Dx)*2, 1);
    for j=1:length(Dx)
        Dd(j,1)=sum(data3(j,2:10))/sum(data3(j,2:10) ~= 0); 
        [comb,Dv(j,:)]=getSummations(Dx(j,:),Dy(j,:),Dd(j,1),ord);
        [ss,ss2]=size(Dv);
        %
        for k=1:ss2
            DVx(j,k)=(Dv(j,k)*cos(atan2(Dy(j,:),Dx(j,:))));
            DVy(j,k)=(Dv(j,k)*sin(atan2(Dy(j,:),Dx(j,:))));

        end
       A=[A;1,Dx(j,:),Dy(j,:),0,0,0,(-DVx(j,:));0,0,0,1,Dx(j,:),Dy(j,:),(-DVy(j,:))];
       b=[b;Dxi(j,:);Deta(j,:)];
    end

    [C]= leastSquares(A, b);
    
    
    
    
    C= NModelConstantPlate([Dxi Deta], [Dx Dy], plateModel, option, tunevalue);
    
    [E POS]= testNPlateModels2([Tx Ty], [Txi Teta], C, focal_length, [CAR CDE]); %RMSE for estimated positions (AR, DE) in each Fold.
    all_pos= [all_pos; POS];
    histConstants(ii, :)= C;
    histError(ii, :)= E;

end

mesError= mean(histError);

meanConst= mean(histConstants);
[E POS]= testNPlateModels2([xmmn ymmn], [XI ETA], meanConst, focal_length, [CAR CDE]); %RMSE for estimated positions (AR, DE) in each Fold.
E

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Second Plot Positions

figure(3)

scatter(POS(:,1), POS(:,2), 'g*');
hold on;
title('With adjustment')
% scatter(xmmn, ymmn, 'r*');
% figure(2)
scatter(XI, ETA, 'b*');
% scatter(Xn, Yn, 'b*');
xlim([-100 100])
ylim([-100 100])
hold off;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% QUiver
[fx, fy]= makeQuiver_2([XI ETA], [POS(:,1) POS(:,2)], [100,100], ' ', 'With adjustment', 'Error in mm', 4, [200 200]); %12000/60=200 D
                
