function [CAR CDE] = findCenter(AR, DE, Xp, Yp, point)

%  AR: Set of Right ascension values in plate.
%  DE: Set of declination values in plate.
%  Xp: Set of X pixel coordinates in plate points.
%  Yp: Set of Y pixel coordinates in plate points.
%  point: point in pixel coordinates to find (x, y).

% data= importdata('plate_files/AC0358');
% AR= data(:,1);
% DE= data(:,2);
% Xp= data(:,4);
% Yp= data(:,5);

fitAR= fit([Xp,Yp],AR,'poly22');
fitDE= fit([Xp,Yp],DE,'poly22');

CAR= fitAR(point(1), point(2));
CDE= fitDE(point(1), point(2));

% [a]= find(Xp==min(Xp));
% [b]= find(Xp==max(Xp));
% 
% dx= Xp(b)-Xp(a);
% dx_a= AR(b)-AR(a);
% dx_d= DE(b)-DE(a);
% 
% app_x= dx_a/dx
% dpp_x= dx_d/dx;
% 
% [c]= find(Yp==min(Yp));
% [d]= find(Yp==max(Yp));
% 
% dy= Yp(d)-Yp(c);
% dy_a= AR(d)-AR(c);
% dy_d= DE(d)-DE(c);
% 
% app_y= dy_a/dy;
% dpp_y= dy_d/dy;
% 
% app= (app_x + app_y)/2;
% dpp= (dpp_x + dpp_y)/2;
% 
% CAR= 6000* app;
% CDE= 6000* dpp;

% % sqrt(sum((AR-CAR).*(AR-CAR)))
% % sqrt(sum((DE-CDE).*(DE-CDE)))
%%%
end