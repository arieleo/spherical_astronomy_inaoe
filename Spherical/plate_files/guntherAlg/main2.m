clear all; 
close all;
%Andrea Burgos Madrigal
%March 2018
%Get the constants


%D : the magnitudes promediated
name='AC0359.txt(ep-B1950,eq-1947.0466,SRef-FK4).xlsmagnitudes.csv';
plate_name = strsplit(name,'.');
values=csvread(name,1,1); %file that contain RA DEC x,y and D
focal_length= 2159.1535353535; %focal length value of Camera Schmidt in mm. 2158.8 +- 1.4
ord=2; %limit of the power serie 

validationFolds= 10; % Number of folds for validation test
numPerFold= floor(length(values(:,1))/validationFolds); % Number of observations for each Fold.

%init
%histError= zeros(validationFolds, 3);                   %History for folds error.


%add ids of each star, this will help to identify which stars are then discarded
ids=[1:length(values)];
ids=ids';
values=horzcat(ids,values);
values2=values;
%eliminate the rows with NaN values
for i=1:length(values2)
    if (isnan(values2(i,14)))||(isnan(values2(i,13)))
        id=find(values(:,1)==i);
            values(id,:)=[];
    end
end

id=values(:,1);


%set the center in the center of the plate
for i=1:length(values)
    if(values(i,13)<=0)
        values(i,13)=values(i,13)-1;
    end
    if(values(i,14)<=0)
        values(i,14)=(values(i,14)-1)*(-1);
    end
end
%pixeles to mmm
values(:,13:14)=(values(:,13:14)/60)-100; % in mm
% AR & DE in rad
values(:,11:12)=deg2rad(values(:,11:12));
%values(:,12)=deg2rad(values(:,12));


%values(:,14)=(values(:,14)/60)-100;



[Car Cde]= findCenter(values(:,11), values(:,12), values(:,13), values(:,14), [0 0]);% Central point of plate.

[IX IY]= getStandardCoordinates(focal_length, [Car Cde], [values(:,11) values(:,12)]);    %Ideal positions for train Constants plate

[xmmn, ymmn, bestAng, min_e]= getFirstAdj(values(:,13), values(:,14), IX, IY, 0.1);  
values(:,13)=xmmn;
values(:,14)=ymmn;

<<<<<<< HEAD
 figure(1)
 scatter(IX, IY, 'b*');
 hold on;
 scatter(xmmn, ymmn, 'r*');
 
 title('With best Angle');
 %xlim([-100 100])
 %ylim([-100 100])
 hold off

 error(1,1)=sqrt(mean((xmmn-IX).^2)); 
 error(2,1)=sqrt(mean((ymmn-IY).^2)); 
 error(3,1)=sqrt(error(1,1) + error(2,1)); 
 %%%[fx0, fy0]= makeQuiver_2([IX IY], [xmmn ymmn], [100,100], plate_name{1,1}, 'With best Angle', 'Error in mm', 2, [200 200]); %12000/60=200 D
                


% % % % % %----------------------------------get initial error----------------------
% % % % % Ix= values(:,13);  
% % % % % Iy= values(:,14);  
% % % % % 
% % % % % IRA = values(:,11);                                   %Right ascension for train Constants plate
% % % % % IDE = values(:,12);                                   %Declination for train Constants plate
% % % % % 
% % % % % %IX=IX-100;
% % % % % %IY=IY-100;
% % % % %  %-------------------------------------------------------------------------
% % % % % 
% % % % % 
% % % % % 
% % % % % 
% % % % % [Car Cde]= findCenter(values(:,11), values(:,12), xmmn, ymmn, [0 0]);% Central point of plate.
% % % % % [xmmnsky, ymmnsky]= getSkyPosition(focal_length, [Car Cde], [xmmn, ymmn]);
% % % % % [IXmm IYmm]= getStandardCoordinates(focal_length, [Car Cde], [IRA IDE]);    %Ideal positions for train Constants plate
% % % % % 
% % % % % figure(1)
% % % % % scatter(IX, IY, 'b*');
% % % % % hold on;
% % % % % scatter(xmmn, ymmn, 'r*');
% % % % % xlim([-100 100])
% % % % % ylim([-100 100])
% % % % % hold off;
% % % % % tempBA=((xmmn-IX).^2);
% % % % % error(1,1)=sqrt(mean((xmmnsky-IRA).^2)); 
% % % % % error(2,1)=sqrt(mean((ymmnsky-IDE).^2)); 
% % % % % 
% % % % % error(1,2)=sqrt(mean((xmmn-IX).^2)); 
% % % % % error(2,2)=sqrt(mean((ymmn-IY).^2));
% % % % % 
% % % % % [fx0, fy0]= makeQuiver([IX IY], [xmmn ymmn], [100,100], plate_name{1,1}, 'With best Angle', 'Error in mm', 2, [200 200]); %12000/60=200 D
=======
figure(1)
scatter(IX, IY, 'b*');
hold on;
scatter(xmmn, ymmn, 'r*');
xlim([-100 100])
ylim([-100 100])
hold off;
tempBA=((xmmn-IX).^2);
error(1,2)=sqrt(mean((xmmn-IX).^2))*1000; %in micros
error(2,2)=sqrt(mean((ymmn-IY).^2))*1000;
e1= sqrt(error(1,2)^2+error(2,2)^2)
% [fx0, fy0]= makeQuiver([IX IY], [xmmn ymmn], [100,100], plate_name{1,1}, 'With best Angle', 'Error in mm', 2, [200 200]); %12000/60=200 D
>>>>>>> 92caa10285a9fd4ab295baccfd3bfe952fcac27e


for i=1: validationFolds
    data3= values;
    
    objTest= [(numPerFold*(i-1))+1:numPerFold*(i)];
    test= data3(objTest,:);                             %Testing set
    
    data3(objTest, :)=[];                               %Training set
    
    TRA = test(:,11);                                    %Right ascension for tests
    TDE = test(:,12);                                    %Declination for tests
    [Txi Teta]= getStandardCoordinates(focal_length, [Car Cde], [TRA TDE]); 
    Tx= test(:,13);                                      %Plate x positions for test
    Ty= test(:,14); 
    
    for j=1:numPerFold
        Td(j,1)=sum(test(j,2:10))/sum(test(j,2:10) ~= 0); 
    end
    
    DRA = data3(:,11);                                   %Right ascension for train Constants plate
    DDE = data3(:,12);                                   %Declination for train Constants plate
    [DX DY]= getStandardCoordinates(focal_length, [Car Cde], [DRA DDE]);    %Ideal positions for train Constants plate
    Dx= data3(:,13);                                     %Plate x positions for train Constants plate.
    Dy= data3(:,14);                                     %Plate y positions for train Constants plate.
    

    [comb]=getSummations(0,0,0,ord);%get the number of combinations
    
    A= zeros(length(Dx)*2, length(comb)+6);
    b= zeros(length(Dx)*2, 1);
    for j=1:length(Dx)
        Dd(j,1)=sum(data3(j,2:10))/sum(data3(j,2:10) ~= 0); 
        [comb,Dv(j,:)]=getSummations(Dx(j,:),Dy(j,:),Dd(j,1),ord);
        [ss,ss2]=size(Dv);
        %
        for k=1:ss2
            DVx(j,k)=(Dv(j,k)*cos(atan2(Dy(j,:),Dx(j,:))));
            DVy(j,k)=(Dv(j,k)*sin(atan2(Dy(j,:),Dx(j,:))));

        end
       A=[A;1,Dx(j,:),Dy(j,:),0,0,0,(-DVx(j,:));0,0,0,1,Dx(j,:),Dy(j,:),(-DVy(j,:))];
       b=[b;DX(j,:);DY(j,:)];
    end

    [ctes]= leastSquares(A, b);
<<<<<<< HEAD
                  
    [E, Pos(numPerFold*(i-1)+1:numPerFold*i,:),tempE1(numPerFold*(i-1)+1:numPerFold*i,:),tempE2(numPerFold*(i-1)+1:numPerFold*i,:)]= testPlateModelsG2([Tx,Ty],Td, [TRA,TDE],ctes , focal_length, [Car Cde],comb);
 
=======

    [E, p,tempC]= testPlateModelsG2([Tx,Ty],Td, [TRA,TDE],ctes , focal_length, [Car Cde],comb);
    p
    Pos(numPerFold*(i-1)+1:numPerFold*i,:)=p;
>>>>>>> 92caa10285a9fd4ab295baccfd3bfe952fcac27e
   
    histConstants(i, :)= ctes;
   % histError2(numPerFold*(i-1)+1:numPerFold*i,:)= tempC;
    histError(i, :)= E;
end
error(1,2)=sqrt(mean(tempE1)); 
error(2,2)=sqrt(mean(tempE2)); 
error(3,2)=sqrt(error(1,2) + error(2,2)); 
figure(3)
scatter(IX,  IY, 'b*');
hold on;
                
scatter(Pos(:,1), Pos(:,2), 'g*');
%xlim([-100 100])
 %ylim([-100 100])
 title('With Adjustment(Günther) and best Angle');
 hold off;
 [fx, fy]= makeQuiver_2([IX(1:numPerFold*i,:) IY(1:numPerFold*i,:)], [Pos(:,1) Pos(:,2)], [100,100], plate_name{1,1}, 'With Adjustment(Günther) and best Angle', 'Error in mm', 4, [200 200]); %12000/60=200 D


 

%Emean=mean(histError);

<<<<<<< HEAD
=======
Emean=mean(histError);
tempc=histError2;
error(2,3)=Emean(1,2)*1000;
error(1,3)=Emean(1,1)*1000;
e2= sqrt(error(2,3)^2+error(1,3)^2)
>>>>>>> 92caa10285a9fd4ab295baccfd3bfe952fcac27e
 %make Quiver only for a partition of training set
%[fx, fy]= makeQuiver([IX IY], Pos, [100,100], plate_name{1,1}, 'After constant-plate Adjustment', 'Error in mm',4, [200 200]); %12000/60=200 D
 
[Zx, Zy, Zxy, aU, aV, aUV]= testZernike(fx, fy, [250 250], [100 100], 1000, 3);
[NPOS]= correctZernike(Zx, Zy, [250 250], [100 100], Pos);
figure(6)
scatter(IX, IY, 'b*');
hold on;
scatter(NPOS(:,1), NPOS(:,2), 'm*');
% hold on;
title('Zernike');
xlim([-100 100])
ylim([-100 100])
hold off;   
  
<<<<<<< HEAD
  [f1, f2]= makeQuiver_2([IX IY],NPOS, [100 100], plate_name{1,1}, 'After Zernike Adjustment', 'Error in mm', 7, [200 200]);
tempZern=((IX-NPOS(:,1)).^2);
error(1,3)=sqrt(mean((IX-NPOS(:,1)).^2));  %in micros
error(2,3)=sqrt(mean((IY-NPOS(:,2)).^2)); 
error(3,3)=sqrt(error(1,3) + error(2,3)); 
=======
%   [f1, f2]= makeQuiver([IX IY],NPOS, [100 100], plate_name{1,1}, 'After Zernike Adjustment', 'Error in mm', 6, [200 200]);
tempZern=((IX-NPOS(:,1)).^2);
error(1,4)=sqrt(mean((IX-NPOS(:,1)).^2))*1000;  %in micros
error(2,4)=sqrt(mean((IY-NPOS(:,2)).^2))*1000;
e3= sqrt(error(1,4)^2+error(2,4)^2)

>>>>>>> 92caa10285a9fd4ab295baccfd3bfe952fcac27e
    
