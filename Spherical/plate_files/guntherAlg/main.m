clear all; 
%Andrea Burgos Madrigal
%March 2018
%Get the constants


%D : the magnitudes promediated
name='AC0386.txt(ep-B1950,eq-1947.0466,SRef-FK4).xlsmagnitudes.csv';
plate_name = strsplit(name,'.');
values=csvread(name,1,1); %file that contain RA DEC x,y and D


focal_length= 2159.1535353535; %focal length value of Camera Schmidt in mm. 2158.8 +- 1.4

ord=2; %limit of the power serie 

validationFolds= 10; % Number of folds for validation test

%histConstants= zeros(validationFolds, plateModel);      %History for Constant plate model.
histError= zeros(validationFolds, 3);                   %History for folds error.


%add ids of each star, this will help to identify which stars are then discarded
ids=[1:length(values)];
ids=ids';
values=horzcat(ids,values);
values2=values;
%eliminate the rows with NaN values
for i=1:length(values2)
    if (isnan(values2(i,14)))||(isnan(values2(i,13)))
        id=find(values(:,1)==i);
            values(id,:)=[];
    end
end

id=values(:,1);

numPerFold= floor(length(values(:,1))/validationFolds); % Number of observations for each Fold.
%magnitudes D are in values from 2 to 10
%coordinates x y in mm
% values(:,13)x=values(:,13)/60; % in mm
% values(:,14) y=values(:,14)/60;

values(:,13)=(values(:,13)-6000);%the center is the origin
values(:,14)=(values(:,14)-6000);
%set the center in the center of the plate
for i=1:length(values)
    if(values(i,13)<=0)
        values(i,13)=values(i,13)-1;
    end
    if(values(i,14)<=0)
        values(i,14)=(values(i,14)-1)*(-1);
    end
end


values(:,13)=values(:,13)/60; % in mm
values(:,14)=values(:,14)/60;

% AR & DE in rad
% % values(:,11)  AR=deg2rad(values(:,11));
% % values (:,12)  DE=deg2rad(values(:,12));
values(:,11)=deg2rad(values(:,11));
values(:,12)=deg2rad(values(:,12));

%[CAR CDE] = findCenter(AR, DE, Xp, Yp, point)
[Car Cde]= findCenter(values(:,11), values(:,12), values(:,13), values(:,14), [0 0]);% Central point of plate.

%----------------------------------get initial error----------------------
Ix= values(:,13);  
Iy= values(:,14);  

IRA = values(:,11);                                   %Right ascension for train Constants plate
IDE = values(:,12);                                   %Declination for train Constants plate

[IX IY]= getStandardCoordinates(focal_length, [Car Cde], [IRA IDE]);    %Ideal positions for train Constants plate
[fx0, fy0]= makeQuiver([IX IY], [Ix Iy], [100,100], plate_name{1,1}, 'Initial error', 'Error in mm', 1, [200 200]); %12000/60=200 D
 %-------------------------------------------------------------------------
[xmmn, ymmn, bestAng, min_e]= getFirstAdj(Ix, Iy, IX, IY, 0.1);  

values(:,13)=xmmn;  
values(:,14)=ymmn;  

[fx0, fy0]= makeQuiver([IX IY], [xmmn ymmn], [100,100], plate_name{1,1}, 'With best Angle', 'Error in mm', 2, [200 200]); %12000/60=200 D


for i=1: validationFolds
    data3= values;
    objTest= [(numPerFold*(i-1))+1:numPerFold*(i)];
    test= data3(objTest,:);                             %Testing set
    
    data3(objTest, :)=[];                               %Training set
    TRA = test(:,11);                                    %Right ascension for tests
    TDE = test(:,12);                                    %Declination for tests
    [Txi Teta]= getStandardCoordinates(focal_length, [Car Cde], [TRA TDE]); 
    Tx= test(:,13);                                      %Plate x positions for test
    Ty= test(:,14); 
    for j=1:numPerFold
        Td(j,1)=sum(test(j,2:10))/sum(test(j,2:10) ~= 0); 
    end
    DRA = data3(:,11);                                   %Right ascension for train Constants plate
    DDE = data3(:,12);                                   %Declination for train Constants plate
    [DX DY]= getStandardCoordinates(focal_length, [Car Cde], [DRA DDE]);    %Ideal positions for train Constants plate
    Dx= data3(:,13);                                     %Plate x positions for train Constants plate.
    Dy= data3(:,14);                                     %Plate y positions for train Constants plate.
    
    
    [comb]=getSummations(0,0,0,ord);%get the number of combinations
    
    A= zeros(length(Dx)*2, length(comb)+6);
    b= zeros(length(Dx)*2, 1);
    for j=1:length(Dx)
        Dd(j,1)=sum(data3(j,2:10))/sum(data3(j,2:10) ~= 0); 
        [comb,Dv(j,:)]=getSummations(Dx(j,:),Dy(j,:),Dd(j,1),ord);
        [ss,ss2]=size(Dv);
        %
        for k=1:ss2
            DVx(j,k)=(Dv(j,k)*cos(atan2(Dy(j,:),Dx(j,:))));
            DVy(j,k)=(Dv(j,k)*sin(atan2(Dy(j,:),Dx(j,:))));

        end
       A=[A;1,Dx(j,:),Dy(j,:),0,0,0,(-DVx(j,:));0,0,0,1,Dx(j,:),Dy(j,:),(-DVy(j,:))];
       b=[b;DX(j,:);DY(j,:)];
    end
%     [sc1,sc2]=size(ctesTemp)
%     %%%%%%%% promedio las constantes de todos los casos
%     for j=1:sc2
%         ctes(1,j)=sum(ctesTemp(:,j))/length(Dx);
%     end

  

    % A=[1,Dx(j,:),Dy(j,:),0,0,0,(-DVx(j,:));0,0,0,1,Dx(j,:),Dy(j,:),(-DVy(j,:))];
    [ctes]= leastSquares(A, b);

    [E, Pos(numPerFold*(i-1)+1:numPerFold*i,:)]= testPlateModelsG([Tx,Ty],Td, [TRA,TDE],ctes , focal_length, [Car Cde],comb);
 
   
    histConstants(i, :)= ctes;
    histError(i, :)= E;
    
end

mean(histError)

 %make Quiver only for a partition of training set
  [cX cY]= getStandardCoordinates(focal_length, [Car Cde], [ Pos(:,1), Pos(:,2)]);    %Ideal positions for train Constants plate

    [fx, fy]= makeQuiver([cX cY], [Ix Iy], [100,100], plate_name{1,1}, 'After constant-plate Adjustment', 'Error in mm',3, [200 200]); %12000/60=200 D
 
  [Zx, Zy, Zxy, aU, aV, aUV]= testZernike(fx, fy, [250 250], [100 100], 1000, 3);
    [NPOS]= correctZernike(Zx, Zy, [250 250], [100 100], Pos);
  %  [c2X c2Y]= getStandardCoordinates(focal_length, [Car Cde], [ NPOS(:,1), NPOS(:,2)]);    %Ideal positions for train Constants plate
    [f1, f2]= makeQuiver([IX IY],NPOS, [100 100], plate_name{1,1}, 'After Zernike Adjustment', 'Error in mm', 4, [200 200]);


    
