function [f1, f2]= makeQuiver(OrigP, DestP, trasl, name, tit, legendE, numfig, sz,maxM,plate)

% [x,y]= meshgrid(0:1:size(1),0:1:size(2));
% size= [12000 12000];

x= OrigP(:,1);
y= OrigP(:,2);

X= linspace(-trasl(1), -trasl(1)+sz(1), 50);
Y= linspace(-trasl(2), -trasl(2)+sz(2), 50);
 
% X= linspace(0,size(1), 50);
% Y= linspace(0,size(2), 50);

[X,Y] = meshgrid(X,Y);

u= DestP(:,1) - OrigP(:,1);
v= DestP(:,2) - OrigP(:,2);

% scale= max([u;v]);
% u= u/scale;
% v= v/scale;

f1= fit( [x y], u, 'poly23');
f2= fit( [x y], v, 'poly23');

%testZernike(f1, f2, size, trasl, 3);

%f1(x,y) = p00 + p10*x + p01*y + p20*x^2 + p11*x*y + p02*y^2 + p21*x^2*y 
%                    + p12*x*y^2 + p03*y^3

co1 = coeffvalues(f1);
co2 = coeffvalues(f2);

U = co1(1) + co1(2)*X + co1(3)*Y + co1(4)*(X.^2) + co1(5)*(X.*Y) + co1(6)*(Y.^2) + co1(7)*(X.^2).*Y + co1(8)*(Y.^2).*X + co1(9)*(Y.^3);
V = co2(1) + co2(2)*X + co2(3)*Y + co2(4)*(X.^2) + co2(5)*(X.*Y) + co2(6)*(Y.^2) + co2(7)*(X.^2).*Y + co2(8)*(Y.^2).*X + co2(9)*(Y.^3);

% figure;
% quiver(x,y,u,v);

%%%%%%%%%%%%%%
fig2= figure(numfig);
set(fig2, 'Position', [100, 100, 1100, 880]);
clf;
% quiver(X,Y,U,V, 1.5);
hold on;

%maxM= 500;
%max(max(sqrt(U.^2 + V.^2)));
% colorm= defineColorMap(64);
colormap jet
% colormap(colorm);

%%%%%%%%%%%

for i=1: length(U(:,1))
   for j=1: length(U(1,:))
       mag= sqrt(U(i,j)^2 + V(i,j)^2);
       raz= abs(mag/maxM);
       
       c= defineColorIndex(raz, colormap);
       q= quiver(X(i,j), Y(i,j), U(i,j)/mag, V(i,j)/mag, 'AutoScaleFactor',sz(1)/60 , 'LineWidth', 1, 'MaxHeadSize', 1.3);
      
       q.Color= c;
   end
end
axis([-trasl(1),-trasl(1)+sz(1),-trasl(2),-trasl(2)+sz(2)])
axis square
grid on
grid minor

hold off;

lab0= linspace(0,1,10)*maxM;
lab=[];
for w=1: length(lab0)
    lab{w}= sprintf('%5.2f', lab0(w));
end

colorbar('Ticks',linspace(0,1,10), 'TickLabels', lab);

title({'Vector Plot of Error', tit, name, plate}, 'FontSize', 19,'FontName','Times New Roman');
xlabel({'X axis digitalized plate', legendE}, 'FontSize', 19,'FontName','Times New Roman');
ylabel({'Y axis'}, 'FontSize', 19,'FontName','Times New Roman');





