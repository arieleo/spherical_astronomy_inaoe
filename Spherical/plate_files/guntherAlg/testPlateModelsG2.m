function [E, Pos,tempE1,tempE2]= testPlateModelsG2(plateCord,D, realPos, C, f, center, comb)

x= plateCord(:,1);
y= plateCord(:,2);

 a=1;
for ii=1:length(x)
    vx=zeros(length(x),1);
    vy=zeros(length(x),1);
    for i=1:length(comb)

        vx(ii,1)=vx(ii,1)+cos(atan2(y(ii,1),x(ii,1)))*C(i+6)*power(x(ii,1),str2num(comb{i,1}(1,1)))*power(y(ii,1),str2num(comb{i,1}(1,2)))*power(D(ii,1),str2num(comb{i,1}(1,3)));
            vy(ii,1)=vy(ii,1)+sin(atan2(y(ii,1),x(ii,1)))*C(i+6)*power(x(ii,1),str2num(comb{i,1}(1,1)))*power(y(ii,1),str2num(comb{i,1}(1,2)))*power(D(ii,1),str2num(comb{i,1}(1,3)));
          
% %         vx(ii,1)=vx(ii,1)+cos(atan(y(ii,1)/x(ii,1)))*C(i+6)*power(x(ii,1),str2num(comb{i,1}(1,1)))*power(y(ii,1),str2num(comb{i,1}(1,2)))*power(D(ii,1),str2num(comb{i,1}(1,3)));
% %             vy(ii,1)=vy(ii,1)+sin(atan(y(ii,1)/x(ii,1)))*C(i+6)*power(x(ii,1),str2num(comb{i,1}(1,1)))*power(y(ii,1),str2num(comb{i,1}(1,2)))*power(D(ii,1),str2num(comb{i,1}(1,3)));
% %    
            a=a+1;
    end
    xi(ii,1)=C(1)+C(2)*x(ii,:)+C(3)*y(ii,:)-vx(ii,:);
    eta(ii,1)=C(4)+C(5)*x(ii,:)+C(6)*y(ii,:)-vy(ii,:);
end
%vx=vx/cos(atan(y/x));
%vy=vy/sin(atan(y/x));


%xi= -(xi-x);
%eta= -(eta-y);

[AR, DE]= getSkyPosition(f, center, [xi eta]);
%[AR DE]= getStandardCoordinates(focal_length, center, [xi eta]);    %Ideal positions for train Constants plate

E1= sqrt(mean((realPos(:,1)-AR).^2));
E2= sqrt(mean((realPos(:,2)-DE).^2));
E3= sqrt(E1.^2 + E2.^2);

E   = [E1 E2 E3]; % Using Root mean square error (RMSE) for Right ascension, Declination and general error.
%tempC=(realPos(:,1)-AR).^2;
tempE1=(plateCord(:,1)-xi).^2;
tempE2=(plateCord(:,2)-eta).^2;
Pos = [xi eta]; % Estimated Right ascension and Declination according to selected plate model.
% [Txi Teta]= getStandardCoordinates(focal_length, [Car Cde], [TRA TDE]);

% 
% 
% n= length(C);
% 
% switch n
%     case 4 % Four plate model
%         xi= -(C(1)-1)*x -C(2)*y -C(3);
%         eta= C(2)*x -(C(1)-1)*y -C(4);
%     case 6 % Six plate model
%         xi=  C(1)*x + C(2)*y + C(3) + C(5)*x + C(6)*y;
%         eta= C(1)*y - C(2)*x + C(4) + C(5)*y + C(6)*x;
%     case 10 % Ten plate model
%         xi  = -(C(1) + C(2)*x + C(3)*y + C(4)*x.*x + C(5)*x.*y + C(6)*y.*y + C(7)*x.*x.*x + C(8)*x.*x.*y + C(9)*x.*y.*y + C(10)*y.*y.*y - x);
%         eta = -(C(1) + C(2)*y + C(3)*x + C(4)*y.*y + C(5)*x.*y + C(6)*x.*x + C(7)*y.*y.*y + C(8)*y.*y.*x + C(9)*x.*x.*y + C(10)*x.*x.*x - y);
%     otherwise
%         disp('not implemented yet');
%     
% end
% 



