function [comb,summations]=getSummations(x,y,D,ord)
%March 2018
%Autor: Andrea Burgos
%Description: Based on the algorithm proposed by Günther&Kox in 1969 is
%calculated the summation of each x y. The constants are still no obtained.
%
%inputs x Arec
%       y Dec
%       D magnitudes
%       ord order for the residuals in equation (2) v=sum_ijk a_ijk x^i y^i  D^i
%outputs:  summations for the star in course

    v=[1:ord];
    l='0';          %// Set of possible letters
    for i=1:ord    
        l=strcat(l,num2str(i));
    end
    K = 3;                      %// Length of each permutation

    %// Create all possible permutations (with repetition) of letters stored in x
    C = cell(K, 1);             %// Preallocate a cell array
    [C{:}] = ndgrid(l);         %// Create K grids of values
    com = cellfun(@(l){l(:)}, C); %// Convert grids to column vectors
    com = [com{:}];   

    comb=cellstr(com);
    a=1;
    for j=1:length(x)
        for i=1:length(comb)
            summations(1,i)=power(x,str2num(comb{i,1}(1,1)))*power(y,str2num(comb{i,1}(1,2)))*power(D,str2num(comb{i,1}(1,3)));
            a=a+1;
        end
    end
    
   
end
