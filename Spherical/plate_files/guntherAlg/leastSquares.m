function [x]= leastSquares(A, b)

%Author: Ariel Ortiz
%September 2016

% Extracted from Matrix Methods, Bronson, 2008
% Solve Systems equation using Ax=b form.

% Input:
%   A: Equation set in matrix form with N columns as variables.
%   b: Solution set for M equations.

% Output:
%   x: Resulting vector of size N. Each value as solution for variables.

Atrans= A';

M= Atrans*A;

N= Atrans*b;

x= pinv(M)*N;

