
function [values]=discardStars(values,name)
    focal_length= 2158.8; %focal length value of Camera Schmidt in mm. 2158.8 +- 1.4

    [CAR CDE]= findCenter(values(:,11), values(:,12), values(:,13), values(:,14), [0 0]);% Central point of plate.

    [XI ETA]= getStandardCoordinates(focal_length, [CAR CDE], [values(:,11) values(:,12)]);    %Ideal positions for train Constants plate

    [xmmn, ymmn, bestAng, min_e]= getFirstAdj(values(:,13), values(:,14), XI, ETA, 0.1);  
    values(:,13)=xmmn;
    values(:,14)=ymmn;

    %%%distancias de cada XI con cada xmmn
    dStars= sqrt((XI - xmmn).^2+(ETA - ymmn).^2);
    dCenter=sqrt((0 - xmmn).^2+(0 - ymmn).^2);

    figure
    
    plot(dCenter,dStars,'r.','DisplayName','Discarded stars');
    legend('Discarded stars')
    f=fit(dCenter,dStars,'poly2');
    hold on;
    plot(f);

 
    for i=1:length(dCenter)
        x=dCenter(i);
        y(i,1) = f.p1*x^2 + f.p2*x + f.p3;
    end
    dis=sqrt((dStars - y).^2);
    index=find(dis>2*mean(dis));

    values2=values;
    for i=1:length(index)
       values2(find(values2(:,1)==index(i)),:)=[];
    end

    values=values2;

    [XI2 ETA2]= getStandardCoordinates(focal_length, [CAR CDE], [values(:,11) values(:,12)]);    %Ideal positions for train Constants plate

    %%%distancias de cada XI con cada xmmn
    dStars= sqrt((XI2 - values(:,13)).^2+(ETA2 - values(:,14)).^2);
    dCenter=sqrt((0 - values(:,13)).^2+(0 - values(:,14)).^2);

    hold on;
    plot(dCenter,dStars,'b.','DisplayName','Final stars');
    title(strcat('Discard stars',name)) 
    hold off;
    xlabel('Distance to center of the plate (mm)');
    ylabel('Distance between stars (mm)');
    legend({'Discarded stars','Fitted curve','Final stars'})
%     print( strcat(name,'curve'),'-dpng') 
%    
end
