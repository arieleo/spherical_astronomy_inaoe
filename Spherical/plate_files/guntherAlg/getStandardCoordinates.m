function [xi, eta]= getStandardCoordinates(f, A, P)

%Author: Ariel Ortiz
%September 2016

% Extracted from Computational Spherical Astronomy, Laurence G. Taff
% Evaluation using ecuatorial coordinates.

% Input:
%   f: telescope focal length.
%   A: Central sky point remaped into plate. A(Right ascension, Declination)
%       in radians.
%   P: Sky point to evaluating. Array of P(Right ascension, Declination) per row in radians.

% Output:
%   xi: Ideal x coordinate over the plate of real P skypoint. eta: Ideal y
%   coordinate over the plate of real P skypoint.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
AR= P(:,1);
DEC= P(:,2);

cotq= cot(DEC).*cos(AR-A(1));
q= acot(cotq);

xi= (cos(q).*tan(AR-A(1)).*sec(q-A(2)))*f;

eta= (tan(q-A(2)))*f;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% 
% xi= ((cot(P(2))*sin(P(1)-A(1)))/(sin(A(2))+cot(P(2))*cos(A(2))*cos(P(1)-A(1))))*f;
% eta= ((cos(A(2))-cot(P(2))*sin(A(2))*cos(P(1)-A(2)))/(sin(A(2))+cot(P(2))*cos(A(2))*cos(P(1)-A(1))))*f;
