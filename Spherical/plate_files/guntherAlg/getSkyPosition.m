function [RA DE]= getSkyPosition (f, A, Pp)

%Author: Ariel Ortiz
%September 2016

% Extracted from Computational Spherical Astronomy, Laurence G. Taff
% Evaluation using ecuatorial coordinates.

% Input:
%   f: telescope focal length.
%   A: Central sky point remaped into plate. A(Right ascension, Declination)
%       in radians.
%   Pp: Array of plate points to evaluate using ideal coordinates. Pp(xi, eta) per row measured in plate units.

% Output:
%   P: resulting Sky point. P(Right ascension, Declination) in radians.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

XI= Pp(:,1);
ETA= Pp(:,2);

tanAlpT= (XI*sec(A(2)))./(f-(ETA*tan(A(2))));

AlpT= atan(tanAlpT);


tanDelta= ((ETA+f*tan(A(2)))./(f-ETA*tan(A(2)))).*cos(AlpT);

RA= AlpT+ A(1);
DE= atan(tanDelta);

% P= [RA, DE];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





