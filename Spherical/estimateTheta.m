function [theta0, bestOp, bestValue]= estimateTheta(A, centerIndex, ppmm, ppmm2, focal_length, theta)

polaris = '01 48 47.78 +89 01 43.6'; % Polaris data
%sigOct = '20 15 03.45 -89 08 18.4';

[ps(1), ps(2)]= str2RD(polaris);

DRA=A(:,1);
DDE=A(:,2);
X = A(:,3);
Y = A(:,4);

DRA=deg2rad(DRA);
DDE=deg2rad(DDE);
DRA(end+1)= ps(1);
DDE(end+1)= ps(2);

center = [DRA(centerIndex),DDE(centerIndex)];
centerXY = [X(centerIndex) Y(centerIndex)];

[Dxi, Deta]= getStandardCoordinates(focal_length, center, [DRA DDE]);
Dxi= Dxi* ppmm;
Deta= Deta*ppmm2;

tform = affine2d([ cos(-theta), -sin(-theta), 0;
    sin(-theta), cos(-theta), 0; 0, 0, 1]);

trasl= [Dxi(centerIndex)-centerXY(1), Deta(centerIndex)-centerXY(2)];

X= X+trasl(1);
Y= Y+trasl(2);

[Dxi2, Deta2] = transformPointsForward(tform,Dxi,Deta);

Dxib = Dxi2(1:end-1);
Detab= Deta2(1:end-1);

% values  = linspace(1,1000,350);
values= 1
options = [1];
minError= inf;
bestValue=[];
bestOp= [];
for ii=1: length(options)
    for jj=1: length(values)
        
        
        C=NineModelCP([X Y], [Dxib Detab], ii, values(jj));
        [POS E]= testNinePlateCoord([Dxi2(end) Deta2(end)], [Dxi2(end) Deta2(end)], C);
        
        if (E(3)<minError)
            minError= E(3);
            bestValue= values(jj);
            bestOp= ii;
            PO= POS;
        end
        
    end
end


X(end+1)= PO(1);
Y(end+1)= PO(2);

u= [Dxi(end) Deta(end)];
v= [X(end) Y(end)];


theta0= getRotationDegByNorth(u,v);
theta0



%cosa= (u(1)*v(1) + u(2)*v(2))/(sqrt(u(1)^2 + u(2)^2)*sqrt(v(1)^2 + v(2)^2));
%theta0= acos(cosa);
% O = [0,0];
%
% m2 = functionSlope(O,u);
% m1 = functionSlope(O,v);
% tanA = (m2 - m1) / (1 + m2 * m1 );
%
% theta0 = atan(tanA);
%
%
% if sign(theta0) == -1
%     theta0 = deg2rad(180) + rad2deg(radanguloEc2);
% end


% 
% 
% % p0= plot(Dxi,Deta,'ro', POS(:,1), POS(:,2), 'blacks', X2,Y2,'b*');
% figure(1000)
% p0= plot(X,Y,'b*', Dxi2,Deta2,'go', Dxi,Deta,'blacks');
% hold on;
% %     for i=1: numStars
% %         tex= num2str(i);
% %         p1=text(X2(i)+70,Y2(i),tex, 'Color','blue');
% %         p2=text(Dxi(i)+70,Deta(i),tex, 'Color','red');
% %         p3=text(POS(i,1)+70,POS(i,2),tex, 'Color','black');
% % %         text(X2(i),Y2(i),tex, 'Color','blue');
% %         plot([X2(i) Dxi(i)], [Y2(i) Deta(i)], 'Color', 'red');
% %         plot([X2(i) POS(i,1)], [Y2(i) POS(i,2)], 'Color', 'black');
% %     end
% axis square
% grid on
% grid minor
% %     legend([p0], 'Estimated Xi and Eta (scaled and rotated)', 'Plate Constant Model Adjustement', 'Plate coordinates');
% hold off;

