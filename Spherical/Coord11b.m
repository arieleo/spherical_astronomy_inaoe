clc;
clear;

dir = 'plate_files/Puntos/pruebas/';

% estadistica = importdata('C:\Users\fenix\Desktop\Puntos\promediosGenerales.txt');
% estadistica = estadistica.data;

NC = obtenerDatos(dir,1,40,0);
RC = obtenerDatos(dir,41,80,-40);
[promN,dN] =promediarPuntos(NC);
[promR,dR] =promediarPuntos(RC);

ppmms= 63.0417;
ppmms2= 63.125;

%Nornmales
XN = promR(:,1)/ppmms;
YN = promR(:,2)/ppmms2;

trasl= [48 40];

XN= XN-trasl(1);
YN= YN-trasl(2);

%Rotadas
XR = promN(:,1)/ppmms;
YR = promN(:,2)/ppmms2;

XR= XR-trasl(1);
YR= YR-trasl(2);

plate_name='CRISTAL';

option=2;
tunevalue=1;

Xorg= linspace(-12*4, 12*4, 9);
% Xorg= [Xorg; Xorg; Xorg; Xorg; Xorg];

Yorg= linspace(-20*2, 20*2, 5);
% Yorg= [Yorg; Yorg; Yorg; Yorg; Yorg; Yorg; Yorg; Yorg; Yorg];

[Xmm, Ymm]= meshgrid(Xorg,Yorg);

Xmm= reshape(Xmm', [size(Xmm,1)*size(Xmm,2), 1]);
Ymm= reshape(Ymm', [size(Ymm,1)*size(Ymm,2), 1]);

Xmm([1,9,37,45])=[];
Ymm([1,9,37,45])=[];

numStars= length(Xmm);


minErr= inf;
bestPpmm=[];
bestPpmm2=[];
bestError=[];


RMSEx= sqrt(mean((XN-Xmm).^2));
RMSEy= sqrt(mean((YN-Ymm).^2));
RMSE= sqrt(RMSEx^2 + RMSEy^2);

POS=[];
E=[];
histC=[];
for pm=1: length(XN)
    X2=XN;
    Y2=YN;
    
    Xmm2= Xmm;
    Ymm2= Ymm;
    
    X2(pm)= [];
    Y2(pm)= [];
    Xmm2(pm)= [];
    Ymm2(pm)= [];
    
    C=NineModelCP([Xmm2 Ymm2], [X2 Y2], option, tunevalue);
    [POS0 E0]= testNinePlateCoord([Xmm(pm) Ymm(pm)], [XN(pm) YN(pm)], C);
    
    histC(pm,:)= C';
    POS(pm,:)=POS0;
    E(pm,:)= E0;
    
end


if(RMSE<minErr)
    minErr= RMSE
    bestPpmm= ppmms;
    bestPpmm2= ppmms2;
    a= mean(histC);
    bestError= mean(E)
    
    %%%%%%% makeQuivers
    [fx0, fy0]= makeQuiver([Xmm Ymm], [XN YN], [0 0], plate_name, 'Before constant-plate Adjustment', 'Error in mm', 2, [100 80]);
    [fx, fy]= makeQuiver([Xmm Ymm], POS, [0 0], plate_name, 'After constant-plate Adjustment', 'Error in mm', 3, [100 80]);
    [Zx, Zy, Zxy, aU, aV, aUV]= testZernike(fx, fy, [100 80], [0 0], 1000, 3);
    [NPOS]= correctZernike(Zx, Zy, [100 80], trasl, POS);
    [f1, f2]= makeQuiver([Xmm Ymm], NPOS, [0 0], plate_name, 'After Zernike Adjustment', 'Error in mm', 4, [100 80]);
    % Reportar aUV
    
    figure(1);
    clf
%     p0= plot(Dxi,Deta,'ro', POS(:,1), POS(:,2), 'blacks', X2,Y2,'b*');
    p0= plot(Xmm, Ymm, 'b*', XN, YN,'ro', POS(:,1), POS(:,2), 'blacks', NPOS(:,1), NPOS(:,2), 'gs');
    hold on;
    for i=1: numStars
        tex= num2str(i);
        p1=text(Xmm(i)+3,Ymm(i),tex, 'Color','blue');
        p2=text(XN(i)+3,YN(i),tex, 'Color','red');
        p3=text(POS(i,1)+3,POS(i,2),tex, 'Color','black');
        p4=text(NPOS(i,1)+3,NPOS(i,2),tex, 'Color','green');
%         text(X2(i),Y2(i),tex, 'Color','blue');
        plot([Xmm(i) XN(i)], [Ymm(i) YN(i)], 'Color', 'red');
        plot([Xmm(i,1) POS(i,1)], [Ymm(i), POS(i,2)], 'Color', 'black');
        plot([Xmm(i,1) NPOS(i,1)], [Ymm(i), NPOS(i,2)], 'Color', 'green');
    end
    axis square
    grid on
    grid minor
    legend([p0],'Original measured points', 'Points measured from plate', 'Plate Constant Model Adjustment', 'Zernike Adjustment');
    hold off;
    pause(0.01);

end

Z_RMSEx= sqrt(mean((NPOS(:,1)-XN).^2));
Z_RMSEy= sqrt(mean((NPOS(:,2)-YN).^2));
Z_RMSE= sqrt(Z_RMSEx^2 + Z_RMSEy^2)




