function [x]= solveUsingMatlab(A, b, op, tun)

%Author: Ariel Ortiz
%September 2016

% Solve linear system of equations given in matrix form by MATLAB native
% function
% Solve Systems equation using Ax=b form.

% According to: http://www.mathworks.com/help/stats/robustfit.html

% Input:
%   A: Equation set in matrix form with N columns as variables.
%   b: Solution set for M equations.

% Output:
%   x: Resulting vector of size N. Each value as solution for variables.

%x= linsolve(A,b);

switch op
    case 2
        x= robustfit(A,b, 'andrews', tun);
    case 3
        x= robustfit(A,b, 'bisquare', tun);
    case 4
        x= robustfit(A,b, 'cauchy', tun);
    case 5
        x= robustfit(A,b, 'fair', tun);
    case 6
        x= robustfit(A,b, 'huber', tun);
    case 7
        x= robustfit(A,b, 'logistic', tun);
    case 8
        x= robustfit(A,b, 'ols', tun);
    case 9
        x= robustfit(A,b, 'talwar', tun);
    case 10
        x= robustfit(A,b, 'welsch', tun);
    otherwise
        x= robustfit(A,b);
end

%x
x(1)=[];