function [C]= tenModelConstantPlate(ideal, standard, op, tun)

%Author: Ariel Ortiz
%September 2016

% Extracted from [2] Sistema para reducción de posiciones de estrellas en
% placas astronómicas, Salazar María-Guadalupe.
% Evaluation using ecuatorial coordinates.

% Input:
%   ideal: Ideal coordinates set. Matrix N x 2. Each row contains xi and
%       eta values for each observation according to getStandardCoordinates
%       function.
%   standard: Measured coordinates taken from plates. Size N x 2. Each row
%       contains x and y values for each visual observation.

% Output:
%   C: Vector containing each value for constansts plate. According to ten
%       model constant plate extracted from [2] p. 35.
%       Solution for Constants plate [a_0 - a_9].

% System form:
%   x - xi = a_0 + a_1*x + a_2*y + a_3*x^2 + a_4*x*y + a_5*y^2 +
%           a_6*x^3 + a_7*x^2*y + a_8*x*y^2 + a_9*y^3

%   y - eta = a_0 + a_1*y + a_2*x + a_3*y^2 + a_4*x*y + a_5*x^2 +
%           a_6*y^3 + a_7*y^2*x + a_8*x^2*y + a_9*x^3

if (length(ideal(:,1))>= 5) % At least 5 observations

A= [];
b= [];

for i= 1: length(ideal(:,1))
    
    x= standard(i,1);
    y= standard(i,2);
    x3= x^3;
    y3= y^2;
    x2= x^2;
    y2= y^2;
    x2y= x2*y;
    y2x= y2*x;
    xy= x*y;
    
    eq1= [1, x, y, x2, xy, y2, x3, x2y, y2x, y3];
    res1= standard(i,1)-ideal(i,1);
    
    eq2= [1, y, x, y2, xy, x2, y3, y2x, x2y, x3];
    res2= standard(i,2)- ideal(i,2);
    
    A= [A; eq1; eq2];
    b= [b; res1; res2];
end

if(op==1)
C= leastSquares(A,b);
else
C= solveUsingMatlab(A,b, op, tun);
end

end