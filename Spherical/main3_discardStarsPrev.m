
clc;
clear all;
close all;

command = 'ls plate_files/platesMagnitudes/'; %navigate to that carpet
system(command)
[status,cmdout] = system(command); %cmdout is the file list
result = textscan( cmdout, '%s %s', 'delimiter', '\n','delimiter', ' ' );%to split the lines
fileList = result{1};

for i=length(fileList):-1:1
    if contains(fileList{i,1},'.m')==1
        fileList{i,1}(:,:)=[];
    end
end
fileList=fileList(~cellfun('isempty',fileList));
fileList=sort(fileList)

validationFolds= 10; % Number of folds for validation test. 48 and 49 examples for .dat files
plateModel= 6; % Degree of equation used.

option=1; % Using different leastsquare implementations. Default 1
tunevalue= 1; % Default= 1.

focal_length= 2158.8; %focal length value of Camera Schmidt in mm. 2158.8 +- 1.4
Error=zeros(1,7)

for s=1:length(fileList)
    try
        name=fileList{s,1};
        plate_name = strsplit(name,'.');

        values=csvread(name,1,1);
        [values falses]= eraseNAN(values);

        %add ids of each star, this will help to identify which stars are then discarded
        ids=[1:length(values)];
        ids=ids';
        values=horzcat(ids,values);
        values2=values;
        id=values(:,1);

        %set the center in the center of the plate
        for i=1:length(values)
            if(values(i,13)<=0)
                values(i,13)=values(i,13)-1;
            end
            if(values(i,14)<=0)
                values(i,14)=(values(i,14)-1)*(-1);
            end
        end

        %pixeles to mmm
        values(:,13:14)=(values(:,13:14)/60)-100; % in mm
        % AR & DE in rad
        values(:,11:12)=deg2rad(values(:,11:12));

       
        

        
       
        [CAR CDE]= findCenter(values(:,11), values(:,12), values(:,13), values(:,14), [0 0]);% Central point of plate.

        [XI ETA]= getStandardCoordinates(focal_length, [CAR CDE], [values(:,11) values(:,12)]);    %Ideal positions for train Constants plate

        [xmmn, ymmn, bestAng, min_e]= getFirstAdj(values(:,13), values(:,14), XI, ETA, 0.1);  
        %values(:,13)=xmmn;
        %values(:,14)=ymmn;






        E1= sqrt(mean((XI - xmmn).^2));
        E2= sqrt(mean((ETA - ymmn).^2));
        E3= sqrt(E1.^2 + E2.^2) % Initial Error.

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% First Plot Positions

        figure(1)
    
        scatter(xmmn, ymmn, 'r*');
        text(xmmn, ymmn,int2str(values(:,1)),'Color','red')
        hold on;
        % scatter(xmmn, ymmn, 'r*');
        % figure(2)
        scatter(XI, ETA, 'b*');
         title(strcat('With best angle ',' ',plate_name{1,1}));
        % scatter(Xn, Yn, 'b*');
        xlim([-100 100])
        ylim([-100 100])
        text(XI, ETA,int2str(values(:,1)),'Color','blue')
        hold on;
       plot([xmmn XI]',[ymmn ETA]','k')
        %plot([xmmn ymmn],[XI ETA],'B')
        hold off;
       
        print( strcat(plate_name{1,1},'BApos_prev'),'-dpng') 
% %         close all;
% 
%         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PLot Quiver
% % % % %        [fx0, fy0]= makeQuiver_2([XI ETA], [xmmn ymmn], [100,100], plate_name{1,1}, 'With best Angle', 'Error in mm', 2, [200 200]); %12000/60=200 D
% % % % %         %print( strcat(plate_name{1,1},'BAquiv_Prev'),'-dpng') 
% % % % % 
% % % % %      
% % % % %         [Zx, Zy, Zxy, aU, aV, aUV]= testZernike(fx0, fy0, [250 250], [100 100], 1000, 3);
% % % % %         [NPOS]= correctZernike(Zx, Zy, [250 250], [100 100], [xmmn ymmn]);    
% % % % %         close all;
% % % % %         [f1, f2]= makeQuiver_2([XI ETA],NPOS, [100 100], plate_name{1,1}, 'After Zernike Adjustment', 'Error in mm', 4, [200 200]);
% % % % %         print( strcat(plate_name{1,1},'ZernikeBA_Prev'),'-dpng') 
        
       
        

         [values]=discardStars2(values,plate_name{1,1});
        [CAR CDE]= findCenter(values(:,11), values(:,12), values(:,13), values(:,14), [0 0]);% Central point of plate.

        [XI ETA]= getStandardCoordinates(focal_length, [CAR CDE], [values(:,11) values(:,12)]);    %Ideal positions for train Constants plate

        [xmmn, ymmn, bestAng, min_e]= getFirstAdj(values(:,13), values(:,14), XI, ETA, 0.1);  
       values(:,13)=xmmn;
        values(:,14)=ymmn;


        c = cvpartition(length(values(:,1)),'KFold', validationFolds);

        numPerFold= floor(length(values(:,1))/validationFolds); % Number of observations for each Fold.
        histConstants= zeros(validationFolds, sum(linspace(1,plateModel+1,plateModel+1)));      %History for Constant plate model.
        histError= zeros(validationFolds, 3);                   %History for folds error.
        all_pos=[];
        for ii=1: validationFolds
            data3= values;

            prev=0;
            if ii>1
                prev= sum(c.TestSize(1:ii-1));
            end

            objTest= [prev+1: prev+c.TestSize(ii)];
            test= data3(objTest,:);                             %Testing set

            data3(objTest, :)=[];                               %Training set

            TRA = test(:,11);                                    %Right ascension for tests
            TDE = test(:,12);                                    %Declination for tests
            [Txi Teta]= getStandardCoordinates(focal_length, [CAR CDE], [TRA TDE]); 
            Tx= test(:,13);                                      %Plate x positions for test
            Ty= test(:,14);                                        %Plate y positions for test

             DRA = data3(:,11);                                   %Right ascension for train Constants plate
            DDE = data3(:,12);                                 %Declination for train Constants plate
            [Dxi Deta]= getStandardCoordinates(focal_length,[CAR CDE], [DRA DDE]);    %Ideal positions for train Constants plate
            Dx= data3(:,13);                                     %Plate x positions for train Constants plate.
            Dy= data3(:,14);                                     %Plate y positions for train Constants plate.

            C= NModelConstantPlate([Dxi Deta], [Dx Dy], plateModel, option, tunevalue);

            [E POS]= testNPlateModels2([Tx Ty], [Txi Teta], C, focal_length, [CAR CDE]); %RMSE for estimated positions (AR, DE) in each Fold.
            all_pos= [all_pos; POS];
            histConstants(ii, :)= C;
            histError(ii, :)= E;

        end

        mesError= mean(histError);
        [histConstants falses]= eraseNAN(histConstants);
        meanConst= mean(histConstants);
        
        [E POS]= testNPlateModels2([xmmn ymmn], [XI ETA], meanConst, focal_length, [CAR CDE]); %RMSE for estimated positions (AR, DE) in each Fold.
        E

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Second Plot Positions

        figure
    
        scatter(POS(:,1), POS(:,2), 'g*');
        text(POS(:,1), POS(:,2),int2str(values(:,1)),'Color','green')
        hold on;
        title( strcat('With adjustment',' ',plate_name{1,1}))
      

        scatter(XI, ETA, 'b*');
        text(XI, ETA,int2str(values(:,1)),'Color','blue')
        xlim([-100 100])
        ylim([-100 100])
        plot([POS(:,1) XI]',[POS(:,2) ETA]','k')
        hold off;
        print( strcat(plate_name{1,1},'ADJpos_prev'),'-dpng') 
        close all;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% QUiver
        [fx, fy]= makeQuiver_2([XI ETA], [POS(:,1) POS(:,2)], [100,100], plate_name{1,1}, 'With adjustment', 'Error in mm', 4, [200 200]); %12000/60=200 D
         print( strcat(plate_name{1,1},'ADJquiv_Prev'),'-dpng') 
        temp(1,:)=[s,E1,E2,E3,E];
       
        %%%para saber el error a qué placa se refiere observar la primera
    %     %%%columna y usar ese indice en fileList
    catch
        temp(1,:)=[s,0,0,0,0,0,0]; 
    end
        Error=[Error;temp];
end                
Error(1,:)=[];