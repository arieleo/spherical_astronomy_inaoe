function [ p  ] = graficarConPlots( C,color )


for j=1:numel(C)
    patron = strcat('*',color);
    graficarN = C{j};
    p =  plot(graficarN(:,1),graficarN(:,2) , patron);
    
end



end

