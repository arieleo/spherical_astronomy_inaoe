function [nonNAN a]= eraseNAN(data)

[a b]= find(isnan(data)==1);
a= unique(a);
nonNAN= data;

nonNAN(a,:)=[];