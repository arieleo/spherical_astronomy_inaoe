function [E, Pos]= testPlateModels(plateCord, realPos, C, f, center)

x= plateCord(:,1);
y= plateCord(:,2);


n= length(C);

switch n
    case 4 % Four plate model
        xi= -(C(1)-1)*x -C(2)*y -C(3);
        eta= C(2)*x -(C(1)-1)*y -C(4);
    case 6 % Six plate model
        xi=  C(1)*x + C(2)*y + C(3) + C(5)*x + C(6)*y;
        eta= C(1)*y - C(2)*x + C(4) + C(5)*y + C(6)*x;
    case 10 % Ten plate model
        xi  = -(C(1) + C(2)*x + C(3)*y + C(4)*x.*x + C(5)*x.*y + C(6)*y.*y + C(7)*x.*x.*x + C(8)*x.*x.*y + C(9)*x.*y.*y + C(10)*y.*y.*y - x);
        eta = -(C(1) + C(2)*y + C(3)*x + C(4)*y.*y + C(5)*x.*y + C(6)*x.*x + C(7)*y.*y.*y + C(8)*y.*y.*x + C(9)*x.*x.*y + C(10)*x.*x.*x - y);
    otherwise
        disp('not implemented yet');
    
end

[AR, DE]= getSkyPosition(f, center, [xi eta]);

E1= sqrt(mean((realPos(:,1)-AR).^2));
E2= sqrt(mean((realPos(:,2)-DE).^2));
E3= sqrt(E1.^2 + E2.^2);

E   = [E1 E2 E3]; % Using Root mean square error (RMSE) for Right ascension, Declination and general error.
Pos = [AR DE]; % Estimated Right ascension and Declination according to selected plate model.


