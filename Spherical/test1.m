clear all;
clc;

cont=1;
par = linspace(1,50, 100);
resultados= zeros(10* length(par), 5);

for iii= 1:10
    iii
    
    for jjj=1: length(par)

validationFolds= 15; % Number of folds for validation test
plateModel= 4; % Plate model used. 4, 6, or 10 at this moment

option= iii; % Using different leastsquare implementations. Default 1
tunevalue= par(jjj); % Default= 1.

centerIndex= 51; % index of selected Ccenter point. Originally 51 for this example.
focal_length= 2159.1535353535; %focal length value of Camera Schmidt in mm. 2158.8 +- 1.4

data= importdata('Plate_1'); % Plate data file
data2= data.data; % Numerical data N x 6 culumns (RA, DEC, X_Std, Y_Std, X_Plate, Y_Plate)

data2(:,1:2)=degtorad(data2(:,1:2)); % Convertion RA and DEC to radians.

center= data2(centerIndex,1:2); % Central point of plate.
data2(centerIndex, :)=[];          % Center point discarted from test.

numPerFold= floor(length(data2(:,1))/validationFolds); % Number of observations for each Fold.
histConstants= zeros(validationFolds, plateModel);      %History for Constant plate model.
histError= zeros(validationFolds, 3);                   %History for folds error.
for ii=1: validationFolds
    data3= data2;
    
    objTest= [(numPerFold*(ii-1))+1:numPerFold*(ii)];
    test= data3(objTest,:);                             %Testing set
    
    data3(objTest, :)=[];                               %Training set
    
    TRA = test(:,1);                                    %Right ascension for tests
    TDE = test(:,2);                                    %Declination for tests
    [Txi Teta]= getStandardCoordinates(focal_length, center, [TRA TDE]);    %Ideal positions for Test
    Tx= test(:,5);                                      %Plate x positions for test
    Ty= test(:,6);                                      %Plate y positions for test
    
    DRA = data3(:,1);                                   %Right ascension for train Constants plate
    DDE = data3(:,2);                                   %Declination for train Constants plate
    [Dxi Deta]= getStandardCoordinates(focal_length, center, [DRA DDE]);    %Ideal positions for train Constants plate
    Dx= data3(:,5);                                     %Plate x positions for train Constants plate.
    Dy= data3(:,6);                                     %Plate y positions for train Constants plate.

    switch plateModel
        case 4 % Four Model constant plate...
            C= fourModelConstantPlate([Dxi Deta], [Dx Dy], option, tunevalue);
        case 6 % Six Model constant plate...
            C= sixModelConstantPlate([Dxi Deta], [Dx Dy], option, tunevalue);
        case 10 % Ten Model constant plate...
            C= tenModelConstantPlate([Dxi Deta], [Dx Dy], option, tunevalue);
        otherwise
            disp('not implemented yet')
    end
    
    [E POS]= testPlateModels([Tx Ty], [TRA TDE], C, focal_length, center); %RMSE for estimated positions (AR, DE) in each Fold.
    histConstants(ii, :)= C;
    histError(ii, :)= E;

end

resultados(cont, :)= [mean(histError) iii jjj];
cont=cont+1;

    end
end