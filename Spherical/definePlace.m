function [ cuadrante ] = definePlace( punto )

x = punto(1);
y = punto(2);


sx = sign(x);
sy = sign(y);


if sx == 1 && sy == 1
    cuadrante = 1;
end

if sx == -1 && sy == 1
    cuadrante = 2;
end

if sx == -1 && sy == -1
    cuadrante = 3;
end

if sx == 1 && sy == -1
    cuadrante = 4;
end

if sx == 0
    if  sy == 1
        cuadrante = 1;
    end
    if  sy == -1
        cuadrante = 3;
    end
end

if sy == 0
    if  sx == 1
        cuadrante = 4;
    end
    if  sx == -1
        cuadrante = 2;
    end
end

if sx == 0 && sy == 0
    cuadrante = 0;
end




end
