
clc;
clear all;

data2= importdata('plate_files/AC0388nuevos'); % Plate data file -> For .dat files (CoordNR-AC0359.dat and CoordNR.dat)
polar= data2(1,:); % north polar star
%data2(1,:)=[];

% data2(:,3)=[];
[data2 falses]= eraseNAN(data2);

validationFolds= length(data2(:,1)); % Number of folds for validation test. 48 and 49 examples for .dat files
plateModel= 5; % Degree of equation used.

option= 1; % Using different leastsquare implementations. Default 1
tunevalue= 1; % Default= 1.

    centerIndex= 17;
% centerIndex= 42; % index of selected Ccenter point. Originally 51 for this example.
focal_length= 2158.8; %focal length value of Camera Schmidt in mm. 2158.8 +- 1.4


%data2= data.data; % Numerical data N x 6 culumns (RA, DEC, X_Std, Y_Std, X_Plate, Y_Plate)

data2(:,1:2)=degtorad(data2(:,1:2)); % Convertion RA and DEC to radians.

center= data2(centerIndex,1:2); % Central point of plate.
%data2(centerIndex, :)=[];          % Center point discarted from test.

numPerFold= floor(length(data2(:,1))/validationFolds); % Number of observations for each Fold.
histConstants= zeros(validationFolds, sum(linspace(1,plateModel+1,plateModel+1)));      %History for Constant plate model.
histError= zeros(validationFolds, 3);                   %History for folds error.
for ii=1: validationFolds
    data3= data2;
    
    objTest= [(numPerFold*(ii-1))+1:numPerFold*(ii)];
    test= data3(objTest,:);                             %Testing set
    
    data3(objTest, :)=[];                               %Training set
    
    TRA = test(:,1);                                    %Right ascension for tests
    TDE = test(:,2);                                    %Declination for tests
    [Txi Teta]= getStandardCoordinates(focal_length, center, [TRA TDE]);    %Ideal positions for Test
    Tx= test(:,3);                                      %Plate x positions for test
    Ty= test(:,4);                                      %Plate y positions for test
    
    DRA = data3(:,1);                                   %Right ascension for train Constants plate
    DDE = data3(:,2);                                   %Declination for train Constants plate
    [Dxi Deta]= getStandardCoordinates(focal_length, center, [DRA DDE]);    %Ideal positions for train Constants plate
    Dx= data3(:,3);                                     %Plate x positions for train Constants plate.
    Dy= data3(:,4);                                     %Plate y positions for train Constants plate.

    C= NModelConstantPlate([Dxi Deta], [Dx Dy], plateModel, option, tunevalue);
    
    [E POS]= testNPlateModels([Tx Ty], [TRA TDE], C, focal_length, center); %RMSE for estimated positions (AR, DE) in each Fold.
    histConstants(ii, :)= C;
    histError(ii, :)= E;

end

mesError= mean(histError)