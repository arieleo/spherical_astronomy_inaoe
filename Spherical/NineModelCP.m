function [C]= NineModelCP(ideal, standard, op, tun)

%Author: Ariel Ortiz
%September 2016

% Extracted from [3] Kovalevsky, J., & Seidelmann, P. K. (2004).
% Fundamentals of astrometry. Cambridge University Press. p. 329.
% Evaluation using ecuatorial coordinates.

% Input:
%   ideal: Ideal coordinates set. Matrix N x 2. Each row contains xi and
%       eta values for each observation according to getStandardCoordinates
%       function.
%   standard: Measured coordinates taken from plates. Size N x 2. Each row
%       contains x and y values for each visual observation.

% Output:
%   C: Vector containing each value for constansts plate. According to ten
%       model constant plate extracted from [3] p. 329.
%       Solution for Constants plate [a_0 - a_8].

% System form:
%   eta =  P_1x + P_2y + P_3 + P_5x + P_6y + P_7x^2 + P_8xy + P_9xr^2
%   xi =  -P_2x + P_1y + P_4 + P_6x - P_5y + P_7xy + P_8y^2 + P_9yr^2

% Where r^2 = x^2 + y^2

if (length(ideal(:,1))>= 9/2) % At least N/2 observations

x= standard(:,1);
y= standard(:,2);

xi= ideal(:,1);
eta= ideal(:,2);

eq1=zeros(length(xi),9);
eq2=zeros(length(xi),9);
% for ii=0: N
%     g= linspace(ii,0,ii+1);
%     g2= linspace(0,ii,ii+1);
%     for jj=1: length(g)
%         eq1=[eq1, (x.^(g(jj))).*(y.^(g2(jj)))];
%         eq2=[eq2, (y.^(g(jj))).*(x.^(g2(jj)))];
%     end
%     
% end

for ii=1: length(xi)
    eq1(ii, :)= [x(ii) y(ii) 1 0 x(ii) y(ii) x(ii)^2 x(ii)*y(ii) x(ii)*(x(ii)^2 + y(ii)^2)];
    eq2(ii, :)= [y(ii) -x(ii) 0 1 -y(ii) x(ii) x(ii)*y(ii) y(ii)^2 y(ii)*(x(ii)^2 + y(ii)^2)];
end

res1= xi;
res2= eta;

A= zeros(length(ideal(:,1))*2, length(eq1(1,:)));
b= zeros(length(ideal(:,1))*2, 1);
con=1;
for i=1:2:length(A(:,1))
    A(i,:)= eq1(con,:);
    A(i+1,:)= eq2(con,:);
    b(i)= res1(con);
    b(i+1)= res2(con);
    con=con+1;
end

if(op==1)
C= leastSquares(A,b);
else
C= solveUsingMatlab(A,b, op, tun);
end

end