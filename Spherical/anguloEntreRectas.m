function [ angulo ] = Untitled2( pu,pv )

num = pu(1) * pv(1) + pu(2) * pv(2);

u22 = pu(1)^2 + pu(2)^2;
v22 = pv(1)^2 + pv(2)^2;

dem =  sqrt(u22 * v22);

cosAangulo = num / dem;
angulo = acos(cosAangulo);




end

