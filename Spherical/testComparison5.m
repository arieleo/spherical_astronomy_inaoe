clc;
clear all;


plateModel= 10; % Degree of equation used. <- Best Results using 4 and 5 for .dat files.

option= 1; %Using different leastsquare implementations. Default 1
tunevalue= 0.01; % Default= 1.

focal_length= 2159.1535353535; %focal length value of Camera Schmidt in mm. 2158.8 +- 1.4

data2= importdata('plate_files/ac1212.txt'); % Plate data file. -> For .dat files (CoordNR-AC0359.dat and CoordNR.dat)
data2= data2.data; % Numerical data N x 6 culumns (RA, DEC, X_Std, Y_Std, X_Plate, Y_Plate, scale factor_eX, scale factor_ey ...)
                    %Using 13, 14 in order to obtain estimated
                    %plate coordinates according to [Scale method]
                    %data0=data2
                                  
%polar= data2(1,:); % north polar star
%data2(1,:)=[];

%data2(:,3)=[];
[data2 falses]= eraseNAN(data2);

numStars= length(data2(:,1))
validationFolds= numStars; % Number of folds for validation test
posCenters= [1:numStars];
data2(:,1:2)=degtorad(data2(:,1:2)); % Convertion RA and DEC to radians.


minError= inf;
bestCenter=0;
%bestPolar=[];
for iii=1:length(posCenters)

centerIndex= posCenters(iii); % index of selected Ccenter point. Originally 51 for this example.
                    

% size(data2)

plateCoords= data2(:,5:6);
%estimatedCoords= data2(:,14:15);

center= data2(centerIndex,1:2); % Central point of plate.
%data2(centerIndex, :)=[];          % Center point discarted from test.

numPerFold= floor(length(data2(:,1))/validationFolds); % Number of observations for each Fold.
histConstants= zeros(validationFolds, sum(linspace(1,plateModel+1,plateModel+1)));      %History for Constant plate model.
histError= zeros(validationFolds, 3);                   %History for folds error.
%histPos= zeros(validationFolds, 2);                     %Histogram for Calculated plate positions.
histPos=[];

for ii=1: validationFolds

    data3= data2;
    
    objTest= [(numPerFold*(ii-1))+1:numPerFold*(ii)];
    test= data3(objTest,:);                             %Testing set
    
    %data3(objTest, :)=[];                               %Training set
    objTest

    DRA = data3(:,1);                                   %Right ascension for train Constants plate
    DDE = data3(:,2);                                   %Declination for train Constants plate
    [Dxi Deta]= getStandardCoordinates(focal_length, center, [DRA DDE]);    %Ideal positions for train Constants plate
    Dx= data3(:,5);                                     %Plate x positions for train Constants plate.
    Dy= data3(:,6);                                     %Plate y positions for train Constants plate.

    C= NModelConstantPlate([Dx Dy], [Dxi Deta], plateModel, option, tunevalue);
    
    TRA = test(:,1);                                    %Right ascension for tests
    TDE = test(:,2);                                    %Declination for tests
    [Txi Teta]= getStandardCoordinates(focal_length, center, [TRA TDE]);    %Ideal positions for Test
    Tx= test(:,5);                                      %Plate x positions for test
    Ty= test(:,6);                                      %Plate y positions for test
    
    [POS E]= testNPlateCoordinates([Txi Teta], [Tx Ty], C);
    
    
   
    histConstants(ii, :)= C;
    histError(ii, :)= E;
    histPos=[histPos; POS];
%     histPos(ii,:)= POS;

end
ERR=mean(histError);

if(ERR(3)<minError)
    minError= ERR(3);
    bestError=histError;
    bestCenter= centerIndex;
%     [TxiPolar TetaPolar]= getStandardCoordinates(focal_length, center, polar(1:2));
%     [POSPolar Epolar]= testNPlateCoordinates([TxiPolar TetaPolar], polar(1:2), mean(histConstants));
    
%     bestPolar= POSPolar;
end

end
minError                               %Min error detected in pixels
% bestPolar                              %Best plate coordinates approximation for polar star
%data2(bestCenter,:)                    %Best centered star aproximation [RA(radians) DE(radians) plateX plateY]
bestCenter

