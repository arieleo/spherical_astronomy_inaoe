function [ m ] = functionSlope( p1,p2 )
%functionSlope, Calculate the slope of a straight line



    x1 = p1(1);
    y1 = p1(2);
    
    
    x2 = p2(1);
    y2 = p2(2);

    xM = x2 - x1;
    yM = y2 - y1;
    m = yM / xM;


    if m == Inf
        m = 0.0000001;
    end


end

