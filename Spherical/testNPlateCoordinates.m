function [Pos, E]= testNPlateCoordinates(realPos, plateCord, C)

xR= realPos(:,1);
yR= realPos(:,2);

xi= plateCord(:,1);
eta= plateCord(:,2);

n= length(C);

N=-1;
con=1;
while(n>0)
    n=n-con;
    N=N+1;
    con=con+1;
end

x=zeros(size(xi));
y=zeros(size(eta));
con=1;
for ii=0: N
    g= linspace(ii,0,ii+1);
    g2= linspace(0,ii,ii+1);
    for jj=1: length(g)
        x=x+(((xi.^(g(jj))).*(eta.^(g2(jj))))*C(con));
        y=y+(((eta.^(g(jj))).*(xi.^(g2(jj))))*C(con));
        con=con+1;
    end
    
end

x= -(x-xi);
y= -(y-eta);

E1= sqrt(mean((xR-x).^2));
E2= sqrt(mean((yR-y).^2));
E3= sqrt(E1.^2 + E2.^2);

E   = [E1 E2 E3]; % Using Root mean square error (RMSE) for Right ascension, Declination and general error.
Pos = [x y]; % Estimated Right ascension and Declination according to selected plate model.