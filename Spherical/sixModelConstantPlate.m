function [C]= sixModelConstantPlate(ideal, standard, op, tun)

%Author: Ariel Ortiz
%September 2016

% Extracted from [3] Morrison, L. V. (1994). Galactic and Solar System Optical Astrometry. Cambridge University Press.
% Evaluation using ecuatorial coordinates.

% Input:
%   ideal: Ideal coordinates set. Matrix N x 2. Each row contains xi and
%       eta values for each observation according to getStandardCoordinates
%       function.
%   standard: Measured coordinates taken from plates. Size N x 2. Each row
%       contains x and y values for each visual observation.

% Output:
%   C: Vector containing each value for constansts plate. According to six
%       model constant plate extracted from [3] p. 13.
%       Solution for Constants plate [A - F].

% System form:
%   xi = Ax + By + C + Ex + Fy
%   eta = Ay - Bx + D  - Ey + Fx

if (length(ideal(:,1))>= 3) % At least 3 observations

A= zeros(length(ideal(:,1))*2, 6);
b= zeros(length(ideal(:,1))*2, 1);

for i= 1: length(ideal(:,1))
    
    x= standard(i,1);
    y= standard(i,2);
    
    eq1= [x, y, 1, 0, x, y];
    res1= ideal(i,1);
    
    eq2= [y, -x, 0, 1, -y, x];
    res2= ideal(i,2);
    
    A= [A; eq1; eq2];
    b= [b; res1; res2];
end

if(op==1)
C= leastSquares(A,b);
else
C= solveUsingMatlab(A,b, op, tun);
end

end