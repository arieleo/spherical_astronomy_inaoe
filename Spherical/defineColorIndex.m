function [color]= defineColorIndex(raz, cm)

siz= size(cm, 1);

values= linspace(0,1,siz);

res= abs(values-raz);

index= find(res==min(res));

color= cm(index(1),:);