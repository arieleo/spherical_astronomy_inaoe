clc
clear
% Importamos los datos de las im�genes y los metemos a las ...
% variables AR, DEC, X y Y. Tambi�n definimos la escala de placa ps
path = 'plate_files';
placa = '1739';
forma = 'Normal';
A=importdata([path,'/',placa,forma])

numStars= length(A(:,1));
centers= 21;

DRA=A(:,1);
DDE=A(:,2);
X=A(:,3);
Y=A(:,4);

focal_length= 2158.8; %focal length value of Camera Schmidt in mm. 2158.8 +- 1.4
plateModel=2;
option=2;
tunevalue=1;

%Pasamos las coordenadas AR y DEC a radianes
DRA=deg2rad(DRA);
DDE=deg2rad(DDE);

ppmms= linspace(40,70,60);
ppmms2= linspace(40,70,60);
angs= deg2rad(linspace(307, 315, 25));

% ppmms= 63.2143;
% ppmms= 63.1020;
% ppmms2= 63.1837;
% angs= 3.9788;
% angs= 0.8183;

minErr= inf;
bestCenter=[];
bestPpmm=[];
bestAng=[];
bestPpmm2=[];
for iii= 1: length(centers)
    iii
    for jjj= 1: length(ppmms)
        for kkk= 1: length(angs)
            for lll= 1: length(ppmms2)
                %     iii
                
                centerIndex= centers(iii);
                ppmm= ppmms(jjj);
                ppmm2= ppmms2(lll);
                theta= angs(kkk);
                
                center= [DRA(centerIndex) DDE(centerIndex)];
                centerXY= [X(centerIndex) Y(centerIndex)];
                
                [Dxi Deta]= getStandardCoordinates(focal_length, center, [DRA DDE]);
                
                Dxi= Dxi* ppmm;
                Deta= Deta* ppmm2;
                
                trasl= [Dxi(centerIndex)-centerXY(1), Deta(centerIndex)-centerXY(2)];
                
                X2= X+trasl(1);
                Y2= Y+trasl(2);
                
                % X2= X2*cos(tetha)-Y2*sin(tetha);
                % Y2= X2*sin(tetha)+ Y2*cos(tetha);
                
                tform = affine2d([cos(-theta) -sin(-theta) 0; sin(-theta) cos(-theta) 0; 0 0 1]);
                [Dxi,Deta] = transformPointsForward(tform,Dxi,Deta);
                
                RMSEx= mean(sqrt((Dxi-X2).^2));
                RMSEy= mean(sqrt((Deta-Y2).^2));
                RMSE= sqrt(RMSEx^2 + RMSEy^2);
                
                % RMSExB= sqrt((Dxi-X2).^2);
                % RMSEyB= sqrt((Deta-Y2).^2);
                % RMSEB= (RMSExB.^2 + RMSEyB.^2).^(1/2);
                
                POS=[];
                E=[];
                histC=[];
                for pm=1: numStars
                    X3=X2;
                    Y3=Y2;
                    Dxib= Dxi;
                    Detab= Deta;
                    
                    X3(pm)=[];
                    Y3(pm)=[];
                    Dxib(pm)=[];
                    Detab(pm)=[];
                    %
                    %     C= NModelConstantPlate([X3 Y3], [Dxib Detab], plateModel, option, tunevalue);
                    %     [POS0 E0]= testNPlateCoordinates([Dxi(pm) Deta(pm)], [X2(pm) Y2(pm)], C);
                    
                    C=NineModelCP([X3 Y3], [Dxib Detab], option, tunevalue);
                    [POS0 E0]= testNinePlateCoord([Dxi(pm) Deta(pm)], [X2(pm) Y2(pm)], C);
                    
                    histC(pm,:)= C';
                    POS(pm,:)=POS0;
                    E(pm,:)= E0;
                    
                end
                
                % C= NModelConstantPlate([X2 Y2], [Dxi Deta], plateModel, option, tunevalue)
                % [POS E]= testNPlateCoordinates([Dxi Deta], [X2 Y2], C);
                
                % C= NineModelCP([X2 Y2], [Dxi Deta], option, tunevalue);
                % [POS E]= testNinePlateCoord([Dxi Deta], [X2 Y2], C);
                
                
                if(RMSE<minErr)
                    minErr= RMSE
                    bestCenter= centerIndex;
                    bestPpmm= ppmm;
                    bestPpmm2= ppmm2;
                    bestAng= theta;
                    
                    figure(1);
                    %     clf
                    p0= plot(Dxi,Deta,'ro', POS(:,1), POS(:,2), 'blacks', X2,Y2,'b*');
                    hold on;
                    for i=1: numStars
                        tex= num2str(i);
                        p1=text(X2(i)+70,Y2(i),tex, 'Color','blue');
                        p2=text(Dxi(i)+70,Deta(i),tex, 'Color','red');
                        p3=text(POS(i,1)+70,POS(i,2),tex, 'Color','black');
                        %         text(X2(i),Y2(i),tex, 'Color','blue');
                        plot([X2(i) Dxi(i)], [Y2(i) Deta(i)], 'Color', 'red');
                        plot([X2(i) POS(i,1)], [Y2(i) POS(i,2)], 'Color', 'black');
                    end
                    axis square
                    grid on
                    grid minor
                    legend([p0], 'Estimated Xi and Eta (scaled and rotated)', 'Plate coordinates', 'Plate Constant Model Adjustement');
                    hold off;
                    
                    
                end
                
%                 mean(E)
                % E
                
            end
        end
    end
end

