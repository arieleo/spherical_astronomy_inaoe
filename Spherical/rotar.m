function [x_rotated, y_rotated]=rotar(x_center, y_center, AR, DEC, theta)

% create a matrix of these points, which will be useful in future calculations
v = [AR';DEC'];
% create a matrix which will be used later in calculations
center = repmat([x_center; y_center], 1, length(AR));

R = [cos(theta) -sin(theta); sin(theta) cos(theta)];
% do the rotation...
s = v - center;     % shift points in the plane so that the center of rotation is at the origin
so = R*s;           % apply the rotation about the origin
vo = so + center;   % shift again so the origin goes back to the desired center of rotation
% this can be done in one line as:
% vo = R*(v - center) + center
% pick out the vectors of rotated x- and y-data
x_rotated = vo(1,:);
y_rotated = vo(2,:);
x_rotated = x_rotated';
y_rotated = y_rotated';
