clc;
clear all;

focal_length= 2158.8; %focal length value of Camera Schmidt in mm. 2158.8 +- 1.4

data2= importdata('plate_files/AC1212'); % Plate data file. -> For .dat files (CoordNR-AC0359.dat and CoordNR.dat)
%data2= data2.data; % Numerical data N x 6 culumns (RA, DEC, X_Std, Y_Std, X_Plate, Y_Plate, scale factor_eX, scale factor_ey ...)
                    %Using indexes 13, 14 in order to obtain estimated
                    %plate coordinates according to [Scale method]
                    %data0=data2
                    
% data2= [data2(:,1:2) data2(:,5:6)];
% data2(:,3)=[];
data2(:,1:2)=degtorad(data2(:,1:2)); % Convertion RA and DEC to radians.
                    
polar= data2(1,:); % north polar star
data2(1,:)=[];

[data2 falses]= eraseNAN(data2);

numStars= length(data2(:,1));

plateModel= 5;
centerIndex= 51; % index of selected Ccenter point. Originally 51 for this example.
option= 1;
tunevalue= 1;

plateCoords= data2(:,3:4);
%estimatedCoords= data2(:,14:15);

center= data2(centerIndex,1:2); % Central point of plate.
%data2(centerIndex, :)=[];          % Center point discarted from test.

    DRA = data2(:,1);                                   %Right ascension for train Constants plate
    DDE = data2(:,2);                                   %Declination for train Constants plate
    [Dxi Deta]= getStandardCoordinates(focal_length, center, [DRA DDE]);    %Ideal positions for train Constants plate
    Dx= data2(:,3);                                     %Plate x positions for train Constants plate.
    Dy= data2(:,4);                                     %Plate y positions for train Constants plate.

    C= NModelConstantPlate([Dx Dy], [Dxi Deta], plateModel, option, tunevalue);
    
    C=C';
    C2= rand(size(C));
%     [POS E]= testNPlateCoordinates([Dxi Deta], [Dx Dy], C);

data= [DRA DDE Dx Dy Dxi Deta];

geneticPlateCons(data, C, C);


