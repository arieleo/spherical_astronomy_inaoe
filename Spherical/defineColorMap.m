function [colmap]= defineColorMap(n)

colmap= zeros(n, 3);
rang= linspace(0,1,n);

for i=1: length(rang);
   colmap(i,:)= defineRGBColor(rang(i)); 
end