function [Cb]= geneticPlateCons(data, C, C2)

numGen= 10000;
numInd= 1000;
mutation= 0.75;
cross= 0.8;
Thr= 100;

range= [-15 15];

numStars= length(data(:,1));
numCross= floor(numStars* cross);

%data2(:,1:2)=degtorad(data2(:,1:2)); % Convertion RA and DEC to radians.
%[data2 falses]= eraseNAN(data2);


% minError= inf;
% 
% plateCoords= data(:,3:4);
% Tx= test(:,3);                                      %Plate x positions for test
% Ty= test(:,4);                                      %Plate y positions for test
% 
% [POS E]= testNPlateCoordinates([Txi Teta], [Tx Ty], C);

pop= genPop(C, numInd, range);
pop= [C; pop];

counter=0;
[pop eval]= evaluatePop(data, pop, numInd);

while (counter<=numGen)
    counter
    abs(mean(pop(1,:)-C2))
    pop(1,:)
    eval(1:20)
    if(eval(1)<Thr)
        break;
    end
    
%     [child]= makeCross(pop, numCross);
    [child]= makeCross2(pop, numCross, 25);
    [child]= makeMut(child, mutation);
    pop= [pop; child];
    [pop eval]= evaluatePop(data, pop, numInd);
    
    counter=counter+1;
end

Cb= pop(1,:)';

end

function [pop]= makeMut(pop, mutation)
numMut= floor(length(pop(:,1))*mutation);
index= ceil(length(pop(:,1))*rand(1, numMut));
crom= ceil(rand(1, numMut)* length(pop(1,:)));

r= [-10 10];

for ii=1: numMut
   n= ((r(2)-r(1))*rand(1))+r(1);
%    n= (3* rand(1))-1.5;
   pop(index(ii), crom(ii))= pop(index(ii), crom(ii))*n;
end

end

function [child]= makeCross2(pop, numCross, maxPar)
child= zeros(numCross, length(pop(1,:)));

for ii= 1: numCross
    par = (round(rand(1)*(maxPar-2)))+2;
    index= ceil(rand(1, par)* numCross);
    parents= pop(index, :);
    for jj=1: length(parents(1,:))
        irow= ceil(rand(1)*length(index));
        child(ii,jj)= parents(irow, jj);
    end
end
end

function [child]= makeCross(pop, numCross)
best= pop(1:numCross, :);
child= zeros(numCross*numCross, length(pop(1,:)));
index= ceil(length(pop(:,1))*rand(1, numCross));

cont=1;
for ii=1: numCross
    for jj=1: length(index)
        child(cont,:)= mean([best(ii,:); pop(index(jj),:)]);
%         child(cont,:)= [best(ii,1:fix(length(best(ii,:))/2)) pop(index(jj),fix(length(best(ii,:))/2)+1:end)];
        cont=cont+1;
    end
end
end

function [pop]= genPop(C, Num, rang)
pop= rand(Num, length(C));
pop= (pop*(rang(2)-rang(1)))+rang(1);
for i= 1: length(pop(:,1))
    pop(i,:)= pop(i,:).* C;
end
end

function [pop eval]= evaluatePop(data, pp, numInd)
eval= zeros(length(data(:,1)),1);
Dxi= data(:,5);
Deta= data(:,6);
Dx= data(:,3);
Dy= data(:,4);

for i=1: length(pp(:,1))
    [POS E]= testNPlateCoordinates([Dxi Deta], [Dx Dy], pp(i,:)');
    eval(i)= E(3);
end

[eval I]= sort(eval);
pop= pp(I,:);

eval= eval(1:numInd);
pop= pop(1:numInd, :);
end



