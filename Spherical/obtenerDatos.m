function [ C ] = obtenerDatos(dir,i,f,index)

C = cell(40,1);
for j=i:f
    path = strcat(dir,'final',num2str(j),'.txt');
    da = importdata(path);
    lista = da.data();
    C{j+index} =lista;
end

end

