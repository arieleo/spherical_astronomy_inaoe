
clc;
clear all;

posCenters= [1 2 3 4 5 6 7];
options= [1 2 3 4 5 6];
% tuneValues= linspace( 1,3 , 10 );
tuneValues= 1;
equations= [5];
minError= inf;

for lll=1: length(equations)
for kkk=1: length(tuneValues)
for jjj=1: length(options)
for iii=1: length(posCenters)
    
    lll

validationFolds= 10; % Number of folds for validation test. 48 and 49 examples for .dat files
plateModel= equations(lll); % Degree of equation used.

option= options(jjj); % Using different leastsquare implementations. Default 1
tunevalue= tuneValues(kkk); % Default= 1.

    centerIndex= posCenters(iii);
% centerIndex= 42; % index of selected Ccenter point. Originally 51 for this example.
focal_length= 2158.8; %focal length value of Camera Schmidt in mm. 2158.8 +- 1.4

data2= importdata('plate_files/CoordNR-AC0359.dat'); % Plate data file -> For .dat files (CoordNR-AC0359.dat and CoordNR.dat)
[data2 falses]= eraseNAN(data2);
%data2= data.data; % Numerical data N x 6 culumns (RA, DEC, X_Std, Y_Std, X_Plate, Y_Plate)

data2(:,2:3)=degtorad(data2(:,2:3)); % Convertion RA and DEC to radians.

center= data2(centerIndex,4:5); % Central point of plate.
%data2(centerIndex, :)=[];          % Center point discarted from test.

numPerFold= floor(length(data2(:,1))/validationFolds); % Number of observations for each Fold.
histConstants= zeros(validationFolds, sum(linspace(1,plateModel+1,plateModel+1)));      %History for Constant plate model.
histError= zeros(validationFolds, 3);                   %History for folds error.
for ii=1: validationFolds
    data3= data2;
    
    objTest= [(numPerFold*(ii-1))+1:numPerFold*(ii)];
    test= data3(objTest,:);                             %Testing set
    
    data3(objTest, :)=[];                               %Training set
    
    TRA = test(:,2);                                    %Right ascension for tests
    TDE = test(:,3);                                    %Declination for tests
    [Txi Teta]= getStandardCoordinates(focal_length, center, [TRA TDE]);    %Ideal positions for Test
    Tx= test(:,4);                                      %Plate x positions for test
    Ty= test(:,5);                                      %Plate y positions for test
    
    DRA = data3(:,2);                                   %Right ascension for train Constants plate
    DDE = data3(:,3);                                   %Declination for train Constants plate
    [Dxi Deta]= getStandardCoordinates(focal_length, center, [DRA DDE]);    %Ideal positions for train Constants plate
    Dx= data3(:,4);                                     %Plate x positions for train Constants plate.
    Dy= data3(:,5);                                     %Plate y positions for train Constants plate.

    C= NModelConstantPlate([Dxi Deta], [Dx Dy], plateModel, option, tunevalue);
    
    [E POS]= testNPlateModels([Tx Ty], [TRA TDE], C, focal_length, center); %RMSE for estimated positions (AR, DE) in each Fold.
    histConstants(ii, :)= C;
    histError(ii, :)= E;

end

mesError= mean(histError);
if(mesError(3)< minError)
    minError= mesError(3)
    bestCenter= centerIndex;
    bestOption= option;
    bestTuneValue= tunevalue;
    bestDegree= plateModel;
end

end
end
end
end