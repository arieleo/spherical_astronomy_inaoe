function [Zx, Zy, Zxy, aU, aV, aUV]= testZernike(fx, fy, sz, trasl, l, mn)

% l= 1200;
L= ceil(l* sqrt(2));

X= linspace(-trasl(1), -trasl(1)+sz(1), L);
Y= linspace(-trasl(2), -trasl(2)+sz(2), L);

% X= linspace(-sz(1)/2,sz(1)/2, L);
% Y= linspace(-sz(2)/2,sz(2)/2, L);

[X,Y] = meshgrid(X,Y);
% Y=-Y;

co1 = coeffvalues(fx);
co2 = coeffvalues(fy);

U = co1(1) + co1(2)*X + co1(3)*Y + co1(4)*(X.^2) + co1(5)*(X.*Y) + co1(6)*(Y.^2) + co1(7)*(X.^2).*Y + co1(8)*(Y.^2).*X + co1(9)*(Y.^3);
V = co2(1) + co2(2)*X + co2(3)*Y + co2(4)*(X.^2) + co2(5)*(X.*Y) + co2(6)*(Y.^2) + co2(7)*(X.^2).*Y + co2(8)*(Y.^2).*X + co2(9)*(Y.^3);
UV = sqrt(U.^2 + V.^2);

% zbf=zernike_bf(L,mn);
% 
% v=zernike_mom(UV,zbf)
% 
% reconstructed=zernike_rec(v,L,zbf);


XR= -1:2/(L-1):1;
[x,y] = meshgrid(XR);
x = x(:); y = y(:);

[theta,r] = cart2pol(x,y);

N = []; M = [];
for n = 0:mn
    N = [N n*ones(1,n+1)];
    M = [M -n:2:n];
end

N= [0 1 1 2 2 2 3 3 3 3 4 4 4];
M= [0 -1 1 -2 0 2 -3 -1 1 3 -4 -2 0];

is_in_circle = ( r <= 1 );
Z = zernfun(N,M,r(is_in_circle),theta(is_in_circle));
% size(Z)

aU = Z\U(is_in_circle);
aV = Z\V(is_in_circle);
aUV = Z\UV(is_in_circle);

% aUV = [33.9992254532;
%     0.8241824861;
%     -4.0225854605;
%     0.0821358444;
%     16.6274485992;
%     -17.3483238166;
%     -5.1271376909;
%     1.9525509234;
%     -2.4123410175;
%     0.9150862639];
% 
% aU = [4.2020419816;
%     14.5388237372;
%     -1.6738148228;
%     -0.8681876198;
%     7.490230973;
%     -8.7077040298;
%     -9.2288908727;
%     18.9170167938;
%     -3.2617258156;
%     9.7851774469];
% 
% aV = [-2.9391071529;
%     -6.8652355706;
%     7.4550279175;
%     0.0970690153;
%     -1.7756565161;
%     11.1934975972;
%     11.9774699965;
%     -10.273821812;
%     1.7924546663;
%     -5.3773639988];

recU= NaN(size(U));
recV= NaN(size(V));
recUV= NaN(size(UV));

recU(is_in_circle)= Z*aU;
recV(is_in_circle)= Z*aV;
recUV(is_in_circle)= Z*aUV;

imsz= size(recUV);
rinic= round(imsz(1)/2 - l/2);
cinic= round(imsz(2)/2 - l/2);

Zx = recU(rinic:rinic+l-1, cinic:cinic+l-1);
Zy = recV(rinic:rinic+l-1, cinic:cinic+l-1);
Zxy= recUV(rinic:rinic+l-1, cinic:cinic+l-1);

Zx= meanNaN(Zx);
Zy= meanNaN(Zy);
Zxy= meanNaN(Zxy);

fig4=figure(5);
imshow(Zxy, colormap)

function [Nmat]= meanNaN(mat)
    Nmat= mat;
    [a b]= find(isnan(Nmat)==1);
    Nmat(a,b)=0;
    m= mean(mean(Nmat));
    for i=1: length(a)
        Nmat(a(i), b(i))= m;
    end
end

% 


end




