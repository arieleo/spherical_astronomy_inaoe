clc
clear
% Importamos los datos de las im�genes y los metemos a las ...
% variables AR, DEC, X y Y. Tambi�n definimos la escala de placa ps
path = 'plate_files';
placa = 'AC4943';
forma = 'Rotadas';
A=importdata([path,'/',placa,forma]);
AR=A(:,1);
DEC=A(:,2);
X=(A(:,3));
Y=A(:,4);
ps=1.508./3600;

auxraq=find(DEC<0);

[indices,~] = size(A);
angulos = linspace(200,250,1000);
minErr = inf;


ARR=deg2rad(AR);
DECC=deg2rad(DEC);

for indiceEstrella=1:indices
    es=indiceEstrella;
    centro=[X(es),Y(es)];
    
    xi=( cos(DECC).*sin(ARR-ARR(es)) )./ ( sin(DECC(es)).*sin(DECC) + ...
        cos(DECC(es).*cos(DECC).*cos(ARR-ARR(es))  ) );
    
    eta= ( cos(DECC(es)).*sin(DECC) - sin(DECC(es)).*cos(DECC).*cos(ARR-ARR(es))) ./ ...
        ( sin(DECC(es)).*sin(DECC) + cos(DECC(es).*cos(DECC).*cos(ARR-ARR(es))  ) );
    XI=rad2deg(xi);
    ETA=rad2deg(eta);
    xcat=(XI./ps)+X(es);
    ycat=ETA./ps+Y(es);
    
    for indexAngulo=1:length(angulos)
        
        theta = angulos(indexAngulo);
        rotados =  functionRotacion(X,Y,centro,theta);
        xr = rotados(1,:)';
        yr = rotados(2,:)';
        
        
        RMSEx= mean(sqrt((xr-xcat).^2));
        RMSEy= mean(sqrt((yr-ycat).^2));
        RMSE= sqrt(RMSEx^2 + RMSEy^2);
        if(RMSE<minErr)
            minErr= RMSE
            bestCenter= indiceEstrella;
            bestAng= theta;
            
            
            plot(xcat,ycat,'k*',xr,yr,'gs', centro(1),centro(2), 'bo');
            % plot(xcat,ycat,'b*',X,Y,'ro',Xc,Yc,'ks');
            
            axis square
            grid on
            grid minor
            
        end
        
        
    end
end











