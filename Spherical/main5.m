
clc;
clear all;


validationFolds= 10; % Number of folds for validation test. 48 and 49 examples for .dat files
plateModel= 6; % Degree of equation used.

option=1; % Using different leastsquare implementations. Default 1
tunevalue= 1; % Default= 1.

focal_length= 2158.8; %focal length value of Camera Schmidt in mm. 2158.8 +- 1.4

data2= importdata('plate_files/CoordNR-AC0359.dat'); % Plate data file -> For .dat files (CoordNR-AC0359.dat and CoordNR.dat)
[data2 falses]= eraseNAN(data2);
%data2= data.data; % Numerical data N x 6 culumns (RA, DEC, X_Std, Y_Std, X_Plate, Y_Plate)

data2(:,2:3)=degtorad(data2(:,2:3)); % Convertion RA and DEC to radians.

AR= data2(:,2);
DE= data2(:,3);

data2(:,4)= (data2(:,4)/60)-100; % x plate cordinates translated to center.
data2(:,5)= (data2(:,5)/60)-100; % y plate cordinates translated to center.

xmm= data2(:,4);
ymm= data2(:,5);

[CAR CDE] = findCenter(AR, DE, xmm, ymm, [0 0]);
[XI ETA]= getStandardCoordinates(focal_length,[CAR CDE], [AR DE]);

[xmmn, ymmn, bestAng, min_e]= getFirstAdj(xmm, ymm, XI, ETA, 0.1);

data2(:,4)= xmmn;
data2(:,5)= ymmn; % y plate cordinates translated to center.

E1= sqrt(mean((XI - xmmn).^2));
E2= sqrt(mean((ETA - ymmn).^2));
E3= sqrt(E1.^2 + E2.^2) % Initial Error.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% First Plot

figure(1)

scatter(xmmn, ymmn, 'r*');
hold on;
% scatter(xmmn, ymmn, 'r*');
% figure(2)
scatter(XI, ETA, 'b*');
% scatter(Xn, Yn, 'b*');
xlim([-100 100])
ylim([-100 100])
hold off;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

c = cvpartition(length(data2(:,1)),'KFold', validationFolds);

numPerFold= floor(length(data2(:,1))/validationFolds); % Number of observations for each Fold.
histConstants= zeros(validationFolds, sum(linspace(1,plateModel+1,plateModel+1)));      %History for Constant plate model.
histError= zeros(validationFolds, 3);                   %History for folds error.
all_pos=[];
for ii=1: validationFolds
    data3= data2;
    
    prev=0;
    if ii>1
        prev= sum(c.TestSize(1:ii-1));
    end
    
    objTest= [prev+1: prev+c.TestSize(ii)];
    test= data3(objTest,:);                             %Testing set
    
    data3(objTest, :)=[];                               %Training set
    
    TRA = test(:,2);                                    %Right ascension for tests
    TDE = test(:,3);                                    %Declination for tests
    [Txi Teta]= getStandardCoordinates(focal_length, [CAR CDE], [TRA TDE]);    %Ideal positions for Test
    Tx= test(:,4);                                      %Plate x positions for test
    Ty= test(:,5);                                      %Plate y positions for test
    
    DRA = data3(:,2);                                   %Right ascension for train Constants plate
    DDE = data3(:,3);                                   %Declination for train Constants plate
    [Dxi Deta]= getStandardCoordinates(focal_length,[CAR CDE], [DRA DDE]);    %Ideal positions for train Constants plate
    Dx= data3(:,4);                                     %Plate x positions for train Constants plate.
    Dy= data3(:,5);                                     %Plate y positions for train Constants plate.

    C= NModelConstantPlate([Dxi Deta], [Dx Dy], plateModel, option, tunevalue);
    
    [E POS]= testNPlateModels2([Tx Ty], [Txi Teta], C, focal_length, [CAR CDE]); %RMSE for estimated positions (AR, DE) in each Fold.
    all_pos= [all_pos; POS];
    histConstants(ii, :)= C;
    histError(ii, :)= E;

end

mesError= mean(histError);

meanConst= mean(histConstants);
[E POS]= testNPlateModels2([xmmn ymmn], [XI ETA], meanConst, focal_length, [CAR CDE]); %RMSE for estimated positions (AR, DE) in each Fold.
E

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Second Plot

figure(2)

scatter(POS(:,1), POS(:,2), 'g*');
hold on;
% scatter(xmmn, ymmn, 'r*');
% figure(2)
scatter(XI, ETA, 'b*');
% scatter(Xn, Yn, 'b*');
xlim([-100 100])
ylim([-100 100])
hold off;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
