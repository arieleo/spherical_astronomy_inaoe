clc
clear
% Importamos los datos de las im�genes y los metemos a las ...
% variables AR, DEC, X y Y. Tambi�n definimos la escala de placa ps
plate_name= 'AC0386';
plate_name2= 'AC1739Normal';

A=importdata(strcat('plate_files/', plate_name));
A(1,:)=[];
% A(:,3)=[];
[A falses]= eraseNAN(A);
falses

centers= [19];

angs= 3.9794;
% angs= 0.8183;
% angs= 0.8183 + 1.5708;

% wrongStars=[24 25 26 27 28 29 34 35]; %Center 36 After. AC0388
wrongStars=[23 25 26 27 28 29 30 32 33 41 42 43 44 45]; %Center 19 After wrong stars. AC0386
% wrongStars=[6 21 22 23 24 25 26 27 28 29 30 31 32 47 48 49 50]; %Center 13 After wrong stars. AC0382
% wrongStars=[1 20 21 48 49 50]; %Center 44 After wrong stars. AC0381
% wrongStars=[3 8 9 10 11 12 21 22 23 24 29]; %Center 26 After wrong stars. AC0387
% wrongStars=[11 15 16]; %Center 25 After wrong stars. AC0359
% wrongStars=[11]; %Center 28 After wrong stars. AC0366 *Without polar star
% wrongStars=[39]; %Center 44 After wrong stars. AC0398 -> AC0389 *Without polar star
% wrongStars=[3 24 25]; %Center 25 After wrong stars. AC0359

A(wrongStars,:)=[];

numStars= length(A(:,1));

DRA=A(:,1);
DDE=A(:,2);
X=A(:,3);
Y=A(:,4);

focal_length= 2158.8; %focal length value of Camera Schmidt in mm. 2158.8 +- 1.4
%plateModel=2;
option=2;
tunevalue=1;

%Pasamos las coordenadas AR y DEC a radianes
DRA=deg2rad(DRA);
DDE=deg2rad(DDE);

% ppmms= linspace(62.5,63.5,25);
% ppmms2= linspace(62.5,63.5,25);
% angs= deg2rad(linspace(227, 228, 15));

ppmms= 63.0417;
ppmms2= 63.125;

[theta, bo, bv]= estimateTheta(A, centers, ppmms, ppmms2, focal_length, angs);
%theta= angs

minErr= inf;
bestCenter=[];
bestPpmm=[];
bestAng=[];
bestPpmm2=[];
bestError=[];

centerIndex= centers;
ppmm= ppmms;
ppmm2= ppmms2;

center= [DRA(centerIndex) DDE(centerIndex)];

[Dxi Deta]= getStandardCoordinates(focal_length, center, [DRA DDE]);

% Dxi= Dxi* ppmm;
% Deta= Deta* ppmm2;

X= X/ppmm;
Y= Y/ppmm2;
centerXY= [X(centerIndex) Y(centerIndex)];
centerStd= [Dxi(centerIndex)-min(Dxi), Deta(centerIndex)-min(Deta)];

trasl= [Dxi(centerIndex)-centerXY(1), Deta(centerIndex)-centerXY(2)];

X2= X+trasl(1);
Y2= Y+trasl(2);

tform = affine2d([cos(theta) -sin(theta) 0; sin(theta) cos(theta) 0; 0 0 1]);
[X2,Y2] = transformPointsForward(tform,X2,Y2);

[Xsky Ysky]= getSkyPosition(focal_length, center, [X2 Y2]);

RMSEx= sqrt(mean((DRA-Xsky).^2));
RMSEy= sqrt(mean((DDE-Ysky).^2));
RMSE= sqrt(RMSEx^2 + RMSEy^2);

POS=[];
E=[];
histC=[];
for pm=1: numStars
    X3=X2;
    Y3=Y2;
    Dxib= Dxi;
    Detab= Deta;
    
    X3(pm)=[];
    Y3(pm)=[];
    Dxib(pm)=[];
    Detab(pm)=[];
    
    C=NineModelCP([Dxib Detab], [X3 Y3], option, tunevalue);
%     load('results/C5.mat');
%     C=C5;
    %[POS0 E0]= testNinePlateCoord([X2(pm) Y2(pm)], [Dxi(pm) Deta(pm)], C);
    [POS0 E0]= testNinePlateM([DRA(pm) DDE(pm)], [X2(pm) Y2(pm)], C, focal_length, center);
    [POS0(:,1) POS0(:,2)] = getStandardCoordinates(focal_length, center, POS0);
    
    histC(pm,:)= C';
    POS(pm,:)=POS0;
    E(pm,:)= E0;
    
end


if(RMSE<minErr)
    minErr= RMSE
    bestCenter= centerIndex;
    bestPpmm= ppmm;
    bestPpmm2= ppmm2;
    bestAng= theta;
    a= mean(histC);
    bestError= mean(E)
    
    %%%%%%% makeQuivers
    [fx0, fy0]= makeQuiver([Dxi Deta], [X2 Y2], centerStd, plate_name, 'Before constant-plate Adjustment', 'Error in mm', 2, [200 200]);
    [fx, fy]= makeQuiver([Dxi Deta], POS, centerStd, plate_name, 'After constant-plate Adjustment', 'Error in mm', 3, [200 200]);
    [Zx, Zy, Zxy, aU, aV, aUV]= testZernike(fx, fy, [250 250], centerStd, 1000, 3);
    [NPOS]= correctZernike(Zx, Zy, [250 250], centerStd, POS);
    [f1, f2]= makeQuiver([Dxi Deta], NPOS, centerStd, plate_name, 'After Zernike Adjustment', 'Error in mm', 4, [200 200]);
    
    
    figure(1);
    clf
%     p0= plot(Dxi,Deta,'ro', POS(:,1), POS(:,2), 'blacks', X2,Y2,'b*');
    p0= plot(Dxi,Deta,'ro', POS(:,1), POS(:,2), 'blacks', NPOS(:,1), NPOS(:,2), 'gs', X2,Y2,'b*');
    hold on;
    for i=1: numStars
        tex= num2str(i);
        p1=text(X2(i)+3,Y2(i),tex, 'Color','blue');
        p2=text(Dxi(i)+3,Deta(i),tex, 'Color','red');
        p3=text(POS(i,1)+3,POS(i,2),tex, 'Color','black');
        p4=text(NPOS(i,1)+3,NPOS(i,2),tex, 'Color','green');
%         text(X2(i),Y2(i),tex, 'Color','blue');
        plot([X2(i) Dxi(i)], [Y2(i) Deta(i)], 'Color', 'blue');
        plot([POS(i,1) Dxi(i)], [POS(i,2) Deta(i)], 'Color', 'black');
        plot([NPOS(i,1) Dxi(i)], [NPOS(i,2) Deta(i)], 'Color', 'green');
    end
    axis square
    grid on
    grid minor
    legend([p0], 'Estimated Xi and Eta', 'Plate Constant Model Adjustment', 'Zernike Adjustment', 'Plate coordinates (Scaled and rotated)');
    hold off;
    pause(0.01);

end

[ZRA ZDE]= getSkyPosition(focal_length, center, NPOS);

Z_RMSEx= sqrt(mean((ZRA-DRA).^2));
Z_RMSEy= sqrt(mean((ZDE-DDE).^2));
Z_RMSE= sqrt(Z_RMSEx^2 + Z_RMSEy^2)
