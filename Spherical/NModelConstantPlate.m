function [C]= NModelConstantPlate(ideal, standard, N, op, tun)

%Author: Ariel Ortiz
%September 2016

% Extracted from [2] Sistema para reducción de posiciones de estrellas en
% placas astronómicas, Salazar María-Guadalupe.
% Evaluation using ecuatorial coordinates.

% Input:
%   ideal: Ideal coordinates set. Matrix N x 2. Each row contains xi and
%       eta values for each observation according to getStandardCoordinates
%       function.
%   standard: Measured coordinates taken from plates. Size N x 2. Each row
%       contains x and y values for each visual observation.

% Output:
%   C: Vector containing each value for constansts plate. According to ten
%       model constant plate extracted from [2] p. 35.
%       Solution for Constants plate [a_0 - a_9].

% System form:
%   x - xi = a_0 + a_1*x + a_2*y + a_3*x^2 + a_4*x*y + a_5*y^2 +
%           a_6*x^3 + a_7*x^2*y + a_8*x*y^2 + a_9*y^3

%   y - eta = a_0 + a_1*y + a_2*x + a_3*y^2 + a_4*x*y + a_5*x^2 +
%           a_6*y^3 + a_7*y^2*x + a_8*x^2*y + a_9*x^3

if (length(ideal(:,1))>= N/2) % At least N/2 observations

x= standard(:,1);
y= standard(:,2);

xi= ideal(:,1);
eta= ideal(:,2);

eq1=[];
eq2=[];
for ii=0: N
    g= linspace(ii,0,ii+1);
    g2= linspace(0,ii,ii+1);
    for jj=1: length(g)
        eq1=[eq1, (x.^(g(jj))).*(y.^(g2(jj)))];
        eq2=[eq2, (y.^(g(jj))).*(x.^(g2(jj)))];
    end
    
end

res1= x-xi;
res2= y-eta;

A= zeros(length(ideal(:,1))*2, length(eq1(1,:)));
b= zeros(length(ideal(:,1))*2, 1);
con=1;
for i=1:2:length(A(:,1))
    A(i,:)= eq1(con,:);
    A(i+1,:)= eq2(con,:);
    b(i)= res1(con);
    b(i+1)= res2(con);
    con=con+1;
end

if(op==1)
C= leastSquares(A,b);
else
C= solveUsingMatlab(A,b, op, tun);
end

end