clc;
clear all;
close all;

focal_length= 2159.1535353535; %focal length value of Camera Schmidt in mm. 2158.8 +- 1.4

data= importdata('plate_files/AC0358'); % Plate data file

AR= deg2rad(data(:,1));
DE= deg2rad(data(:,2));
x= data(:,4);
y= data(:,5);

xmm= (x/60)-100;
ymm= (y/60)-100;

[Car Cde]= findCenter(AR, DE, xmm, ymm, [0 0]);

[X Y]= getStandardCoordinates(focal_length, [Car Cde], [AR DE]);
[Xn Yn]= getStandardCoordinates(focal_length, [Car Cde], [0 1.5708]);

[xmmn, ymmn, bestAng, min_e]= getFirstAdj(xmm, ymm, X, Y, 0.1);


figure(1)

scatter(xmmn, ymmn, 'r*');
hold on;
% scatter(xmmn, ymmn, 'r*');
% figure(2)
scatter(X, Y, 'b*');
% scatter(Xn, Yn, 'b*');
xlim([-100 100])
ylim([-100 100])
hold off;

[fx0, fy0]= makeQuiver([X Y], [xmmn ymmn], [100,100], 'AC0358', 'With best Angle', 'Error in mm', 2, [200 200])

% [Xc Yc]= getStandardCoordinates(focal_length, [Car Cde], [Car Cde])
%