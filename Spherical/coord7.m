clc
clear
% Importamos los datos de las im�genes y los metemos a las ...
% variables AR, DEC, X y Y. Tambi�n definimos la escala de placa ps
A=importdata('plate_files/AC0389')
A(1,:)=[];
[A falses]= eraseNAN(A);

numStars= length(A(:,1));

DRA=A(:,1);
DDE=A(:,2);
X=A(:,3);
Y=A(:,4);

focal_length= 2158.8; %focal length value of Camera Schmidt in mm. 2158.8 +- 1.4

%Pasamos las coordenadas AR y DEC a radianes
DRA=deg2rad(DRA);
DDE=deg2rad(DDE);

centers= [44];
ppmms= linspace(62.8,63.4,60);
ppmms2= linspace(62.8,63.4,60);
% ppmms= 63.2143;
angs= deg2rad(linspace(47, 47.5, 60));

minErr= inf;
bestCenter=[];
bestPpmm=[];
bestAng=[];
bestPpmm2=[];
for iii= 1: length(centers)
    iii
for jjj= 1: length(ppmms)
for lll= 1: length(ppmms2)
for kkk= 1: length(angs)
%     iii

centerIndex= centers(iii);
ppmm= ppmms(jjj);
ppmm2= ppmms2(lll);
theta= angs(kkk);

center= [DRA(centerIndex) DDE(centerIndex)];
centerXY= [X(centerIndex) Y(centerIndex)];

[Dxi Deta]= getStandardCoordinates(focal_length, center, [DRA DDE]);

Dxi= Dxi* ppmm;
Deta= Deta* ppmm2;

trasl= [Dxi(centerIndex)-centerXY(1), Deta(centerIndex)-centerXY(2)];

X2= X+trasl(1);
Y2= Y+trasl(2);

% X2= X2*cos(tetha)-Y2*sin(tetha);
% Y2= X2*sin(tetha)+ Y2*cos(tetha);

tform = affine2d([cos(-theta) -sin(-theta) 0; sin(-theta) cos(-theta) 0; 0 0 1]);
[Dxi,Deta] = transformPointsForward(tform,Dxi,Deta);

RMSEx= mean(sqrt((Dxi-X2).^2));
RMSEy= mean(sqrt((Deta-Y2).^2));
RMSE= sqrt(RMSEx^2 + RMSEy^2);

if(RMSE<minErr)
    minErr= RMSE
    bestCenter= centerIndex;
    bestPpmm= ppmm;
    bestPpmm2= ppmm2;
    bestAng= theta;
    
    figure(1);
%     clf
    plot(X2,Y2,'b*',Dxi,Deta,'ro');
    hold on;
    for i=1: numStars
        tex= num2str(i);
        text(X2(i)+10,Y2(i),tex, 'Color','blue');
        text(Dxi(i)+10,Deta(i),tex, 'Color','red');
%         text(X2(i),Y2(i),tex, 'Color','blue');
        plot([X2(i) Dxi(i)], [Y2(i) Deta(i)], 'Color', 'cyan');
    end
    hold off;
    
end



end
end
end
end

