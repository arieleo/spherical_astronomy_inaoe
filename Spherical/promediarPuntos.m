function [ C,C2 ] = promediarPuntos( puntos )

for k = 1 : 41
    
    x = 0;
    y = 0;
    
    for j=1:numel(puntos)
        promediar = puntos{j};
        x = x + promediar(k,1);
        y = y + promediar(k,2);
    end
    
    x = x/40;
    y = y/40;
    
    C(k,1) = x;
    C(k,2) = y;
end

for k = 1 : 41
    
    vx = 0;
    vy =0;
    promX = C(k,1);
    promY = C(k,2);
    
    for j=1:numel(puntos)
        punto = puntos{j};
        
        px = punto(k,1);
        py = punto(k,2);
        
       
        vx = vx +(px- promX).^2;
        vy = vy +(py- promY).^2;
    end

    vx = vx/(j-1);
    vy = vy/(j-1);
    
    C2(k,1) = vx;
    C2(k,2) = vy;
end




end

