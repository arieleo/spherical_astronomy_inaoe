function [Pos, E]= testNinePlateM(realPos, plateCord, C, f, center)

xR= plateCord(:,1);
yR= plateCord(:,2);

AR0= realPos(:,1);
DE0= realPos(:,2);

xi= C(1)*xR + C(2)*yR + C(3) + C(5)*xR + C(6)*yR + C(7)*xR.^2 + C(8)*xR.*yR + C(9)*xR.*(xR.^2 + yR.^2);
eta=-C(2)*xR + C(1)*yR + C(4) + C(5)*xR + C(6)*xR - C(5)*yR + C(7)*xR.*yR + C(8)*yR.^2 + C(9)*yR.*(xR.^2 + yR.^2);

[AR, DE]= getSkyPosition(f, center, [xi eta]);

E1= sqrt(mean((AR0-AR).^2));
E2= sqrt(mean((DE0-DE).^2));
E3= sqrt(E1.^2 + E2.^2);

E   = [E1 E2 E3]; % Using Root mean square error (RMSE) for Right ascension, Declination and general error.
Pos = [AR DE]; % Estimated Right ascension and Declination according to selected plate model.


