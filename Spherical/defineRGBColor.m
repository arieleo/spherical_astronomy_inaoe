function [color]= defineRGBColor(raz)

%        r= (log10(raz+0.1)+1)/(log10(1+0.1)+1);
       r= 2^(raz)-1;
       b= normpdf(raz,0.5,0.22)/normpdf(0.5,0.5,0.22);
       g= 1-(2^(raz)-1);
%        g= 1-((log10(raz+0.1)+1)/(log10(1+0.1)+1));

color= [r g b];