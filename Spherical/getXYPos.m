function [Xc, Yc]= getXYPos(X,Y,RA,DEC, point, degree)
    
poly= strcat('poly', num2str(degree), num2str(degree));

fitX= fit([RA,DEC],X, poly);
fitY= fit([RA,DEC],Y, poly);

Xc= fitX(point(1), point(2));
Yc= fitY(point(1), point(2));

end