clc
clear
% Importamos los datos de las im�genes y los metemos a las ...
% variables AR, DEC, X y Y. Tambi�n definimos la escala de placa ps
A=importdata('PLACA AC0388.txt');
AR=A(:,2);
DEC=A(:,3);
X=(A(:,4));
Y=A(:,5);
ps=1.508./3600;

auxraq=find(DEC<0);

% Elegimos la estrella del centro y encontramos sus coordenadas X y Y
es=45;
Xc=X(es);
Yc=Y(es);

%Pasamos las coordenadas AR y DEC a radianes
ARR=deg2rad(AR);
DECC=deg2rad(DEC);

% Calculamos la proyecci�n de cada estrella del cielo a la placa
% Solo como referencia
xi=( cos(DECC).*sin(ARR-ARR(es)) )./ ( sin(DECC(es)).*sin(DECC) + ...
    cos(DECC(es).*cos(DECC).*cos(ARR-ARR(es))  ) );

eta= ( cos(DECC(es)).*sin(DECC) - sin(DECC(es)).*cos(DECC).*cos(ARR-ARR(es))) ./ ...
    ( sin(DECC(es)).*sin(DECC) + cos(DECC(es).*cos(DECC).*cos(ARR-ARR(es))  ) );
XI=rad2deg(xi);
ETA=rad2deg(eta);
xcat=(XI./ps)+X(es); % --> Coordenadas del cat�logo proyectadas a la placa,
ycat=ETA./ps+Y(es);  %     centradas y escaladas.

%________________________________________________________________________
% Rotar
theta1 = deg2rad(0);
theta2 = deg2rad(47.35);
[Xcat_r, Ycat_r]=rotar(Xc, Yc, xcat, ycat, theta1);
[xr, yr]=rotar(Xc, Yc, X, Y,theta2);
% 
% % Mirrors
% %________________________________________________________________________
xr=(xr-Xc).*-1;
xr=xr+Xc;
yr=(yr-Yc).*-1;
yr=yr+Yc;
% % 
% %________________________________________________________________________
% % Derrotar
% theta2 = deg2rad(-227.71);
% [Xcat_r, Ycat_r]=rotar(Xc, Yc, xcat, ycat,-theta1);
% [xr, yr]=rotar(Xc, Yc, xr, yr,theta2);
% 
% % auxraq1=find(xr<0);
% % yr(auxraq1)=yr(auxraq1)-Yc; %desplazamos al cero
% % yr(auxraq1)=yr(auxraq1).*(-1); % Espejeamos en Y
% % yr(auxraq1)=yr(auxraq1)+Yc; %desplazamos al valor original otra vez
% 
% %________________________________________________________________________
% 
plot(Xcat_r,Ycat_r,'k*',xr,yr,'gs', xcat(es), ycat(es), 'bo',Xc,Yc,'bo');
% plot(xcat,ycat,'b*',X,Y,'ro',Xc,Yc,'ks');

axis square
grid on
grid minor