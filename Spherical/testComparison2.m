clc;
clear all;

validationFolds= 78; % Number of folds for validation test
plateModel= 13; % Degree of equation used. <- Best Results using 13

option= 1; %Using different leastsquare implementations. Default 1
tunevalue= 0.01; % Default= 1.

centerIndex= 51; % index of selected Ccenter point. Originally 51 for this example.
focal_length= 2159.1535353535; %focal length value of Camera Schmidt in mm. 2158.8 +- 1.4

data= importdata('plate_files/ac1212.txt'); % Plate data file
data2= data.data; % Numerical data N x 6 culumns (RA, DEC, X_Std, Y_Std, X_Plate, Y_Plate, scale factor_eX, scale factor_ey ...)
                    %Using indexes 13, 14 in order to obtain estimated
                    %plate coordinates according to [Scale method]
                    
data2(:,1:2)=degtorad(data2(:,1:2)); % Convertion RA and DEC to radians.
% size(data2)

plateCoords= data2(:,5:6);
estimatedCoords= data2(:,14:15);

center= data2(centerIndex,1:2); % Central point of plate.
%data2(centerIndex, :)=[];          % Center point discarted from test.

numPerFold= floor(length(data2(:,1))/validationFolds); % Number of observations for each Fold.
histConstants= zeros(validationFolds, sum(linspace(1,plateModel+1,plateModel+1)));      %History for Constant plate model.
histError= zeros(validationFolds, 3);                   %History for folds error.
%histPos= zeros(validationFolds, 2);                     %Histogram for Calculated plate positions.
histPos=[];

for ii=1: validationFolds

    data3= data2;
    
    objTest= [(numPerFold*(ii-1))+1:numPerFold*(ii)];
    test= data3(objTest,:);                             %Testing set
    
    %data3(objTest, :)=[];                               %Training set

    DRA = data3(:,1);                                   %Right ascension for train Constants plate
    DDE = data3(:,2);                                   %Declination for train Constants plate
    [Dxi Deta]= getStandardCoordinates(focal_length, center, [DRA DDE]);    %Ideal positions for train Constants plate
    Dx= data3(:,5);                                     %Plate x positions for train Constants plate.
    Dy= data3(:,6);                                     %Plate y positions for train Constants plate.

    C= NModelConstantPlate([Dx Dy], [Dxi Deta], plateModel, option, tunevalue);
    
    TRA = test(:,1);                                    %Right ascension for tests
    TDE = test(:,2);                                    %Declination for tests
    [Txi Teta]= getStandardCoordinates(focal_length, center, [TRA TDE]);    %Ideal positions for Test
    Tx= test(:,5);                                      %Plate x positions for test
    Ty= test(:,6);                                      %Plate y positions for test
    
    [POS E]= testNPlateCoordinates([Txi Teta], [Tx Ty], C);
   
    histConstants(ii, :)= C;
    histError(ii, :)= E;
    histPos=[histPos; POS];
%     histPos(ii,:)= POS;

end
ERR=mean(histError)

