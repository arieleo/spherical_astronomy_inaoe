function [ dist ] = dist2dots( p0,p1 )

x0 = p0(1);
y0 = p0(2);


x1 = p1(1);
y1 = p1(2);


X = x1 - x0;
X = X^2;
Y = y1 - y0;
Y = Y^2;

SUMA = X+Y;

dist = sqrt(SUMA);


end

