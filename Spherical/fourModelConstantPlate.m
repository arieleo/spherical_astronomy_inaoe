function [C] = fourModelConstantPlate(ideal, standard, op, tun)

%Author: Ariel Ortiz
%September 2016

% Extracted from [1] Computational Spherical Astronomy, Laurence G. Taff
% Evaluation using ecuatorial coordinates.

% Input:
%   ideal: Ideal coordinates set. Matrix N x 2. Each row contains xi and
%       eta values for each observation according to getStandardCoordinates
%       function.
%   standard: Measured coordinates taken from plates. Size N x 2. Each row
%       contains x and y values for each visual observation.

% Output:
%   C: Vector containing each value for constansts plate. According to four
%       model constant plate extracted from [1] p. 125.
%       Solution for Constants plate [a - d].

% System form:
%   x - xi = a*x + b*y + c
%   y - eta = -b*x + a*y + d


if (length(ideal(:,1))>= 2) % At least 2 observations

A= zeros(length(ideal(:,1))*2, 4);
b= zeros(length(ideal(:,1))*2, 1);

for i= 1: length(ideal(:,1))
    eq1= [standard(i,1), standard(i,2), 1, 0];
    res1= standard(i,1)-ideal(i,1);
    
    eq2= [standard(i,2), -standard(i,1), 0, 1];
    res2= standard(i,2)- ideal(i,2);
    
    A= [A; eq1; eq2];
    b= [b; res1; res2];
end

if(op==1)
C= leastSquares(A,b);
else
C= solveUsingMatlab(A,b, op, tun);
end

end
