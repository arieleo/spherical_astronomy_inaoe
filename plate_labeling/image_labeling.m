function labelling_images
clc;
clear all;
% close all;

plate_name= 'PLACAS-DIRECTAS/ST8076_210414';

plate_name_scaled= strcat(plate_name,'_scaled');

global fig;
fig = 1;
global image;
image = imread(strcat(plate_name_scaled, '.tif'));
global rad;
rad= 25;
global lng;
lng= 8;
global scale;
scale= 2;

file_name= strcat(plate_name, '.txt');

stars=[];

ok= false;
while(~ok)
printImage(stars);
k = waitforbuttonpress;
k=get(gcf,'CurrentCharacter');
switch k
    case 'n'
        disp('Add a new point for start');
        stars= addPoint(stars);
    case 'e'
        disp('Erasing last start');
        stars= erasePoint(stars);
    case 'f'
        disp('Finished labelling...');
        ok= true;
        stars
        dlmwrite(file_name,stars,'delimiter','\t');
        figure(fig);
        saveas(gcf,strcat(plate_name, '_labelled.tif'));
        close all;
    otherwise
        disp('Wrong key...');
end
pause(0.1);
end

function printImage(stars)
    figure(fig);
%     clf;
    imshow(image);
%     size(image)
    [h, w, dh]= size(image);
    cx= w/2;
    cy= h/2;
    hold on;
%     plot(cx,cy, 'bo');
    for ii=1:size(stars,1)
        x= stars(ii,2)/scale;
        y= stars(ii,3)/scale;
        
        ang= atan2(cy-y, cx-x);
        text_x= x+(rad*lng*cos(ang));
        text_y= y+(rad*lng*sin(ang));
        
        plot(x,y,'ro');
        viscircles([x y], abs(rad),'Color','r','LineWidth', 1);
        text(text_x,text_y,num2str(ii),'FontSize',14,'Color','red');
    end
    hold off;
end

function[list1]= erasePoint(list0)
    if(size(list0,1)>0)
        list0(end,:)=[];
    end
    list1= list0;
end
   
   
function[list1]= addPoint(list0)
    finished= false;
    if(length(list0)==0)
        list1= zeros(1,3);
    else
        list1= zeros(size(list0,1)+1, size(list0,2));
        list1(1:end-1,:)=list0;
    end
    
    cont= size(list1,1);
    
    disp('Select position of new start. Press q to quit.');
    
    add= false;
    
    while(~finished)
        if(waitforbuttonpress==0)
            disp('Added point.');
            C = get (gca, 'CurrentPoint');
            px= C(1,1);
            py= C(1,2);
            list1(end,:)= [cont px*scale, py*scale];
            add=true;
            finished= true;
        elseif(waitforbuttonpress==1)
            finished= true;
            disp('Done.');            
        end
    end
    if(~add)
        list1=list0;
    end
end

end