function [ExtremosDigitales]=obtenerExtremosFisicosSingle(xyn)
%% OBTENIENDO LOS EXTREMOS DIGITALES
oX=ordenar(xyn,'X');
oY=ordenar(xyn,'Y');
ExtremosDigitales=[oX(1,3),oX(end,3),oY(1,3),oY(end,3)];
end