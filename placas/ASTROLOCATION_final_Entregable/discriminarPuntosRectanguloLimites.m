function [newXYFisicos,newXYDigitales,newXYEstandar] = discriminarPuntosRectanguloLimites(XYFisicos,XYDigitales,XYEstandar, nuevosExtFis)
%% MINIMOS EN X
minX=nuevosExtFis(1,1); 
maxX=nuevosExtFis(2,1);
%% MINIMOS EN Y
minY=nuevosExtFis(3,2);
maxY=nuevosExtFis(4,2);

%% GENERAR ARREGLO DE PUNTOS A ELIMINAR
[f,~]=size(XYFisicos);
arrPosEliminar=[0];
arrPosSeQuedan=[0];
for ii=1 : f
    if (minX<=XYFisicos(ii,1)) && (maxX>=XYFisicos(ii,1)) && (minY<=XYFisicos(ii,2)) && (maxY>=XYFisicos(ii,2))
        arrPosSeQuedan=[arrPosSeQuedan,XYFisicos(ii,3)]; %% ARREGLO DE POSICIONES A ELIMINAR
    else
        arrPosEliminar=[arrPosEliminar,XYFisicos(ii,3)]; %% ARREGLO DE POSICIONES A ELIMINAR
    end % if
end % for
% % % disp (arrPosEliminar);
%% ELIMINANDO POSICIONES
newXYFisicos = eliminaPosicionMatriz(XYFisicos, arrPosEliminar);
newXYDigitales= eliminaPosicionMatriz(XYDigitales, arrPosEliminar);
newXYEstandar= eliminaPosicionMatriz(XYEstandar, arrPosEliminar);
%% RECORTANDO IMAGEN
% newIMAGEN=IMAGENPLACA(minY:maxY,minX:maxX); %% 
% figure(8),title('IMAGEN RECORTADA'),imshow(newIMAGEN), hold on;

end %% function