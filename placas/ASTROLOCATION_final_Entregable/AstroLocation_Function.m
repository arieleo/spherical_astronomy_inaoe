function [XYDigitales,fEsca,fDesp, ErrorProm] = AstroLocation_Function(XYFisicos,XYDigitales,IMAGEN, bandMedFis, multiFigu,bandEspX, bandEspY)
%% CALCULANDO EXTREMOS 
[ExtremosFisicos]=obtenerExtremosFisicosYDigitales(XYFisicos,XYDigitales,bandMedFis); %% LLAMANDO A LA FUNCION QUE OBTIENE LOS EXTREMOS 

%% CALCULANDO NUEVOS PUNTOS DIGITALES Y GRAFICANDOLOS
[XYDigitales,fEsca,fDesp]=ubicarPuntosEnPlaca(XYDigitales,IMAGEN,ExtremosFisicos,true,multiFigu, bandEspX, bandEspY);   %% ESCALA, ESPEJEA Y DESPLAZA LOS PUNTOS AL CENTRO DE LOS EXTRMOS FISICOS

%% GRAFICANDO PUNTOS FISICOS
graficarPuntos(XYFisicos,' ', '*b'); %% GRAFICA PUNTOS FISICOS

%% CALCULANDO EL ERROR FISICOS-DIGITALES
XYDigitales=eliminaPosicionMatriz(XYDigitales, [0]); %% ELIMINANDO LA POSICION 0 QUE ES EL CENTRO AGREGADO POR ESTETICA
[f1,c1]=size(XYDigitales);
[f2,c2]=size(XYFisicos);

XYDigitales=ordenarMat3d(XYDigitales,'Z');
XYFisicos=ordenarMat3d(XYFisicos,'Z');

if (f1==f2 && c1==c2)
    Error=XYFisicos-XYDigitales;
    ErrorProm=mean(abs(Error));
    fprintf('****** Error promedio en X= %5.3f   Y= %5.3f   NUMERACION= %d ******\n',ErrorProm(1),ErrorProm(2),ErrorProm(3));
    title(strcat(num2str(f1),'Estrellas---Error promedio en X=',num2str(ErrorProm(1)),', Y=',num2str(ErrorProm(2)),', Eti=',num2str(ErrorProm(3)))); %% CAMBIANDO TITULO PARA FIGURA 1
    fprintf('\n');
    
    %% DEMOSTRACION GRAFICA DEL ERROR CON COLORES
    ErrorABS=representaErrorFisDig(XYFisicos,XYDigitales,multiFigu,''); %% CREA Y GRAFICA FIGURA 3
else
    warning('¡¡LOS TAMAÑOS NO COINCIDEN!!');
end % if
fprintf('%s ... OK \n');
end