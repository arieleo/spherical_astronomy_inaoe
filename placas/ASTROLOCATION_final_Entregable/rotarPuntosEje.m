function [XYprima]=rotarPuntosEje(XY,ejeDigital,angulo)
% function [XYprima]=rotarPuntosEje(XY,ejeDigital,angulo,sentido)

% if (sentido=='-')
%     angulo=-1*angulo;
% end

[f,c]=size(XY);
XYprima=zeros(f,c);

xc=ejeDigital(1);
yc=ejeDigital(2);

if (c>3) %% PARA 3Dyc+((x-xc)*sind(angulo))+((y-yc)*cosd(angulo))
    zc=ejeDigital(3);
    warning('ROTACION 3D.');
end %% if

for i=1:f   
    x=XY(i,1);
    y=XY(i,2);
    
    if (c>3) %% PARA 3D
        z=XY(i,3);
        eti=XY(i,4);
        
        xp=xc+((x-xc)*cosd(angulo))-((y-yc)*sind(angulo));
        yp=yc+((x-xc)*sind(angulo))+((y-yc)*cosd(angulo));
        zp=z;
        
        XYprima(i,1)=xp;
        XYprima(i,2)=yp;
        XYprima(i,3)=zp;
        XYprima(i,4)=eti;
    else 
        eti=XY(i,3);    
        
        xp=xc+((x-xc)*cosd(angulo))-((y-yc)*sind(angulo));
        yp=yc+((x-xc)*sind(angulo))+((y-yc)*cosd(angulo));
        
        XYprima(i,1)=xp;
        XYprima(i,2)=yp;
        XYprima(i,3)=eti;
    end %% if 
end %for


end %function