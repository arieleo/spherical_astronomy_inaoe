%%*****************************************************************************************
% Funcion dezplazar los puntos en respecto al centro de la placa.
% 
% Developer: 
% Date:  Mayo 2016
% inputs: 
%           XYN:                        Matriz con datos con numero de estrella.
%           pCentroFisico:                  Centro de la imagen (x,y).
% outputs:
%           nXY:                        Matriz con los puntos escalados.
%           fDesplazamiento:            Factor desplazamiento.
%************************************************************************************

function [nXY,fDesplazamiento]=desplazarPuntosCentroFisico(XYN,pCentroFisico,pCentroDigi)
%% CENTRO IMAGEN
xc=pCentroFisico(1);
yc=pCentroFisico(2);

% centroDigi=obtenerCentroConjPuntos(XYN);
xd=pCentroDigi(1);
yd=pCentroDigi(2);
 
    %% OBTENIENDO FACTOR DE DESPLAZAMIENTO
    xDif=(xd)-xc;
    yDif=(yd)-yc;
       
    fDesplazamiento=[xDif,yDif];
    
% % %     fprintf('==================== Desplazamiento ====================\n');
% % %     fprintf('CENTRO FISICO--> X: %d   Y:%d \n', xc,yc);
% % %     fprintf('CENTRO DIGITAL--> X: %d   Y:%d \n', xd,yd);
% % %     fprintf('****Desplazamiento: X= %5.0f \t Y=%5.0f.**** \n',xDif,yDif);
% % %     fprintf('=========================================================\n');  
% % %     
    %% APLICANDO EL DESPLAZAMIENTO
    [f,~]=size(XYN);
        nXY=zeros(f,3);
        for i=1:f
            nXY(i,1)=XYN(i,1)-xDif;
            nXY(i,2)=XYN(i,2)-yDif;
            nXY(i,3)=XYN(i,3);
        end %for

end
