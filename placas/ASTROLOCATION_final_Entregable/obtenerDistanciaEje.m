%%*****************************************************************************************
% Funcion para obtener la distancia y el valor minimo y maximo en el eje
% especificado
% 
% Developer: 
% Date:  Mayo 2016
% inputs: 
%           XYN:               Matriz con datos con numero de estrella.
%           eje:               Eje por al cual se quiere obtener la distancia (X o Y)    
% outputs:
%           distancia:         Distancia entre el eje de interes.
%           minimo:            Valor minimo del eje.
%           maximo:            Valor maximo del eje.
%************************************************************************************
function [distancia, minimo, maximo]=obtenerDistanciaEje(XYN,eje)
if (eje=='y' || eje=='Y')
    mOrdenada=ordenarMat3d(XYN,eje);
    minimo=mOrdenada(1,2); val1=mOrdenada(1,1);numEst1=mOrdenada(1,3);
    maximo=mOrdenada(end,2); val2=mOrdenada(end,1);numEst2=mOrdenada(end,3);
    
else %(eje=='x' || eje=='X')
    mOrdenada=ordenarMat3d(XYN,eje);
    minimo=mOrdenada(1,1); val1=mOrdenada(1,2);numEst1=mOrdenada(1,3);
    maximo=mOrdenada(end,1); val2=mOrdenada(end,2);numEst2=mOrdenada(end,3);
end    
    %%%distancia=sqrt(((minimo-maximo)^2)+((val1-val2)^2));  %% OBTIENE LA DISTANCIA DEL MENOR Y MAYOR EN X - POLIGONOS CALCULADOS (excel)  
    distancia=(maximo-minimo);  %% OBTIENE LA DISTANCIA DEL MENOR Y MAYOR EN X - POLIGONOS CALCULADOS (excel)
        
% %     fprintf('#### Ordenados respecto %s maximo: %d (%2.0f)    minimo: %d (%2.0f)\t',eje,maximo,numEst2,minimo,numEst1);
% %     fprintf('%d - (%d) = %d\n',maximo,minimo,distancia);
    
end %function