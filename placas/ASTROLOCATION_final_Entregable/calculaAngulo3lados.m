function [A,B,C]=calculaAngulo3lados(aaa,bbb,ccc)
%% ANGULOS
A=acosd(((aaa^2)-(bbb^2)-(ccc^2))/(-2*bbb*ccc));

B=acosd(((bbb^2)-(aaa^2)-(ccc^2))/(-2*aaa*ccc));

C=acosd(((ccc^2)-(aaa^2)-(bbb^2))/(-2*aaa*bbb));

end