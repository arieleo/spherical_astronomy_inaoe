function [ResultadosFinales] =AstroLocation_Main(NOMBREPLACA,dirRaiz,imgPath,nar,ndec,puntoTang,bandFisicos,archivoPunFis,ExtremosFisicos,filesDataStars,bandMostrarTodGraf,fileVar)
clc
%%**************************************************************************
% ------------------------------ASTROLOCATION------------------------------
% PROGRAMA PARA LOCALIZAR ESTRELLAS (ESPECTROS) EN UNA IMAGEN UTILIZANDO
% COORDENADAS ECUATORIALES PARA TRANSFORMARLAS A COORDENADAS XY, DICHAS
% COORDENAS RESULTANTES SON SOBREPUESTAS EN LA IMAGEN PARA LOCALIZAR CADA
% ESTRELLA INDIVIDUALMENTE.
% 
% Developer: Luis González Guzmán
% E-mail: luisiiyoo@gmail.com
% Date:  Mayo-Julio 2016
%***************************************************************************
%% LIMPIANDO EJECUCIONES ANTERIORES
if (exist(strcat(dirRaiz ,'AstroLocation_Main_Console.txt'), 'file')~=0)
    delete(strcat(dirRaiz ,'AstroLocation_Main_Console.txt'))
end
% clc;
% clear;
% close all
% fprintf('*** CREANDO ARCHIVO:  %s ***\n',strcat(dirRaiz ,'AstroLocation_Main_Console.txt'));
diary(strcat(dirRaiz ,'AstroLocation_Main_Console.txt'));
fprintf('°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°\n');
fprintf('//////////////////////// ASTROLOCATION \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ \n');
fprintf('°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°\n\n');
tic; %% INICIO DE MEDICION DE TIEMPO DE EJECUCION
%%  #################################################  VARIABLES DE CONTROL #################################################
% °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
% NOMBREPLACA='ST0685'; %% NOMBRE DE LA PLACA. OJO ALTERA VARIABLES!!!!!!! (SIGAN EL ESTANDAR DE NOMBRAR LOS ARCHIVOS EN DATASTARS)
% dirRaiz=strcat('/home/luis/Descargas/ProgramasMatlab/ASTROLOCATION/DataStars_',NOMBREPLACA,'/'); %% DIRECTORIO RAIZ DE LA INFORMACION E IMAGEN
% 
% imgPath=strcat(dirRaiz,NOMBREPLACA,'.tif'); %% RUTA+IMAGEN
% nar=9;      %% COLUMNA DEL EXCEL PARA OBTENER LA ASCENSION RECTA
% ndec=10;    %% COLUMNA DEL EXCEL PARA OBTENER LA DECLINACION 
% puntoTang=51; %% Punto tangente!!!!
% bandFisicos=true;   %% ¿UTILIZAR TODOS LOS PUNTOS FISICOS? (FALSE SOLO UTILIZA LOS 4 EXTREMOS FISICOS)
%         archivoPunFis=strcat(dirRaiz,NOMBREPLACA,'_FISICOS.xls');  %% TRUE========ARCHIVO CON COORDENADES DE LAS ESTRELLAS, SE UTILIZA SOLO SI BANDFISICOS ES VERDADERO
%         ExtremosFisicos=[6452,982,1;9017,11390,44; 584,7489,63;10940,9139,42]; %% FALSE======== 4 EXTREMOS FISICOS NECESARIOS, SE UTILIZA SOLO SI BANDFISICOS ES FALSA
% filesDataStars={strcat(NOMBREPLACA,'_CARTES-DU-CIEL')}; % % filesDataStars={'AC1212_CARTES-DU-CIEL','AC1212_USNO'}; %% COORDENADAS DE CATALGOS ASTRO. A UTILIZAR
% bandMostrarTodGraf=true;    %%B ANDERA PARA MOSTRAR TODAS LAS GRAFICAS QUE SE GENEREN POR CATALOGO ASTRONOMICO
bandMedFis=true; %% TRABAJAR CON LOS MINIMOS Y MAXIMOS DE LAS MEDICIONES FISICAS
% fileVar=strcat('AstroLocation','_',NOMBREPLACA,'_var.mat'); %% ARCHIVO DONDE SE GUARDARAN LAS VARIABLES PARA SU POSTERIOR USO EN ASTROLOCATION_AdapterMain
bandIgualarCantElementos=true;  %% SOLO PARA PRUEBAS TRABAJA CON LAS ESTRELLAS DIGITALES ENCONTRADAS E IGNARA EL RESTO QUE NO SE TENGAN EN DIGITAL
% °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
%% RESERVANDO ESPACIO PARA VARIABLES FINALES
% % XYDigitales=cell(length(filesDataStars),1); %% CELL: COORDENADAS DIGITALES (CALCULADAS EN BASE A LA AR Y DEC)
% % XYEstandar=cell(length(filesDataStars),1);  %% CELL: COORDENAS ESTANDAR ORIGINALES
% % fDesplazamiento=cell(length(filesDataStars),1); %% CELL: FACTOR DEZPLAZAMIENTO APLICADO AL CONJUNTO DE DATOS
% % fEscala=cell(length(filesDataStars),1); %% CELL: FACTOR ESCALA APLICADO AL CONJUNTO DE DATOS
% % ExtFisicos=cell(length(filesDataStars),1);  %%  CELL: EXTREMOS FISICOS
% % ExtDigitales=cell(length(filesDataStars),1);  %%  CELL: EXTREMOS DIGITALES
% % ARDEC_Originales=cell(length(filesDataStars),1);  %%  CELL: AR Y DEC ORIGINALES DEL CATALOGO ASTRO.
%% OBTENIENDO EXTREMOS FISICOS
    disp('Datos archivo coordenadas Digitales (AR y DEC)');
    archivoPunDig=strcat(dirRaiz,filesDataStars);   %% GENERANDO LA RUTA COMPLETA DE ARCHIVO A LEER
    [ar,dec,numEst]=getDataExcel(archivoPunDig,1,3,nar,ndec,1); %% EXTRACCION DE DATOS "ARCHIVO PUNTOS DIGITALES"
    ARDEC_Originales=[ar,dec,numEst]; % Agrupando los datos leidos en una matriz
   ARDEC_Originales=comprobarNumerosMatriz(ARDEC_Originales);
   disp('Datos archivo coordenadas Fisicas');
    %% ¿CUENTA CON TODAS LAS COORDENADAS DE LA PLACA EN UN ARCHIVO DE EXCEL? 
    if (bandFisicos==true)  %% PARA TRABAJAR CON TODOS LOS PUNTOS FISICOS <-----------------OPCION 1
        
        [numeros, ~, ~] = xlsread(archivoPunFis,1); %% EXTRACCION DE DATOS "ARCHIVO PUNTOS FISICOS"
        noEst=numeros(1:end,1); % lee los identificadores (numeros) de las estrellas
        x=numeros(1:end,2);     % lee las coordenadas del eje X
        y=numeros(1:end,3);     % lee las coordenadas del eje Y
        XYFisicos=[x,y,noEst];  % Agrupando los datos leidos en una matriz
%         display(XYFisicos)
        XYFisicos=comprobarNumerosMatriz(XYFisicos);
        clear noEst x y; % limpiando variables despues de utilizarlas
        fprintf('Extremos Digitales:  %s\n',filesDataStars);
        [ExtremosFisicos]=obtenerExtremosFisicosYDigitales(XYFisicos,ARDEC_Originales,bandMedFis); %% LLAMANDO A LA FUNCION QUE OBTIENE LOS EXTREMOS 
        
        if (bandIgualarCantElementos==true)
            size(XYFisicos)
            size(ARDEC_Originales)
            [ARDEC_Originales,XYFisicos]=igualarCantElementosFisDig(XYFisicos,ARDEC_Originales);
        end
        
    else  %% PARA TRABAJAR SOLO CON LOS 4 PUNTOS FISICOS NECESARIOS <----------OPCION 2
        XYFisicos=ExtremosFisicos;
    end
    ExtFisicos=ExtremosFisicos;
    fprintf('\n');


%%{
fprintf('Leyendo imagen: "%s" ... OK \n',imgPath);
IMAGENPLACA=imread(imgPath); %% LEE UNA SOLA VEZ LA PLACA

%% TRABAJANDO CON TODOS LOS CATALOGOS ASTRO. INDIVIDUALMENTE 
% %  for ii=1:length(filesDataStars)
% %     fprintf('\n=============================== CATALOGO %d DE %d ===============================\n', ii,length(filesDataStars));
    ii=1;
    if (bandMostrarTodGraf)
        multiFigu=10*ii; %% PARA MOSTRAR TODAS LAS GRAFICAS
    else 
        multiFigu=0;    %% NO SE MOSTRAN TODAS LAS GRAFICAS, SE SOBRESCRIBIRAN 
    end % if   
    %% PROYECCIONES ESTANDAR!!!
    estCentro=buscaEstrellaMat(ARDEC_Originales,puntoTang); %% OBTIENE LAS COORDENADAS DEL PUNTO TANGENTE
    if isnan(estCentro)
        assert(false,'Estrella %4.0f (puntoTang) no encontrado, cambie de punto tangente.', puntoTang);
        return        
    end
    XYEst=converCEcuato2CEstand(estCentro,ARDEC_Originales); %% CONVERCION DE COORDENADAS ECU A COORDENADAS ESTANDAR
    clear estCentro;
    %% CALCULANDO PUNTOS DIGITALES Y GRAFICANDOLOS
    [XYDigi,fEsca,fDesp]=ubicarPuntosEnPlaca(XYEst,IMAGENPLACA,ExtFisicos,true,multiFigu, true, true);%% ESCALA, ESPEJEA Y DESPLAZA LOS PUNTOS AL CENTRO DE LOS EXTRMOS FISICOS (CREA Y GRAFICA EN FIGURA 1)
    XYDigitales=XYDigi; %% AÑADIENDO DATOS A LA VARIABLE "SEGURA"
    XYEstandar=XYEst;   %% AÑADIENDO DATOS A LA VARIABLE "SEGURA"
    fEscala=fEsca;      %% AÑADIENDO DATOS A LA VARIABLE "SEGURA"
    fDesplazamiento=fDesp;  %% AÑADIENDO DATOS A LA VARIABLE "SEGURA"
    %%%{
    
%% GRAFICANDO PUNTOS FISICOS
    graficarPuntos(XYFisicos,' ', '*k'); %% GRAFICA EN FIGURA 1

%% CALCULANDO EL ERROR FISICOS-DIGITALES
    XYDigitales=eliminaPosicionMatriz(XYDigitales, [0]); %% ELIMINANDO LA POSICION 0 QUE ES EL CENTRO AGREGADO POR ESTETICA
    [f1,c1]=size(XYDigitales);
    [f2,c2]=size(XYFisicos);

    XYDigitales=ordenarMat3d(XYDigitales,'Z');
    XYFisicos=ordenarMat3d(XYFisicos,'Z');

    if (f1==f2 && c1==c2)
        Error=XYFisicos-XYDigitales;
        ErrorProm=mean(abs(Error));
        fprintf('\n****** Error promedio en X= %5.3f   Y= %5.3f   NUMERACION= %d ******\n',ErrorProm(1),ErrorProm(2),ErrorProm(3));
        title(strcat(filesDataStars,' - ',num2str(f1),'Estrellas---Error promedio en X=',num2str(ErrorProm(1)),', Y=',num2str(ErrorProm(2)),', Eti=',num2str(ErrorProm(3)))); %% CAMBIANDO TITULO PARA FIGURA 1
        fprintf('\n');
    %% DEMOSTRACION GRAFICA DEL ERROR CON COLORES
        ErrorABS=representaErrorFisDig(XYFisicos,XYDigitales,multiFigu,filesDataStars); %% CREA Y GRAFICA FIGURA 3
    else
        warning('¡¡LOS TAMAÑOS NO COINCIDEN!!');
    end % if
    
    fprintf('%s ... OK \n',filesDataStars);
    pause(2); %% Esperar 5 segundos

% close all;
clearvars -except fileVar filesDataStars XYDigitales XYEstandar fDesplazamiento fEscala Error ErrorABS ErrorProm bandFisicos  IMAGENPLACA dirRaiz ExtFisicos XYFisicos bandMostrarTodGraf puntoTang ARDEC_Originales NOMBREPLACA
 fprintf('==================================================================================\n\n');  
    %%}
% % end %% FOR

%% ESCALA DEL RECTANGULO IMAGINARIO DE LIMITES
WF=obtenerDistanciaEje(XYFisicos, 'x');
HF=obtenerDistanciaEje(XYFisicos, 'y');
centroFis=obtenerCentroConjPuntos(XYFisicos);
fprintf('Dimensiones (FISICAS) del Rectangulo de Limites: ANCHO= %d  ALTO= %d   ** CENTRO: XC=%6.2f  YC=%6.2f **\n', WF,HF,centroFis(1),centroFis(2));

% % for ii=1:length(filesDataStars)
    WD=obtenerDistanciaEje(XYDigitales, 'x');
    HD=obtenerDistanciaEje(XYDigitales, 'y');
    centroDig=obtenerCentroConjPuntos(XYDigitales);
    fprintf('Dimensiones (DIGITAL) del Rectangulo de Limites: ANCHO= %d  ALTO= %d   ** CENTRO: XC=%6.2f  YC=%6.2f **\n', WD,HD,centroDig(1),centroDig(2));
% % end
clear WD HD centroDig ii; 

%}
fileVar=strcat(dirRaiz,fileVar);
fprintf('\nGuardando variables en "%s" ... ',fileVar);
save(fileVar)
fprintf(' OK.\n');

ResultadosFinales=load(fileVar);

tiempo=toc; %% FIN DE MEDICION DE TIEMPO DE EJECUCION
if (tiempo<=60) %% PARA LOS SEGUNDOS
    fprintf('\n\nTIEMPO DE EJECUCION... %3.3f segundos.\n', tiempo); %% MOSTRANDO TIEMPO DE EJECUCION
else %% PARA LOS MINUTOS
    fprintf('\n\nTIEMPO DE EJECUCION... %3.3f minutos.\n', tiempo/60); %% MOSTRANDO TIEMPO DE EJECUCION
end %% else
disp(datestr(now));
diary off;





