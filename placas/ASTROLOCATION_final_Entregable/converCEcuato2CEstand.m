function [XYEstandar] = converCEcuato2CEstand(estCentro,ArDec)
[f,c]=size(ArDec);
% display(ArDec)
XYEstandar=zeros(f,c);

for ii=1:f
    estEcuat=ArDec(ii,1:3); % Ar Dec NumEst
    
%     display(estCentro)
%     display(estEcuat)
    [X,Y]=Trans2CoorEstandar(estCentro,estEcuat);
    
    XYEstandar(ii,1)=X;     % X proyectada (estandar)
    XYEstandar(ii,2)=Y;     % Y proyectada (estandar)
    XYEstandar(ii,3)=estEcuat(3); % NumEst
    
end %for 
%% INVERSA PARA COMPROBAR LA PROYECCION
%{
AD=ProyeccionPlana2Esferica(estCentro,XYEstandar);

disp([ArDec(1:end,1),AD(1:end,1)])
disp([ArDec(1:end,2),AD(1:end,2)])

if ArDec==AD
    disp('La comprobacion resulto EXITOSA!!!');
else
    disp('No son iguales las matrices AD y ArDec!!!');
end
%}
end %function 


function [X,Y]=Trans2CoorEstandar(est1,est2)
A0=est1(1);
D0=est1(2);
A=est2(1);
D=est2(2);
%% X proyectada
numerador=coseno(D)*seno(A-A0);
denominador=((coseno(D0)*coseno(D))*coseno(A-A0))+(seno(D0)*seno(D));
X=numerador/denominador;

clear numerador denominador;

%% Y proyectada
numerador=(coseno(D0)*seno(D))-(coseno(D)*seno(D0)*coseno(A-A0));
denominador=(seno(D0)*seno(D))+(coseno(D0)*coseno(D)*coseno(A-A0));
Y=numerador/denominador;

end


function [ArDec]=ProyeccionPlana2Esferica(estCentro,XY)
[f,c]=size(XY);

ArDec=zeros(f,c);

for ii=1:f
    estEstan=XY(ii,1:3); % Ar Dec NumEst
    [ar,dec]=Trans2CoorCelestes(estCentro,estEstan);
    
    ArDec(ii,1)=ar;     % X proyectada (estandar)
    ArDec(ii,2)=dec;     % Y proyectada (estandar)
    ArDec(ii,3)=estEstan(3); % NumEst
    
end %for 
end % function


function [A,D]=Trans2CoorCelestes(est1,est2) % AR y Dec
A0=est1(1);
D0=est1(2);
X=est2(1);
Y=est2(2);
%% Alfa 
A=A0+atang(X/(coseno(D0)-Y*seno(D0)));

%% Delta
disp('------------------------')
fprintf('numerador= 1 - (%d) * cotang(%d) = 1 - (%d) * (%d) \n',Y,D0,Y,cotang(D0));
numerador=1-Y*cotang(D0);
fprintf('denominador= ((%d) + tang(%d)) * coseno((%d) - (%d))=((%d) + (%d)) * (%d) \n',Y,D0,A,A0,Y,tang(D0),coseno(A-A0));
denominador=(Y+tang(D0))*coseno(A-A0);

D=acotang(numerador/denominador);

fprintf('D= acotang( (%d) / (%d)) = %d \n',numerador,denominador,D);
disp('------------------------')
end %function


function [val]=coseno(v)
val=cosd(v);
end % coseno

function [val]=seno(v)
val=sind(v);
end % seno

function [val]=tang(v)
val=tand(v);
end % tangente

function [val]=atang(v)
val=atand(v);
end % tangente inversa

function [val]=cotang(v)
val=cotd(v);
end % cotangente

function [val]=acotang(v)
val=acotd(v);
end % cotangente inversa

