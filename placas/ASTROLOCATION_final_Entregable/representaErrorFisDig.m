%%*****************************************************************************************
% Funcion que grafica el error entre los puntos fisicos y los digitales en 4 rangos: 
%       1) CYAN: Error bajo. 
%       2) AZUL: Error intermedio-bajo. 
%       3) MAGENTA: Error alto-intermedio. 
%       4) ROJO: Error alto.
% 
% Developer: Luis González Guzmán
% Date:  Mayo 2016
% 
% inputs:   XYFisss:      Puntos medidos manualmente.
%           XYDiggg:    Puntos calculados con la AR y Decl. Con respectivo escalamiento y desplazmiento.
% outputs:  Error:          Error absoluto (Diferencia entre Puntos fisicos y digitales).
%****************************************************************************************
function [Error]= representaErrorFisDig(XYFisss,XYDiggg,multiFigu,fds)
%% ESPEJEADO POR Y
% XYFisss=espejearPuntos(XYFisss,'y'); %ESPEJEADO POR Y
% XYDiggg=espejearPuntos(XYDiggg,'y'); %ESPEJEADO POR Y

XYDiggg=ordenarMat3d(XYDiggg,'Z');
XYFisss=ordenarMat3d(XYFisss,'Z');

[f1,c1]=size(XYDiggg);
[f2,c2]=size(XYFisss);

if (f1==f2 && c1==c2)
    err=XYFisss-XYDiggg;
    err=abs(err);
      
    Error=zeros(f1,1);
    for i=1:f1
        Error(i)=err(i,1)+err(i,2); % Valor numerico del error
        %Error(i,2)=0; % NADA
%         Error(i,3)=XYDiggg(i,3); % Numero de estrella
    end % for
    
    valMax=max(Error); %valor maximo
    %% CREACION DE RANGOS (COLORES)
    % ROJO, MAGENTA, AMARILLO, VERDE
    rango1=[1,(valMax*1/4)-1]; %c
    rango2=[rango1(2),(valMax*2/4)-1]; %b
    rango3=[rango2(2),(valMax*3/4)-1]; %magenta
    rango4=[rango3(2),(valMax*4/4)]; %rojo
    
    color=['c','b','m','r'];
    figure(2+multiFigu);
    hold on;
    
    tit=strcat(fds,'---Rango: *',upper(color(1)),': ',num2str(rango1(1)),' - ',num2str(rango1(2)), ...
        ' *',upper(color(2)),': ',num2str(rango2(1)),' - ',num2str(rango2(2)), ...
        ' *',upper(color(3)),': ',num2str(rango3(1)),' - ',num2str(rango3(2)), ...
        ' *',upper(color(4)),': ',num2str(rango4(1)),' - ',num2str(rango4(2)));
    title(tit);
%     whitebg(fig,'black')
        
    for i=1:f1
        bandCont=true;
        tip='-';
        if(bandCont==true) && (rango1(1)<=Error(i)) && (Error(i)<=rango1(2))
            n=1;
            plot([XYFisss(i,1),XYDiggg(i,1)],[XYFisss(i,2),XYDiggg(i,2)],strcat(tip,color(n)));
            bandCont=false;
        end
        if(bandCont==true) && (rango2(1)<=Error(i)) && (Error(i)<=rango2(2))
            n=2;
            plot([XYFisss(i,1),XYDiggg(i,1)],[XYFisss(i,2),XYDiggg(i,2)],strcat(tip,color(n)));
            bandCont=false;
        end
        if(bandCont==true) && (rango3(1)<=Error(i)) && (Error(i)<=rango3(2))           
            n=3;
            plot([XYFisss(i,1),XYDiggg(i,1)],[XYFisss(i,2),XYDiggg(i,2)],strcat(tip,color(n)));
            bandCont=false;
        end
        if(bandCont==true) && (rango4(1)<=Error(i)) && (Error(i)<=rango4(2))
            n=4;
            plot([XYFisss(i,1),XYDiggg(i,1)],[XYFisss(i,2),XYDiggg(i,2)],strcat(tip,color(n)));
            bandCont=false;
        end
        
        if (bandCont==false)
            plot(XYFisss(i,1),XYFisss(i,2),strcat('*',color(n)));
            text(XYFisss(i,1),XYFisss(i,2),num2str(XYFisss(i,3)),'Color','k');
            
            plot(XYDiggg(i,1),XYDiggg(i,2),strcat('*',color(n)));            
%             text(XYDiggg(i,1),XYDiggg(i,2),num2str(XYDiggg(i,3)),'Color',color);
        else 
            warning('*** ESTRELLA %d : EN SU POSICION CORRECTA, Error: %6.2f \n',i,Error(i));
            plot(XYFisss(i,1),XYFisss(i,2),strcat('*','k'));
            text(XYFisss(i,1),XYFisss(i,2),num2str(XYFisss(i,3)),'Color','k');           
            plot(XYDiggg(i,1),XYDiggg(i,2),strcat('*','k'));   
        end % if
    end %for
    axis square;
% %     display(rango1);display(rango2);display(rango3);display(rango4);
end % IF si son del mismo tamañano

end %function
