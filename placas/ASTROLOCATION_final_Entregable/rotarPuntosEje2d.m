function [XYprima]=rotarPuntosEje2d(XY,ejeDigital,angulo)
[f,c]=size(XY);
XYprima=zeros(f,c);

xc=ejeDigital(1);
yc=ejeDigital(2);



for i=1:f   
    x=XY(i,1);
    y=XY(i,2);
    
    eti=XY(i,3);
    xp=xc+((x-xc)*cosd(angulo))-((y-yc)*sind(angulo));
    yp=yc+((x-xc)*sind(angulo))+((y-yc)*cosd(angulo));
        
    XYprima(i,1)=xp;
    XYprima(i,2)=yp;
    XYprima(i,3)=eti;
end %for


end %function