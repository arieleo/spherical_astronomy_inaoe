%% LIMPIANDO VARIABLES 
clear;
close all;
clc;

angulo=5.31;
%% VARIABLES DE CONTROL
dirRaiz='/home/luis/Descargas/ProgramasMatlab/ASTROLOCATION/DataStars_AC1212/';
archivoPunDig='AC1212_CARTES-DU-CIEL';

%% OBTENCION DE COORDENADAS DEL EXCEL
[ar,dec,numEst]=getDataExcel(strcat(dirRaiz,archivoPunDig),1,3,9,10, 1);
ArDec=[ar,dec,numEst];

%% CONVERSION DE COORDENADAS ECU A XYZ
XYZCartesianas=converCEcuato2CXYZ(ArDec, true);

%% GENERAR FIGURA 1
figure (1);

zlabel('EJE Z','fontsize',12,'fontweight','b','color','r');
xlabel('EJE X','fontsize',12,'fontweight','b','color','r');
ylabel('EJE Y','fontsize',12,'fontweight','b','color','r');

graficarPuntos3D(XYZCartesianas,'','*b');
hold on;
title(strcat('COORDENADAS XYZ DE: ',' ',archivoPunDig(1:strfind(archivoPunDig,'_')-1)));
axis on
grid on
box on
xlabel('Eje X','FontSize',18.5,'Color','B');
ylabel('Eje Y','FontSize',18.5,'Color','B');
zlabel('Eje Z','FontSize',18.5,'Color','B');

%% OBTENCION DE punto1 Y punto2
horizontal=true;

%% ORDENADO DE LA MATRIZ PARA OBTENER EL punto1 Y punto2
if horizontal==true
    auxCarte=ordenarMatNDim(XYZCartesianas,1); % EJE X        
else
    auxCarte=ordenarMatNDim(XYZCartesianas,3); % EJE Y (Ó Z)
end

punto1=auxCarte(1,1:end); %% OBTENCION DE PRIMERA FILA
punto2=auxCarte(end,1:end); %% OBTENCION DE ULTIMA FILA

%% PINTAR PUNTOS - CUERDA DE LA SEMICIRCUNFERENCIA
plot3(punto1(1),punto1(2),punto1(3),'*R');
plot3(punto2(1),punto2(2),punto2(3),'*R');

plot3([punto1(1),punto2(1)],[punto1(2),punto2(2)],[punto1(3),punto2(3)],'--R');

%% CALCULANDO EL LARGO DEL ARCO
disCuerda = sqrt((punto1(1)-punto2(1))^2+(punto1(2)-punto2(2))^2+(punto1(3)-punto2(3))^2);

%% OBTENIENDO PUNTO MEDIO DE CUERDA
xc=punto1(1)+(punto2(1)-punto1(1))/2;
yc=punto1(2)+(punto2(2)-punto1(2))/2;
zc=punto1(3)+(punto2(3)-punto1(3))/2;

punto3=[xc,yc,zc];

plot3(punto3(1),punto3(2),punto3(3),'*R');
text(xc,yc,zc,'C. CUERDA','Color','R');

radio=(disCuerda/2)/sind(angulo/2);
 
%% LADOS TRIANGULO

radio=radio
segCuerda=disCuerda/2
segRadio=sqrt((radio^2)-(segCuerda^2))

%% ANGULOS

[A,B,C]=calculaAngulo3lados(segCuerda,segRadio,radio)

%%{
%% ROTACION
pAux=rotarPuntosEje(punto1,punto3,90);

pAux=[pAux(1),pAux(2),pAux(3),pAux(4)];
plot3(pAux(1),pAux(2),pAux(3),'*M');
text(pAux(1),pAux(2),pAux(3),strcat('PuntoAux(',num2str(pAux(4)),')'),'Color','M');

%% union de triaungulo auxiliar
plot3([punto1(1),pAux(1)],[punto1(2),pAux(2)],[punto1(3),pAux(3)],'--M');
plot3([punto3(1),pAux(1)],[punto3(2),pAux(2)],[punto3(3),pAux(3)],'--M');


[anguPAux,~]=calculaAngulo3puntos(pAux,punto1,punto3)
[anguP1,~]=calculaAngulo3puntos(punto1,pAux,punto3)
[anguP3,~]=calculaAngulo3puntos(punto3,pAux,punto1)

pAux
punto3



kk=(radio^2)-((pAux(1)-punto1(2))^2)-((pAux(3)-punto1(3))^2)-(punto1(2)^2)
a=1
b=-2*punto1(2)
c=kk*-1

Y1=(-(b)+sqrt((b^2)-(4*a*c)))/2*a
Y2=(-(b)-sqrt((b^2)-(4*a*c)))/2*a


pCentro=[pAux(1),Y2,pAux(3)];


%% union de triaungulo auxiliar
plot3(pCentro(1),pCentro(2),pCentro(3),'*G');
text(pCentro(1),pCentro(2),pCentro(3),'Centro');

plot3([punto1(1),pCentro(1)],[punto1(2),pCentro(2)],[punto1(3),pCentro(3)],'--M');
plot3([punto3(1),pCentro(1)],[punto3(2),pCentro(2)],[punto3(3),pCentro(3)],'--M');

plot3([pAux(1),pCentro(1)],[pAux(2),pCentro(2)],[pAux(3),pCentro(3)],'--G');


%% COMPROBANDO
[angulo,distancias]=calculaAngulo3puntos(pCentro,punto1,punto3)
[angulo,~]=calculaAngulo3puntos(punto3,pCentro,punto1)
[angulo,~]=calculaAngulo3puntos(punto1,pCentro,punto3)


%% Mostrando resultados
% display(punto1)
% display(punto2)
% display(punto3);
% display(pCentro);

% display(disCuerda);
% display(radio);
%}
