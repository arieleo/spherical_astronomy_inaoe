function varargout = interfazplacas(varargin)
% INTERFAZPLACAS MATLAB code for interfazplacas.fig
%      INTERFAZPLACAS, by itself, creates a new INTERFAZPLACAS or raises the existing
%      singleton*.
%
%      H = INTERFAZPLACAS returns the handle to a new INTERFAZPLACAS or the handle to
%      the existing singleton*.
%
%      INTERFAZPLACAS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in INTERFAZPLACAS.M with the given input arguments.
%
%      INTERFAZPLACAS('Property','Value',...) creates a new INTERFAZPLACAS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before interfazplacas_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to interfazplacas_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help interfazplacas

% Last Modified by GUIDE v2.5 11-Aug-2016 16:22:51

% Begin initialization code - DO NOT EDIT
% clc;
% clear;
% close all


gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @interfazplacas_OpeningFcn, ...
                   'gui_OutputFcn',  @interfazplacas_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before interfazplacas is made visible.
function interfazplacas_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to interfazplacas (see VARARGIN)

% Choose default command line output for interfazplacas
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
set(handles.uibuttongroup2,'selectedobject',handles.TrueGraf)

% UIWAIT makes interfazplacas wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = interfazplacas_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function arcPFis_Callback(hObject, eventdata, handles)
% hObject    handle to arcPFis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of arcPFis as text
%        str2double(get(hObject,'String')) returns contents of arcPFis as a double


% --- Executes during object creation, after setting all properties.
function arcPFis_CreateFcn(hObject, eventdata, handles)
% hObject    handle to arcPFis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function minARx_Callback(hObject, eventdata, handles)
% hObject    handle to minARx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global miARx;
miARx = str2double(get(hObject,'string'));
% Hints: get(hObject,'String') returns contents of minARx as text
%        str2double(get(hObject,'String')) returns contents of minARx as a double


% --- Executes during object creation, after setting all properties.
function minARx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minARx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function MiY_Callback(hObject, eventdata, handles)
% hObject    handle to MiY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MiY as text
%        str2double(get(hObject,'String')) returns contents of MiY as a double


% --- Executes during object creation, after setting all properties.
function MiY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MiY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function MaX_Callback(hObject, eventdata, handles)
% hObject    handle to MaX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MaX as text
%        str2double(get(hObject,'String')) returns contents of MaX as a double


% --- Executes during object creation, after setting all properties.
function MaX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MaX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function MaY_Callback(hObject, eventdata, handles)
% hObject    handle to MaY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MaY as text
%        str2double(get(hObject,'String')) returns contents of MaY as a double


% --- Executes during object creation, after setting all properties.
function MaY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MaY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function archEst_Callback(hObject, eventdata, handles)
% hObject    handle to archEst (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of archEst as text
%        str2double(get(hObject,'String')) returns contents of archEst as a double
% global NOMBREPLACA
extDatosD = get(hObject,'Value')



% --- Executes during object creation, after setting all properties.
function archEst_CreateFcn(hObject, eventdata, handles)
% hObject    handle to archEst (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function minDECx_Callback(hObject, eventdata, handles)
% hObject    handle to minDECx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of minDECx as text
%        str2double(get(hObject,'String')) returns contents of minDECx as a double
global miDECx;
miDECx = str2double(get(hObject,'string'));

% --- Executes during object creation, after setting all properties.
function minDECx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minDECx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function minNumx_Callback(hObject, eventdata, handles)
% hObject    handle to minNumx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of minNumx as text
%        str2double(get(hObject,'String')) returns contents of minNumx as a double
global miNumx;
miNumx = str2double(get(hObject,'string'));

% --- Executes during object creation, after setting all properties.
function minNumx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minNumx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function minARy_Callback(hObject, eventdata, handles)
% hObject    handle to minARy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of minARy as text
%        str2double(get(hObject,'String')) returns contents of minARy as a double
global miARy;
miARy = str2double(get(hObject,'string'));

% --- Executes during object creation, after setting all properties.
function minARy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minARy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function minDECy_Callback(hObject, eventdata, handles)
% hObject    handle to minDECy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of minDECy as text
%        str2double(get(hObject,'String')) returns contents of minDECy as a double
global miDECy;
miDECy = str2double(get(hObject,'string'));

% --- Executes during object creation, after setting all properties.
function minDECy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minDECy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function minNumy_Callback(hObject, eventdata, handles)
% hObject    handle to minNumy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of minNumy as text
%        str2double(get(hObject,'String')) returns contents of minNumy as a double
global miNumy;
miNumy = str2double(get(hObject,'string'));

% --- Executes during object creation, after setting all properties.
function minNumy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minNumy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function maxARx_Callback(hObject, eventdata, handles)
% hObject    handle to maxARx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of maxARx as text
%        str2double(get(hObject,'String')) returns contents of maxARx as a double
global maARx;
maARx = str2double(get(hObject,'string'));

% --- Executes during object creation, after setting all properties.
function maxARx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxARx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function maxDECx_Callback(hObject, eventdata, handles)
% hObject    handle to maxDECx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of maxDECx as text
%        str2double(get(hObject,'String')) returns contents of maxDECx as a double
global maDECx;
maDECx = str2double(get(hObject,'string'));

% --- Executes during object creation, after setting all properties.
function maxDECx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxDECx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function maxNumx_Callback(hObject, eventdata, handles)
% hObject    handle to maxNumx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of maxNumx as text
%        str2double(get(hObject,'String')) returns contents of maxNumx as a double
global maNumx;
maNumx = str2double(get(hObject,'string'));

% --- Executes during object creation, after setting all properties.
function maxNumx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxNumx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function maxARy_Callback(hObject, eventdata, handles)
% hObject    handle to maxARy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of maxARy as text
%        str2double(get(hObject,'String')) returns contents of maxARy as a double
global maARy;
maARy = str2double(get(hObject,'string'));

% --- Executes during object creation, after setting all properties.
function maxARy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxARy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function maxDECy_Callback(hObject, eventdata, handles)
% hObject    handle to maxDECy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of maxDECy as text
%        str2double(get(hObject,'String')) returns contents of maxDECy as a double
global maDECy;
maDECy = str2double(get(hObject,'string'));

% --- Executes during object creation, after setting all properties.
function maxDECy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxDECy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function maxNumy_Callback(hObject, eventdata, handles)
% hObject    handle to maxNumy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of maxNumy as text
%        str2double(get(hObject,'String')) returns contents of maxNumy as a double
global maNumy;
maNumy = str2double(get(hObject,'string'));

% --- Executes during object creation, after setting all properties.
function maxNumy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxNumy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in buscar.
function buscar_Callback(hObject, eventdata, handles)
% hObject    handle to buscar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[PathFile] = uigetdir;
% [Filename pathname] = uigetfile({'*.txt'},'File Selector');
% fm = strrep(Filename,'.txt','')
% fullpathname = strcat(pathname,fm,'/');
% imag = strcat(fm,'.tif')
set(handles.dirRaiz,'String',PathFile);
% set(handles.nombPlaca,'String',fm);
% set(handles.nomImage,'String',imag);
% global NOMBREPLACA dirRaiz imgPath;
% NOMBREPLACA = fm;
global dirRai;
dirRai = PathFile;
% imgPath = strcat(pathname,imag);
% fileDataStars = strcat(fm,'_CARTES-DU-CIEL')
guidata(hObject,handles)


function nombPlaca_Callback(hObject, eventdata, handles)
% hObject    handle to nombPlaca (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global NOMBREPLAC;
NOMBREPLAC = get(hObject,'String');
% Hints: get(hObject,'String') returns contents of nombPlaca as text
%        str2double(get(hObject,'String')) returns contents of nombPlaca as a double

% --- Executes during object creation, after setting all properties.
function nombPlaca_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nombPlaca (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit23_Callback(hObject, eventdata, handles)
% hObject    handle to edit23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit23 as text
%        str2double(get(hObject,'String')) returns contents of edit23 as a double


% --- Executes during object creation, after setting all properties.
function edit23_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit24_Callback(hObject, eventdata, handles)
% hObject    handle to edit24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit24 as text
%        str2double(get(hObject,'String')) returns contents of edit24 as a double


% --- Executes during object creation, after setting all properties.
function edit24_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in buscarImage.
function buscarImage_Callback(hObject, eventdata, handles)
% hObject    handle to buscarImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[Filename pathname] = uigetfile({'*.*'},'File Selector');
set(handles.nomImage,'String',Filename);

function dirRaiz_Callback(hObject, eventdata, handles)
% hObject    handle to dirRaiz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of dirRaiz as text
%        str2double(get(hObject,'String')) returns contents of dirRaiz as a double


% --- Executes during object creation, after setting all properties.
function dirRaiz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dirRaiz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function nomImage_Callback(hObject, eventdata, handles)
% hObject    handle to nomImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nomImage as text
%        str2double(get(hObject,'String')) returns contents of nomImage as a double


% --- Executes during object creation, after setting all properties.
function nomImage_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nomImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ar_Callback(hObject, eventdata, handles)
% hObject    handle to ar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ar as text
%        str2double(get(hObject,'String')) returns contents of ar as a double
global ar;
ar = str2double(get(hObject,'String'));
if isnan(ar)
  errordlg('Solo acepta valores numericos','Invalid Input','modal')
  uicontrol(hObject)
  return
else
  ar = str2double(get(hObject,'String'));
end

% --- Executes during object creation, after setting all properties.
function ar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit28_Callback(hObject, eventdata, handles)
% hObject    handle to edit28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit28 as text
%        str2double(get(hObject,'String')) returns contents of edit28 as a double
global pT;
pT = str2double(get(hObject,'String'));
if isnan(pT)
  errordlg('Solo acepta valores numericos','Invalid Input','modal')
  uicontrol(hObject)
  return
else
  pT = str2double(get(hObject,'String'));
end

% --- Executes during object creation, after setting all properties.
function edit28_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit29_Callback(hObject, eventdata, handles)
% hObject    handle to edit29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit29 as text
%        str2double(get(hObject,'String')) returns contents of edit29 as a double
global dec;
dec = str2double(get(hObject,'String'));
if isnan(dec)
  errordlg('Solo acepta valores numericos','Invalid Input','modal')
  uicontrol(hObject)
  return
else
  dec = str2double(get(hObject,'String'));
end

% --- Executes during object creation, after setting all properties.
function edit29_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in True.
function True_Callback(hObject, eventdata, handles)
% hObject    handle to True (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global band;
val = get(hObject,'Value');
if val == 1
%       handles.True_Callback = val;
      set(handles.arcPFis,'enable','on');
      set(handles.extPF,'enable','on');
      obt2(hObject, handles);
      band = true;
      guidata(hObject, handles);

else
      set(handles.arcPFis,'enable','off');
end

% Hint: get(hObject,'Value') returns toggle state of True

% --- Executes on button press in False.
function False_Callback(hObject, eventdata, handles)
% hObject    handle to False (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global band;
val2 = get(hObject,'Value');
if val2 == 1
%       handles.True_Callback = val;
      set(handles.arcPFis,'enable','off');
      set(handles.extPF,'enable','off');
      obt(hObject, handles);
      guidata(hObject, handles);
      band = false;

else
      set(handles.arcPFis,'enable','on');
      
end
% Hint: get(hObject,'Value') returns toggle state of False

% function
function obt(hObject, handles)
    set(handles.minARx,'enable','on');
    set(handles.minDECx,'enable','on');
    set(handles.minNumx,'enable','on');
    set(handles.minARy,'enable','on');
    set(handles.minDECy,'enable','on');
    set(handles.minNumy,'enable','on');
    set(handles.maxARx,'enable','on');
    set(handles.maxDECx,'enable','on');
    set(handles.maxNumx,'enable','on');
    set(handles.maxARy,'enable','on');
    set(handles.maxDECy,'enable','on');
    set(handles.maxNumy,'enable','on');
    guidata(hObject, handles);

function obt2(hObject, handles)
    set(handles.minARx,'enable','off');
    set(handles.minDECx,'enable','off');
    set(handles.minNumx,'enable','off');
    set(handles.minARy,'enable','off');
    set(handles.minDECy,'enable','off');
    set(handles.minNumy,'enable','off');
    set(handles.maxARx,'enable','off');
    set(handles.maxDECx,'enable','off');
    set(handles.maxNumx,'enable','off');
    set(handles.maxARy,'enable','off');
    set(handles.maxDECy,'enable','off');
    set(handles.maxNumy,'enable','off');
    guidata(hObject, handles);

function compr(hObject, handles)
global miARx miDECx miNumx miARy miDECy miNumy maARx maDECx maNumx maARy maDECy maNumy extFis;
    a1 = str2double(get(handles.minARx,'String'));
    a2 = str2double(get(handles.minDECx,'String'));
    a3 = str2double(get(handles.minNumx,'String'));
    a4 = str2double(get(handles.minARy,'String'));
    a5 = str2double(get(handles.minDECy,'String'));
    a6 = str2double(get(handles.minNumy,'String'));
    a7 = str2double(get(handles.maxARx,'String'));
    a8 = str2double(get(handles.maxDECx,'String'));
    a9 = str2double(get(handles.maxNumx,'String'));
    a10 = str2double(get(handles.maxARy,'String'));
    a11 = str2double(get(handles.maxDECy,'String'));
    a12 = str2double(get(handles.maxNumy,'String'));
    if isnan(a1)
        errordlg('Debe ingresar los datos necesarios en Extremos 1','Invalid Input','modal')
        errordlg('Solo acepta valores numericos','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    if isnan(a2)
        errordlg('Debe ingresar los datos necesarios en Extremos 1','Invalid Input','modal')
        errordlg('Solo acepta valores numericos','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    if isnan(a3)
        errordlg('Debe ingresar los datos necesarios en Extremos 1','Invalid Input','modal')
        errordlg('Solo acepta valores numericos','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    if isnan(a4)
        errordlg('Debe ingresar los datos necesarios en Extremos 2','Invalid Input','modal')
        errordlg('Solo acepta valores numericos','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    if isnan(a5)
        errordlg('Debe ingresar los datos necesarios en Extremos 2','Invalid Input','modal')
        errordlg('Solo acepta valores numericos','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    if isnan(a6)
        errordlg('Debe ingresar los datos necesarios en Extremos 2','Invalid Input','modal')
        errordlg('Solo acepta valores numericos','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    if isnan(a7)
        errordlg('Debe ingresar los datos necesarios en Extremos 3','Invalid Input','modal')
        errordlg('Solo acepta valores numericos','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    if isnan(a8)
        errordlg('Debe ingresar los datos necesarios en Extremos 3','Invalid Input','modal')
        errordlg('Solo acepta valores numericos','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    if isnan(a9)
        errordlg('Debe ingresar los datos necesarios en Extremos 3','Invalid Input','modal')
        errordlg('Solo acepta valores numericos','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    if isnan(a10)
        errordlg('Debe ingresar los datos necesarios en Extremos 4','Invalid Input','modal')
        errordlg('Solo acepta valores numericos','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    if isnan(a11)
        errordlg('Debe ingresar los datos necesarios en Extremos 4','Invalid Input','modal')
        errordlg('Solo acepta valores numericos','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    if isnan(a12)
        errordlg('Debe ingresar los datos necesarios en Extremos 4','Invalid Input','modal')
        errordlg('Solo acepta valores numericos','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    extFis = [miARx,miDECx,miNumx;miARy,miDECy,miNumy;maARx,maDECx,maNumx;maARy,maDECy,maNumy];
    guidata(hObject, handles);

% --- Executes on selection change in extImag.
function extImag_Callback(hObject, eventdata, handles)
% hObject    handle to extImag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns extImag contents as cell array
%        contents{get(hObject,'Value')} returns selected item from extImag
global nomIm;
 valor = get(handles.extImag,'Value')
 if valor == 1
     nomIm = '.tif';
 elseif valor == 2
     nomIm = '.jpg';
 elseif valor == 3
     nomIm = '.png';
 end

% --- Executes during object creation, after setting all properties.
function extImag_CreateFcn(hObject, eventdata, handles)
% hObject    handle to extImag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in extPF.
function extPF_Callback(hObject, eventdata, handles)
% hObject    handle to extPF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns extPF contents as cell array
%        contents{get(hObject,'Value')} returns selected item from extPF
global extD;
 valor = get(handles.extPF,'Value');
 if valor == 1
     extD = '.xls';
 else
     extD = '.xlsx';
 end

% --- Executes during object creation, after setting all properties.
function extPF_CreateFcn(hObject, eventdata, handles)
% hObject    handle to extPF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in extDatos.
function extDatos_Callback(hObject, eventdata, handles)
% hObject    handle to extDatos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns extDatos contents as cell array
%        contents{get(hObject,'Value')} returns selected item from extDatos
global arDatos;
 valor = get(handles.extDatos,'Value');
 if valor == 1
     arDatos = '.xls';
 else
     arDatos = '.xlsx';
 end

% --- Executes during object creation, after setting all properties.
function extDatos_CreateFcn(hObject, eventdata, handles)
% hObject    handle to extDatos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ayudaImagern.
function ayudaImagern_Callback(hObject, eventdata, handles)
% hObject    handle to ayudaImagern (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
hFig = figure('units','normalized','outerposition',[0 0 1 1]);
imshow('astrolocationInformacion.png');
hFig = gcf;
hAx  = gca;
set(hAx,'Unit','normalized','Position',[0 0 1 1]);
set(hFig,'menubar','none')
set(hFig,'NumberTitle','off');


function arcRes_Callback(hObject, eventdata, handles)
% hObject    handle to arcRes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of arcRes as text
%        str2double(get(hObject,'String')) returns contents of arcRes as a double


% --- Executes during object creation, after setting all properties.
function arcRes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to arcRes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cargar.
function cargar_Callback(hObject, eventdata, handles)
% hObject    handle to cargar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global dirRai NOMBREPLAC;
S = get(handles.dirRaiz, 'String');
Ss = get(handles.nombPlaca, 'String');
if isempty(S)
    errordlg('Debe ingresar los datos necesarios en Directorio Ra�z','Invalid Input','modal')
    uicontrol(hObject)
    return
end
if isempty(Ss)
    errordlg('Debe ingresar los datos necesarios en Nombre de placa','Invalid Input','modal')
    uicontrol(hObject)
    return
end  
try
    set(handles.nomImage,'String',NOMBREPLAC);
    filesDataS = strcat(NOMBREPLAC,'_CARTES-DU-CIEL');
    set(handles.archEst,'String',filesDataS);
    archivoPF = strcat(dirRai,'/',NOMBREPLAC,'_FISICOS');
    set(handles.arcPFis,'String',archivoPF);
    fileVar = strcat('AstroLocation','_',NOMBREPLAC,'_var.mat');
    set(handles.arcRes,'String',fileVar);
catch 
    disp('Error')
end


% --- Executes on button press in ejec.
function ejec_Callback(hObject, eventdata, handles)
% hObject    handle to ejec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global NOMBREPLAC NOMBREPLACA dirRai dirRaiz imgPath ar nar dec ndec pT puntoTang bandFisicos band archivoPunFis ExtremosFisicos extFis filesDataStars bandG bandMostrarTodGraf fileVar arDatos extD nomIm;
Ss = get(handles.nombPlaca, 'String');
if isempty(Ss)
    errordlg('Debe ingresar los datos necesarios en Nombre de placa','Invalid Input','modal')
    uicontrol(hObject)
    return
end  
NOMBREPLACA = NOMBREPLAC
dirRaiz = dirRai
if isempty(ar)
    nar = get(handles.ar, 'String');
    nar = str2double(nar)
else
    nar = ar
end
if isempty(dec)
    ndec = get(handles.edit29, 'String');
    ndec = str2double(ndec)
else
    ndec = dec
end
if isempty(pT)
    puntoTang = get(handles.edit28, 'String');
    puntoTang = str2double(puntoTang)
else
    puntoTang = pT
end
% val3 = get(handles.False,'Value')
if isempty(band)
    bandFisicos = 0
else
    bandFisicos = band
end
if bandFisicos == 0
    compr(hObject, handles);
    guidata(hObject, handles);
    ExtremosFisicos = extFis
end
% bandFisicos = band;

if isempty(bandG)
    bandMostrarTodGraf = 0
else
    bandMostrarTodGraf = bandG
end
extI = nomIm;
if isempty(extI)
    extIn = '.tif';
else
    extIn = nomIm;
end
imgPath = strcat(dirRaiz,'/',NOMBREPLACA,extIn)
extDa = extD;
if isempty(extDa)
    extDa = '.xls';
else
    extDa = extD;
end
archivoPunFis = strcat(dirRaiz,'/',NOMBREPLACA,'_FISICOS',extDa)
extDat = arDatos;
if isempty(extDat)
    extDat = '.xls';
else
    extDat = arDatos;
end
filesDataStars = strcat(NOMBREPLACA,'_CARTES-DU-CIEL',extDat)
fileVar = strcat('AstroLocation','_',NOMBREPLACA,'_var.mat')

%% COMIENZA ASTROLOCATION_MAIN.M
aaa=strfind(dirRaiz,'\');
if (length(aaa)>0)
    %% es Windows
    dirRaiz=strcat(dirRaiz,'\');
else
    %% es linux
    dirRaiz=strcat(dirRaiz,'/');
end
   clc;
   for kk=1:5
    try 
        close(kk);
    catch
    end   
   end
assignin('base','NOMBREPLACA',NOMBREPLACA)
assignin('base','dirRaiz',dirRaiz)
assignin('base','imgPath',imgPath)
assignin('base','nar',nar)
assignin('base','ndec',ndec)
assignin('base','puntoTang',puntoTang)
assignin('base','bandFisicos',bandFisicos)
assignin('base','archivoPunFis',archivoPunFis)
assignin('base','ExtremosFisicos',ExtremosFisicos)
assignin('base','filesDataStars',filesDataStars)
assignin('base','bandMostrarTodGraf',bandMostrarTodGraf)
assignin('base','fileVar',fileVar)

[ResultadosFinales] =AstroLocation_Main(NOMBREPLACA,dirRaiz,imgPath,nar,ndec,...
    puntoTang,bandFisicos,archivoPunFis,ExtremosFisicos,filesDataStars, ...
    bandMostrarTodGraf,fileVar);

assignin('base','ResultadosFinales',ResultadosFinales)


fprintf('¡¡COMPLETADO!! \n');








% --- Executes on key press with focus on extDatos and none of its controls.
function extDatos_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to extDatos (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in TrueGraf.
function TrueGraf_Callback(hObject, eventdata, handles)
% hObject    handle to TrueGraf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bandG;
val = get(hObject,'Value');
if val == 1
      bandG = true;
else
      bandG = false;
end
% Hint: get(hObject,'Value') returns toggle state of TrueGraf


% --- Executes on button press in FalseGraf.
function FalseGraf_Callback(hObject, eventdata, handles)
% hObject    handle to FalseGraf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bandG;
val = get(hObject,'Value');
if val == 1
      bandG = false;
else
      bandG = true;
end
% Hint: get(hObject,'Value') returns toggle state of FalseGraf



function NomPlaca_Callback(hObject, eventdata, handles)
% hObject    handle to nombPlaca (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nombPlaca as text
%        str2double(get(hObject,'String')) returns contents of nombPlaca as a double


% --- Executes during object creation, after setting all properties.
function NomPlaca_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nombPlaca (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when selected object is changed in uibuttongroup1.
function uibuttongroup1_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uibuttongroup1 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
global band;
val2 = get(handles.False,'Value');
if val2 == 1
%       handles.True_Callback = val;
      set(handles.arcPFis,'enable','off');
      set(handles.extPF,'enable','off');
      obt(hObject, handles);
      guidata(hObject, handles);
      band = false;

else
      set(handles.arcPFis,'enable','on');
      set(handles.extPF,'enable','on');
      obt2(hObject, handles);
      guidata(hObject, handles);
      band = true;

end
