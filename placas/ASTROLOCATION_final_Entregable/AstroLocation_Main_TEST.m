try
    close 1;
    close 2;
catch
    %% NADA 
end

indiceCatalogo=1;
numEstrella=51;
multiFigu=0;
[XYDi,~]=desplazarPuntosCentroFisico(XYDigitales{indiceCatalogo},buscaEstrellaMat(XYFisicos,numEstrella),buscaEstrellaMat(XYDigitales{1},numEstrella));

figure(1)
imshow(IMAGENPLACA);
hold on
title(strcat('Datos desplazados a la estrella: ',num2str(numEstrella)));

graficarPuntos(XYDi,'','*R');
graficarPuntos(XYFisicos,'','*k');

%%{
%% CALCULANDO EL nuevoError FISICOS-DIGITALES
    XYDi=eliminaPosicionMatriz(XYDi, [0]); %% ELIMINANDO LA POSICION 0 QUE ES EL CENTRO AGREGADO POR ESTETICA
    [f1,c1]=size(XYDi);
    [f2,c2]=size(XYFisicos);

    XYDi=ordenarMat3d(XYDi,'Z');
    XYFisicos=ordenarMat3d(XYFisicos,'Z');

    if (f1==f2 && c1==c2)
        nuevoError=XYFisicos-XYDi;
        nuevoErrorProm=mean(abs(nuevoError));
        fprintf('\n****** nuevoError promedio en X= %5.3f   Y= %5.3f   NUMERACION= %d ******\n',nuevoErrorProm(1),nuevoErrorProm(2),nuevoErrorProm(3));
        title(strcat(filesDataStars{indiceCatalogo},' - ',num2str(f1),'Estrellas---nuevoError promedio en X=',num2str(nuevoErrorProm(1)),', Y=',num2str(nuevoErrorProm(2)),', Eti=',num2str(nuevoErrorProm(3)))); %% CAMBIANDO TITULO PARA FIGURA 1
        fprintf('\n');
    %% DEMOSTRACION GRAFICA DEL nuevoError CON COLORES
        nuevoErrorABS=representaErrorFisDig(XYFisicos,XYDi,multiFigu,filesDataStars{indiceCatalogo}); %% CREA Y GRAFICA FIGURA 3
    else
        warning('¡¡LOS TAMAÑOS NO COINCIDEN!!');
    end % if
%}
disp('***  Error: ***');
display(ErrorProm);
display(nuevoErrorProm);
DiferenciaError=abs(ErrorProm-nuevoErrorProm);
display(DiferenciaError);

