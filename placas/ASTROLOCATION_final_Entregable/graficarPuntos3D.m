%%*****************************************************************************************
% Funcion para escalar los puntos convertidos a coor. cartesianas
% respecto a los puntos fisicos (placa digitalizada) utilizando los
% extremos (minX-maxX) (minY-maxY).
% 
% Developer: 
% Date:  Mayo 2016
% inputs: 
%           XY:                 Matriz con datos con numero de estrella.
%           cad:                Cadena de texto en cada punto
%           color:              Cadena indicando el color
%************************************************************************************

function [] = graficarPuntos3D(XY,cad,color)

[f,c]=size(XY);
if (c~=4)
    assert(false,'El tamaño de la matriz no es valio, compruebe que tenga 4 columnas (X, Y, Z, Eti)');
    return;
end %if 
% display(XY);

plot3(XY(1:end,1),XY(1:end,2),XY(1:end,3),color);
hold on;

for i=1:f
    x=XY(i,1);
    y=XY(i,2);
    z=XY(i,3);
    numEstrella=XY(i,4);     
    
    text(x,y,z,strcat(num2str(numEstrella),' ',cad));
        
end %for
axis square;
end %function