function [newXYDig,newXYFis] = igualarCantElementosFisDig(XYFis,XYDig)
[f1,c1]=size(XYFis);
[f2,c2]=size(XYDig);

% % newXYDig=zeros(f1,c1);

if(c1==c2 && c1==3)
    cont=1;
    for ii=1:f1
%         display(XYFis(ii,3))
        estrella=buscaEstrellaMat(XYDig,XYFis(ii,3));
        if (~isnan(estrella))
            
            newXYDig(cont,1)=estrella(1);
            newXYDig(cont,2)=estrella(2);
            newXYDig(cont,3)=estrella(3);
        
            newXYFis(cont,1)=XYFis(ii,1);
            newXYFis(cont,2)=XYFis(ii,2);
            newXYFis(cont,3)=XYFis(ii,3);
            cont=cont+1;
        else 
%             fprintf('Estrella %3.0f (XYFis) no encontrada en XYDig.\n',XYFis(ii,3))
        end
        
    end %%for
else 
    assert(false,'Error: El numero de columnas no es igual entre las matrices.');
    return;
end %% if
% % 
% % display(XYFis);
% % display(XYDig);
% % display(newXYDig);
% % display(newXYFis);
% % [f1,c1]=size(newXYFis);
% % [f2,c2]=size(newXYDig);
% % 
% % if (f1==f2 && c1==c2)
% %     disp('TAMAÑOS IGUALES!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
% % else
% %     disp('TAMAÑOS Noooooooooooo son iguales!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
% % end

% 
% size(XYFis)
% size(newXYDig)


end