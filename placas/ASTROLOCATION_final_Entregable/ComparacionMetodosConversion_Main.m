% PROGRAMA QUE LEE ARCHIVOS DE EXCEL PARA GRAFICAR LAS COORDENADAS ESTANDAR
% Y LAS COORDENADAS XYZ, GENERANDO DOS GRAFICAS, LA PRIMER GRAFICA MUESTRA
% LOS PUNTOS EN "CRUDO" (SIN APLICAR ESCALAMIENTO NI ESPEJEO, SOLO
% DESPLAZAMIENTO A UN PUNTO EN COMUN), LA SEGUNDA GRAFICA MUESTRA LOS DATOS
% "PROCESADOS" (APLICANDO DESPLAZAMIENTO, ESPEJEO, ETC).

%%
clear;
close all;
clc;

%% GOGL9306299I0
dirRaiz='/home/luis/Descargas/ProgramasMatlab/ASTROLOCATION/DataStars_AC1212/';
arcPunFisicos='AC1212_FISICOS';
arcPunDig='AC1212_CARTES-DU-CIEL';
imgPath=strcat(dirRaiz,'AC1212.tif'); 
IMAGENPLACA=imread(imgPath);


%% LEYENDO PUNTOS FISICOS
[ar,dec,numEst]=getDataExcel(strcat(dirRaiz,arcPunFisicos),1,2,2,3,1);
XYFisicos=[ar,dec,numEst];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% LEYENDO PUNTOS DIGITALES ESTANDAR - CRUDOS
hoja=2;
nAR=5;
nDEC=6;
nEti=1;
[ar,dec,numEst]=getDataExcel(strcat(dirRaiz,arcPunDig),hoja,3,nAR,nDEC,nEti);
XYDigitalesEstand=[ar,dec,numEst];
XYDigitalesEstand=XYDigitalesEstand(1:end-2,1:end);

%% LEYENDO PUNTOS DIGITALES ESTANDAR - CRUDOS
hoja=3;
nEti=1;
[ar,dec,numEst]=getDataExcel(strcat(dirRaiz,arcPunDig),hoja,3,nAR,nDEC,nEti);
XYDigitalesXYZ=[ar,dec,numEst];
XYDigitalesXYZ=XYDigitalesXYZ(1:end-2,1:end);

%% DESPLAZANDO LOS DATOS A UN PUNTO 
puntoCentral=[0,0];

[XYDigitalesEstand,~]=desplazarPuntosCentroFisico(XYDigitalesEstand,puntoCentral,obtenerCentroConjPuntos(XYDigitalesEstand));
[XYDigitalesXYZ,~]=desplazarPuntosCentroFisico(XYDigitalesXYZ,puntoCentral,obtenerCentroConjPuntos(XYDigitalesXYZ));

%% GRAFICANDO FIGURA 1
figure(1);
hold on;
title('***DATOS EN CRUDO:*** Azul=XYDigitalesEstand    Magenta=XYDigitalesXYZ');
graficarPuntos(XYDigitalesEstand,'','*b'); %% PUNTOS XYDigitalesEstand
graficarPuntos(XYDigitalesXYZ,'','*m'); %% PUNTOS XYDigitalesXYZ
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% LEYENDO PUNTOS DIGITALES ESTANDAR - PROCESADOS
hoja=2;
nAR=15;
nDEC=16;
nEti=1;
[ar,dec,numEst]=getDataExcel(strcat(dirRaiz,arcPunDig),hoja,3,nAR,nDEC,nEti);
XYDigitalesEstand_Pros=[ar,dec,numEst];
XYDigitalesEstand_Pros=XYDigitalesEstand_Pros(1:end-2,1:end);

%% LEYENDO PUNTOS DIGITALES ESTANDAR - PROCESADOS
hoja=3;
nEti=1;
[ar,dec,numEst]=getDataExcel(strcat(dirRaiz,arcPunDig),hoja,3,nAR,nDEC,nEti);
XYDigitalesXYZ_Pros=[ar,dec,numEst];
XYDigitalesXYZ_Pros=XYDigitalesXYZ_Pros(1:end-2,1:end);

%% DESPLAZANDO LOS DATOS A UN PUNTO 
puntoCentral=obtenerCentroConjPuntos(XYFisicos);

[XYDigitalesEstand_Pros,~]=desplazarPuntosCentroFisico(XYDigitalesEstand_Pros,puntoCentral,obtenerCentroConjPuntos(XYDigitalesEstand_Pros));
[XYDigitalesXYZ_Pros,~]=desplazarPuntosCentroFisico(XYDigitalesXYZ_Pros,puntoCentral,obtenerCentroConjPuntos(XYDigitalesXYZ_Pros));

%% GRAFICANDO FIGURA 2
figure(2);
imshow(IMAGENPLACA);
hold on;
title('***DATOS PROCESADOS:*** Negro=XYFisicos    Azul=XYDigitalesEstand    Magenta=XYDigitalesXYZ');
graficarPuntos(XYDigitalesEstand_Pros,'','*b'); %% PUNTOS XYDigitalesEstand
graficarPuntos(XYDigitalesXYZ_Pros,'','*m'); %% PUNTOS XYDigitalesXYZ
graficarPuntos(XYFisicos,'','*k'); %% PUNTOS XYFISICOS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% LIMPIANDO VARIABLES INNECESARIAS
clearvars -except XYFisicos XYDigitalesXYZ XYDigitalesEstand  XYDigitalesXYZ_Pros XYDigitalesEstand_Pros

save('PRUEBALUIS.mat');
