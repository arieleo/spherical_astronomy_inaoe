clear all
clc
format long
x0 = -1.747;    %x del punto 1
y0 = -3.027;     %y del punto 1
z0 = 0.1895;     %z del punto 1
x1 = 0.9723;     %x del punto 2
y1 = -3.357;   %y del punto 2
z1 = -0.1895;     %z del punto 2
h = 3.5;        %la altura maxima que hay entre estos dos puntos

L = sqrt((x1-x0)^2+(y1-y0)^2+(z1-z0)^2)     %Se saca la distancia que hay entre los dos puntos en 3 dimensiones
x = x1 - (L/2)                              %Se saca el punto medio en x de la distancia que hay entre los puntos anteriores
y = y0 + (((x-x0)/(x1-x0))*(y1-y0))         %Se busca el punto y del punto medio
z = z0 + (((x-x0)/(x1-x0))*(z1-z0))         %Se busca el punto z del punto medio
%Se busca la flecha
% if z >= x & z >= y
    d = h - z
% elseif y >= x & y >= z
%     d = h - y
% else x >= z & x >= y
%     d = h - z
% end
% y = y0 + (x) * ((y1-y0)/(x1-x0))
r = ((L/2)^2 + d^2)/ (2*d)                  %Se saca el radio de la esfera
% se busca sacar un triangulo para poder hacer el calculo de la longitud
% del arco
% a = r - d                                   %Se saca el lado a del triangulo usando la distancia que hay entre nuestro punto medio y el centro de la esfera
% b = L / 2                                   %Se saca el lado b del triangulo sacando la mitad de la distancia 
% c = sqrt(a^2 + b^2)                         %Se saca por metodo de pitagoras la hipotenusa del triangulo
% angAlfa = acosd(a/c)                        %Se busca el angulo C
% angBeta = asind(a/c)                        %Se busca el angulo A
% dE2 = sqrt((x-x1)^2+(y-y1)^2+(z-z1)^2)      %Se saca la distancia que hay entre el centro y uno de los puntos
% l = (pi / 180) * dE2 * (angAlfa * 2)        %Se hace el calculo de la longitud del arco
