%%*****************************************************************************************
% Funcion para obtener el centro de un conjunto de puntos,
% el centro es la distancia entre Xmin y Xmax entre dos asi
% como en (Ymin-Ymax)/2.
% 
% Developer: 
% Date:  Mayo 2016
% inputs: 
%           XYN:                Matriz con datos con numero de estrella.
% outputs:
%           centroDigi:         Centro de los puntos (X,Y,NumEstrella).
%************************************************************************************

function [centroDigi]=obtenerCentroConjPuntos(XYN)
%% OBTIENE LARGO DE LOS PUNTOS DEL EXCEL 
   [distanciaX, minimoX, maximoX]=obtenerDistanciaEje(XYN,'x');
   [distanciaY, minimoY, maximoY]=obtenerDistanciaEje(XYN,'y');
   
   xc=minimoX+(distanciaX/2);
   yc=minimoY+(distanciaY/2);
    
    centroDigi=[xc,yc, 0]; %posX posY numEstrella
end % function 