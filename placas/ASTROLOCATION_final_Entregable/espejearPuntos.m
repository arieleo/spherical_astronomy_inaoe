%%*****************************************************************************************
% Funcion para escalar los puntos convertidos a coor. cartesianas
% respecto a los puntos fisicos (placa digitalizada) utilizando los
% extremos (minX-maxX) (minY-maxY).
% 
% Developer: 
% Date:  Mayo 2016
% inputs: 
%           XY:         Matriz con datos (x, y) y numero de estrella.
%           sentido:    Sentido en el que se espejeara (X o Y).
% outputs:
%           newXY:      Datos espejeados respecto al sentido (X o Y)
%*****************************************************************************************

function [newXY]=espejearPuntos(XY,sentido)
%% OBTENER CENTRO DATOS
% disp('*************') 
centro=obtenerCentroConjPuntos(XY);
 numm=-1;
 
%Espejeado respecto a X
[f,~]=size(XY);
newXY=zeros(f,3); % [posX ,posY ,numEspectro]

if(sentido=='X' || sentido=='x')
    espejo=centro(1);
    for i=1:f
        despX=numm*(XY(i,1)-espejo);
        
        newXY(i,1)=espejo+despX;
        newXY(i,2)=XY(i,2);
        newXY(i,3)=XY(i,3);
    end % for
end
if(sentido=='y' || sentido=='Y')
    espejo=centro(2);
    for i=1:f
        despY=numm*(XY(i,2)-espejo);
        
        newXY(i,1)=XY(i,1);
        newXY(i,2)=espejo+despY;
        newXY(i,3)=XY(i,3);
    end % for
end %IF

end % function 