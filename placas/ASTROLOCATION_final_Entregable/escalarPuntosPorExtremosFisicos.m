%%*****************************************************************************************
% Funcion para escalar los puntos convertidos a coor. cartesianas
% respecto a los puntos fisicos (placa digitalizada) utilizando los
% extremos (minX-maxX) (minY-maxY).
% 
% Developer: 
% Date:  Mayo 2016
% inputs: 
%           XYN:                Matriz con datos con numero de estrella.
%           ExtremosFisicos:	Puntos fisicos con numero de estrella (minX-maxX) (minY-maxY).
% outputs:
%           XYprima:            Matriz con los puntos escalados.
%           fEscala:            Factor de escalado.
%************************************************************************************
function [XYprima,fEscala]= escalarPuntosPorExtremosFisicos(XYN,ExtremosFisicos)
%% OBTIENE LARGO DE LOS PUNTOS DEL EXCEL    
    [dX, ~]=obtenerDistanciaEje(XYN,'X');
    [dY, ~]=obtenerDistanciaEje(XYN,'Y');
    
%% OBTIENE LARGO DE LOS PUNTOS FISICOS   
    [dX_fisico, ~]=obtenerDistanciaEje(ExtremosFisicos,'X');
    [dY_fisico, ~]=obtenerDistanciaEje(ExtremosFisicos,'Y');
    
    %% OBTIENE EL FACTOR ESCALA EN "X" Y EN "Y"
    escalaX=dX_fisico/dX;
    escalaY=dY_fisico/dY;
% % %     fprintf('==================== ESCALA ====================\n');
% % %     fprintf('Escala p. digital--> X: %d   Y:%d \n', dX,dY);
% % %     fprintf('Escala p. fisico--> X: %d   Y:%d \n', dX_fisico,dY_fisico);
% % %     fprintf('****FACTOR ESCALA EN--> X: %d   Y:%d **** \n', escalaX,escalaY);
% % %     fprintf('================================================\n');

    %% ESCALADO
    % PUNTOS PARA MOVER EL ESCALADO
    xc=0;
    yc=0;   
    [fil,~]=size(XYN); %numero de elementos
    XYprima=zeros(fil,3);
    for i=1:fil
        xp=xc+(escalaX*(XYN(i,1)-xc)); %NUEVA POSICION X DESPUES DEL ESCALADO
        yp=yc+(escalaY*(XYN(i,2)-yc)); %NUEVA POSICION Y DESPUES DEL ESCALADO
        % GUARDANDO DATOS EN UNA MATRIZ
        XYprima(i,1)=xp;
        XYprima(i,2)=yp;
        XYprima(i,3)=XYN(i,3); % Numero de estrella
    end %for   
    fEscala=[escalaX,escalaY];
end %function