%%**************************************************************************
% ------------------------------ASTROLOCATION------------------------------
% COMPLEMENTO DEL PROGRAMA ASTROLOCATION_MAIN, ESTE PROGRAMA CONSIDERA LAS
% VARIABLES DEL PROGRAMA "ASTROLOCATION_MAIN" PARA REALIZAR AJUSTES EN EL
% ESCALADO, TRASLACION, ESPEJEO, CON BASE AL ESCALADO DEL CUADRADO DE
% LIMITES CONSIDERANDO LOS ESPECTROS QUE ESTEN DENTRO DEL NUEVO CUADRADO DE
% LIMITES.
% 
% Developer: Luis González Guzmán
% E-mail: luisiiyoo@gmail.com
% Date:  Julio 2016
%***************************************************************************
%% LIMPIANDO EJECUCIONES ANTERIORES
clc;
clear;
close all;
%% TIME CONTROLLER
tic; %% INICIO DE MEDICION DE TIEMPO DE EJECUCION

%% ASTROLOCATION ADAPTER
fprintf('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n');
fprintf('//////////////////////// ASTROLOCATION ADAPTER \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ \n');
fprintf('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n');
%%  #################################################  VARIABLES DE CONTROL #################################################
% °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
fileVar='AstroLocation_AC1212_var.mat'; %% ARCHIVO DONDE SE GUARDARAN LAS VARIABLES PARA SU POSTERIOR USO EN ASTROLOCATION_AdapterMain
escalaCuadradoLim=25;   %% ESCALA DEL NUEVO CUADRADO DE LIMITES (ESCALA DE 0 A 100 %)
numCatalogo=1;          %% NUMERO DE INDICE DEL CATALOGO A UTILIZAR
% °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
%% LECTURA DE VARIABLES 
if (exist(fileVar,'file')~=2)
    assert(false,strcat('No existe el archivo "',fileVar,'" en el directorio principal, ejecute primero el programa "AstroLocation_Main.m".'));
    return;
end %if 

fprintf('Cargando variables de %s ... ',fileVar);
AstLoc=load(fileVar);           %% VARIABLE PARA HACER REFERENCIA A LAS VARIABLES LEIDAS
fprintf(' OK \n');

%% NORMALIZANDO ESCALA CUADRADO LIMITES
if (escalaCuadradoLim>100)
    warning('La escala del cuadrado de limites no esta en el rango de 0% al 100%, Normalizando a 100%');
    escalaCuadradoLim=100;
elseif (escalaCuadradoLim<0)
    warning('La escala del cuadrado de limites no esta en el rango de 0% al 100%, Normalizando a 0%');
    escalaCuadradoLim=0;
else 
    fprintf('\t\t*** ESCALA CUADRADO LIMITES = %d ***\n', escalaCuadradoLim);
end %% if
%% COMPROBANDO CATALOGO A TRABAJAR
if (numCatalogo>0 && numCatalogo<=length(AstLoc.filesDataStars))
    fprintf('Trabajando con el catalogo de indice %d : "%s" \n', numCatalogo,AstLoc.filesDataStars{numCatalogo})
else 
    fprintf('\n');
    assert(false,'El indice del Catalogo Astronomico no existe, consulte la variable: "numCatalogo".');
    return;
end%% if


%% GRAFICANDO FIGURA
close all, 
figure(100); %% FIGURA
imshow(AstLoc.IMAGENPLACA); %% MUESTRA IMAGEN 
hold on %% HACER MODIFICACIONES  A LA FIGURA
title('Escalado de Cuadrado de Limites')
graficarLimitesImaginarios(AstLoc.ExtFisicos{1},'g');   %% MUESTRA LIMITES (EXTREMOS DE ASTROLOCATION)
plot(AstLoc.centroFis(1),AstLoc.centroFis(2),'pg');     %% MOSTRAR CENTRO DE DATOS
text(AstLoc.centroFis(1),AstLoc.centroFis(2),'CENTER','Color','g');     %% MOSTRAR CENTRO DE DATOS
graficarPuntos(AstLoc.XYFisicos,' ', '*b'); %% GRAFICA EN FIGURA
graficarPuntos(AstLoc.XYDigitales{numCatalogo},' ', '*r'); %% GRAFICA EN FIGURA

%% ESCALADO CUADRADO LIMITES
nuevosExtFis=AstLoc.ExtFisicos{1}*(escalaCuadradoLim/100);  %% NUEVOS EXTREMOS FISICOS DESPUES DE ESCALAR
[nuevosExtFis,~]=desplazarPuntosCentroFisico(nuevosExtFis,AstLoc.centroFis, obtenerCentroConjPuntos(nuevosExtFis)); %% DEZPLAZAR TODOS LOS PUNTOS DE LA MATRIZ 
graficarLimitesImaginarios(nuevosExtFis,'m');   %% MUESTRA LIMITES (EXTREMOS DE ASTROLOCATION)
plot(AstLoc.centroFis(1),AstLoc.centroFis(2),'pm');     %% MOSTRAR CENTRO DE DATOS
text(AstLoc.centroFis(1),AstLoc.centroFis(2),'CEN','Color','m');     %% MOSTRAR CENTRO DE DATOS

fprintf('\nALTERANDO LA FIGURA CON LOS NUEVOS RESULTADOS (DISCRIMINACION DE LOS PUNTOS QUE ESTAN DENTRO DEL CUADRADO DE LIMITES)... ');
 %% ALTERANDO LA FIGRA CON LOS NUEVOS RESULTADOS (DISCRIMINACION DE LOS PUNTOS QUE ESTAN DENTRO DEL CUADRADO DE LIMITES)
title('DISCRIMINACION DE PUNTOS BASANDOSE EN LOS PUNTOS FISICOS');
graficarLimitesImaginarios(AstLoc.ExtFisicos{1},'g');   %% MUESTRA LIMITES (EXTREMOS DE ASTROLOCATION)
plot(AstLoc.centroFis(1),AstLoc.centroFis(2),'pg');     %% MOSTRAR CENTRO DE DATOS
text(AstLoc.centroFis(1),AstLoc.centroFis(2),'CENTER','Color','g');     %% MOSTRAR CENTRO DE DATOS
graficarPuntos(AstLoc.XYFisicos,' ', '*k'); %% GRAFICA EN FIGURA 1
graficarPuntos(AstLoc.XYDigitales{numCatalogo},' ', '*k'); %% GRAFICA EN FIGURA

[XYFisicosReducido,XYDigitalesReducido, newXYEstandar] = discriminarPuntosRectanguloLimites(AstLoc.XYFisicos,AstLoc.XYDigitales{numCatalogo},AstLoc.XYEstandar{numCatalogo}, nuevosExtFis);

graficarPuntos(XYFisicosReducido,' ', '*b'); %% GRAFICA EN FIGURA 1
graficarPuntos(XYDigitalesReducido,' ', '*r'); %% GRAFICA EN FIGURA

[tam1,~]=size(XYDigitalesReducido);
[tam2,~]=size(AstLoc.XYDigitales{numCatalogo});

fprintf('%d de %d  ... ',tam1,tam2);
title(strcat('DISCRIMINACION DE PUNTOS BASANDOSE EN LOS PUNTOS FISICOS (',num2str(tam1),' estrellas de ',num2str(tam2),')'));


fprintf(' OK \n');

[newXYDigitales,fEsca,fDesp, ErrorProm]=AstroLocation_Function(XYFisicosReducido,XYDigitalesReducido,AstLoc.IMAGENPLACA, true, 100,false, false); %% Escalar y mover los puntos
% % [XYDigitales,fEsca,fDesp, ErrorProm]=AstroLocation_Function(newXYFisicos,newXYEstandar,AstLoc.IMAGENPLACA, true, 100, true, true)
%% MOSTRAR ERROR
ErrorDigitalesRedu=zeros(tam1,2);
ErrorNewDigitales=zeros(tam1,2);

fprintf('\n\t Error en posiciones respecto a las Posiciones fisicas de %s :\n',AstLoc.filesDataStars{numCatalogo});
for ii=1:tam1
    fprintf('--------------------------- Estrella %3.0f : ---------------------------\n',XYFisicosReducido(ii,3));
    fprintf('XFisRed=%5.3f XDigRed=%5.3f \t YFisRed=%5.3f YDigRed=%5.3f\n',XYFisicosReducido(ii,1),XYDigitalesReducido(ii,1),XYFisicosReducido(ii,2),XYDigitalesReducido(ii,2));
    fprintf('XFisRed=%5.3f XNewDig=%5.3f \t YFisRed=%5.3f YNewDig=%5.3f\n',XYFisicosReducido(ii,1),newXYDigitales(ii,1),XYFisicosReducido(ii,2),newXYDigitales(ii,2));
    
    ErrorDigitalesRedu(ii,1)=abs(XYFisicosReducido(ii,1)-XYDigitalesReducido(ii,1));
    ErrorDigitalesRedu(ii,2)=abs(XYFisicosReducido(ii,2)-XYDigitalesReducido(ii,2));
    
    ErrorNewDigitales(ii,1)=abs(XYFisicosReducido(ii,1)-newXYDigitales(ii,1));
    ErrorNewDigitales(ii,2)=abs(XYFisicosReducido(ii,2)-newXYDigitales(ii,2));
    
    fprintf('\n');
end % for
%     fprintf('\n');
promErrorDigitalesRedu=mean(ErrorDigitalesRedu);
promErrorNewDigitales=mean(ErrorNewDigitales);
fprintf('\nError promedio de las %d estrellas ANTES de ejecutar el programa: ',tam1);
fprintf('X= %5.3f , Y= %5.3f \n',promErrorDigitalesRedu(1),promErrorDigitalesRedu(2));

fprintf('\nError promedio de las %d estrellas DESPUES de ejecutar el programa: ',tam1);
fprintf('X= %5.3f , Y= %5.3f \n',promErrorNewDigitales(1),promErrorNewDigitales(2));

fprintf('\n\t *** DIFERENCIA DE ERROR: X= %5.3f , Y=%5.3f ***\n',abs(promErrorDigitalesRedu(1)-promErrorNewDigitales(1)),abs(promErrorDigitalesRedu(2)-promErrorNewDigitales(2)));


%% GRAFICAR ERROR
%{

fprintf('\n\t Error Promedio de %s :\n',AstLoc.filesDataStars{numCatalogo});
fprintf('%3.0f estrellas : X=%5.3f, Y=%5.3f, Numeracion=%3.3f \n',tam1,ErrorProm(1),ErrorProm(2),ErrorProm(3));
fprintf('%3.0f estrellas : X=%5.3f, Y=%5.3f, Numeracion=%3.3f \n',tam2,AstLoc.ErrorProm(1),AstLoc.ErrorProm(2),AstLoc.ErrorProm(3));
fprintf('---Diferencia : X=%5.3f, Y=%5.3f, Numeracion=%3.3f \n',abs(AstLoc.ErrorProm(1)-ErrorProm(1)),abs(AstLoc.ErrorProm(2)-ErrorProm(2)),abs(AstLoc.ErrorProm(3)-ErrorProm(3)));

arrColor=['k','m','c'];
figure(1000);
title(strcat('Representacion de puntos: *',arrColor(1),': P. Fisicos *',arrColor(2),': P. DigReducidos *',arrColor(3),': P. NuevosDig'));
hold on;
graficarPuntos(XYFisicosReducido,' ', strcat('p',arrColor(1))); %% GRAFICA PUNTOS FISICOS
graficarPuntos(XYDigitalesReducido,' ', strcat('*',arrColor(2))); %% GRAFICA PUNTOS DIGITALES REDUCIDOS
graficarPuntos(newXYDigitales,' ', strcat('*',arrColor(3))); %% GRAFICA NUEVOS PUNTOS DIGITALES
%}



%% TIME CONTROLLER
tiempo=toc; %% FIN DE MEDICION DE TIEMPO DE EJECUCION
if (tiempo<=60) %% PARA LOS SEGUNDOS
    fprintf('\n\nTIEMPO DE EJECUCION... %3.3f segundos.\n', tiempo); %% MOSTRANDO TIEMPO DE EJECUCION
else %% PARA LOS MINUTOS
    fprintf('\n\nTIEMPO DE EJECUCION... %3.3f minutos.\n', tiempo/60); %% MOSTRANDO TIEMPO DE EJECUCION
end %% else
disp(datestr(now));