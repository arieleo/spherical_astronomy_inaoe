function [newMat]=comprobarNumerosMatriz(Mat)
disp(Mat)
[f,c]=size(Mat);

cont=1;
for ii=1:f
    if sum(isnan(Mat(ii,1:end))) == 0
        for jj=1:c
            newMat(cont,jj)=Mat(ii,jj);
        end %% for
    cont=cont+1;
    end %% if
        
end %% for filas



end %% function 