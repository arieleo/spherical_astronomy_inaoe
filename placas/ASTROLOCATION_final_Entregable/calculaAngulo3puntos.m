function [angulo,distancias]=calculaAngulo3puntos(p0,p1,p2)
%^   RAIZCUADRADA((Y2-Y1)^2 + (X2-X1)^2) 
X=1;
Y=2;
% Formulas Distacia entre dos puntos
POP1=sqrt((p1(Y)-p0(Y))^2 + (p1(X)-p0(X))^2);
P0P2=sqrt((p2(Y)-p0(Y))^2 + (p2(X)-p0(X))^2);
P1P2=sqrt((p2(Y)-p1(Y))^2 + (p2(X)-p1(X))^2);

%Ley de cosenos
% c2 = a2 + b2 – 2abcos C.
c=P1P2;
a=POP1;
b=P0P2;

distancias=[a,b,c];
angulo=acosd(((a^2)+(b^2)-(c^2))/(2*a*b));
end