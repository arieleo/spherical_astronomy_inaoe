function []=graficarLimitesImaginarios(ExtremosFisicos,color)
    minX=ExtremosFisicos(1,1);  %% Obtiene valor minimo de X
    maxX=ExtremosFisicos(2,1);  %% Obtiene valor maximo de X
    minY=ExtremosFisicos(3,2);  %% Obtiene valor minimo de Y
    maxY=ExtremosFisicos(4,2);  %% Obtiene valor maximo de Y
    
    plot([minX,minX,maxX,maxX,minX],[minY,maxY,maxY,minY,minY],strcat('--',color));
    
end % function 