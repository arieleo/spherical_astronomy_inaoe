function [XZCartesianas] = converCEcuato2CXYZ(ArDec,band3D)
[f,c]=size(ArDec);
XZCartesianas=zeros(f,c);
for ii=1:f
    eti=ArDec(ii,3);
    x=coseno(ArDec(ii,2))*coseno(ArDec(ii,1));
    y=coseno(ArDec(ii,2))*seno(ArDec(ii,1));
    z=seno(ArDec(ii,2));
    
    if (band3D==true)
        XZCartesianas(ii,1)=x; %% VALOR DE X
        XZCartesianas(ii,2)=y; %% VALOR DE Y
        XZCartesianas(ii,3)=z; %% VALOR DE Y
        XZCartesianas(ii,4)=eti; %% ETIQUETA
    else
        XZCartesianas(ii,1)=x; %% VALOR DE X
        XZCartesianas(ii,2)=z; %% VALOR DE Y
        XZCartesianas(ii,3)=eti; %% ETIQUETA
    end
end %% for

end %% function


function [val]=coseno(v)
val=cosd(v);
end % coseno

function [val]=seno(v)
val=sind(v);
end % seno

function [val]=tang(v)
val=tand(v);
end % tangente

function [val]=atang(v)
val=atand(v);
end % tangente inversa

function [val]=cotang(v)
val=cotd(v);
end % cotangente

function [val]=acotang(v)
val=acotd(v);
end % cotangente inversa

