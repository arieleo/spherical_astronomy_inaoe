%%*****************************************************************************************
% Funcion para escalar los puntos convertidos a coor. cartesianas
% respecto a los puntos fisicos (placa digitalizada) utilizando los
% extremos (minX-maxX) (minY-maxY).
% 
% Developer: 
% Date:  Mayo 2016
% inputs: 
%           XY:                 Matriz con datos con numero de estrella.
%           cad:                Cadena de texto en cada punto
%           color:              Cadena indicando el color
%************************************************************************************

function [] = graficarPuntos(XY,cad,color)

[f,~]=size(XY);

% display(XY);

for i=1:f
    x=XY(i,1);
    y=XY(i,2);
    numEstrella=XY(i,3);
    
    plot(x,y,color);
    if (numEstrella==0)
        color2='r';
        plot(x,y,color2);
        text(x,y,strcat(num2str(numEstrella),' CENTRO',' ',cad),'Color',color2);
    else
        text(x,y,strcat(num2str(numEstrella),' ',cad));
    end
    
end %for
axis square;
end %function