%%*****************************************************************************************
% Funcion para obtener los datos de una estrella especifica
% 
% Developer: Luis González Guzmán
% Date:  Mayo 2016
% 
% inputs:   XY:         Matruz de datos estrella (X, Y, NumEstrella).
%           numEst:     Numero de estrella para buscar y obtener los datos.
% outputs:  Estrella:   Datos estrella de interes (X, Y, NumEstrella).
%****************************************************************************************
function [estrella]=buscaEstrellaMat(XY,numEst)
estrella=NaN;
% bandConsec=false;
% [f,c]=size(XY);
% try
%     mt=1:f;
%     mt=mt';
%     mt2=XY(1:end,3);
%     if (mt==mt2)
%         bandConsec=true;
%     end % if
%     
% catch
%     bandConsec=false;
% end
% 
% 
% 
% %% VERIFICANDO TAMAÑO DE LA MATRIZ
% if(c==3)
%     if (bandConsec==false)
%         for i=1:f
%             if (numEst==XY(i,3))
%                 estrella=XY(i,1:3);
%                 break;
%             end %if
%         end %for
%     else 
%         % Los datos son cosecutivos 
% %         disp('consecutivos')
%         estrella=XY(numEst,1:3);
%     end % if 
% else
%     warning('ERROR: LOS TAMAÑOS NO COINCIDEN CON LA BUSQUEDA!!')
% end % if 

estrella=NaN;
[f,c]=size(XY);
if c==3
    for ii=1:f
        if (XY(ii,3)==numEst)
            estrella=XY(ii,1:3);
        end
    end
end

end %function
