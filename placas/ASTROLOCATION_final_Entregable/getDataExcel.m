function [ar,dec,numEst]=getDataExcel(archivoPunDig,hoja,inicioFila,nAR,nDEC, nEti)
% if (exist(strcat(archivoPunDig,'.xls'),'file'))
%     [~, ~, alldata] = xlsread(strcat(archivoPunDig,'.xls'),hoja);
% else
%     [~, ~, alldata] = xlsread(strcat(archivoPunDig,'.xlsx'),hoja);
% end
    [~, ~, alldata] = xlsread(strcat(archivoPunDig,''),hoja);


numEst=sinCeldas(alldata(inicioFila:end,nEti)); %columna 1
ar=sinCeldas(alldata(inicioFila:end,nAR)); %columna 9
dec=sinCeldas(alldata(inicioFila:end,nDEC));%columna 10

end


function [ndata] = sinCeldas(alldata)
[f,c]=size(alldata);
ndata=zeros(f,c);

for ii=1:f
    ndata(ii) = alldata{ii};
end %for

end %% funcion