%%*****************************************************************************************
% Funcion para ordenar una matriz de 3 dimensiones
% respecto a una columna (X, Y o Z), retorna la 
% matriz ordenada
% 
% Developer: 
% Date:  Mayo 2016
% inputs: 
%           B:	Matriz a ordenar
%           c:	Columna por la cual se ordenara (X,Y o Z)
% outputs:
%           E:  Matriz ordenada respecto 'c'.
%************************************************************************************

function [ E ] = ordenarMat3d( B, c )

x=B(:,1);
y=B(:,2);
z=B(:,3);
if (c=='y'||c=='Y')
    C = [y(:),x(:),z(:)];
end
if (c=='x'||c=='X')
    C = [x(:),y(:),z(:)];    
end
if (c=='z'||c=='Z')
    C = [z(:),x(:),y(:)];
end

D = sortrows(C);

x=D(:,1);
y=D(:,2);
z=D(:,3);
    
if (c=='y'||c=='Y')
    E = [y(:),x(:),z(:)];
end
if (c=='x'||c=='X')     
    E = [x(:),y(:),z(:)];
end    
if (c=='z'||c=='Z')         
    E = [y(:),z(:),x(:)];
end

% disp(E)

end

% A = [6,1;8,15;2,6;2,9;8,3;10,5;4,4;7,6;5,15;7,11;6,8;12,5;3,7;1,8;12,5;9,2;10,3;11,4;12,5;11,13;11,8;14,7;15,5;10,4;11,6;2,1]

% A = [6,1,2;8,15,5;2,6,8;2,9,0;8,3,3;10,5,6;4,4,7;7,6,6;5,15,1;7,11,6;6,8,9;12,5,5;3,7,1;1,8,2;12,5,4;9,2,8;10,3,3;11,4,4;12,5,1;11,13,0;11,8,8;14,7,3;15,5,1;10,4,2;11,6,5;2,1,3]