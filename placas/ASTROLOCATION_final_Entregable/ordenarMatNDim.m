%%*****************************************************************************************
% Funcion para ordenar una matriz de 3 dimensiones
% respecto a una columna (X, Y o Z), retorna la 
% matriz ordenada
% 
% Developer: 
% Date:  Mayo 2016
% inputs: 
%           Mat:	Matriz a ordenar
%           c:	Columna por la cual se ordenara (X,Y o Z)
% outputs:
%           E:  Matriz ordenada respecto 'c'.
%************************************************************************************

function [ MOrdenada ] = ordenarMatNDim( Mat, numCol )
%% OBTENIENDO TAMAÑO DE LA MATRIZ
[~,col]=size(Mat);

%% VERIFICANDO SI COINCIDE LA COLUMNA A ORDENAR CON EL TAMAÑO DE LA MATRIZ
if (numCol>col) %% Error
    assert(false,'Indice de la matriz incorrecto, debe ser menor o igual al tamaño de columnas de la matrix.');
    return;
        
end %% if


%% GENERANDO CELDAS PARA ALMACENAR LAS COLUMNAS INDEPENDIENTEMENTE 
MatCols=cell(col,1);

for ii=1:col
    MatCols{ii}=Mat(:,ii);
end %% for

% % x=Mat(:,1);
% % y=Mat(:,2);
% % z=Mat(:,3);

%% PRE-ORDENADO
if (numCol==1) %% SI SE DESEA ORDENAR POR EL PRIMER ELEMENTO (LA MATRIZ SE QUEDA IGUAL)
    MM=MatCols{1};
    
    for ii=1+1:col
        MM=[MM,MatCols{ii}];        
    end %%for 
    
elseif (numCol==col) %% SI SE DESEA ORDENAR POR EL ULTIMO ELEMENTO 
    MM=MatCols{col};
    
    for ii=1:col-1
        MM=[MM,MatCols{ii}];        
    end %%for 
    
else %% POR CUALQUIER POSICION VALIDA (si el num de col. es 4 (xyzw) y el nOrdenamiento es 3 entonces zxyw)
    MM=MatCols{numCol};
    
    for ii=1:numCol-1
        MM=[MM,MatCols{ii}];
    end %% for
    
    for ii=numCol+1:col
        MM=[MM,MatCols{ii}];
    end %% for
    
end %% if
    
% % if (c=='x'||c=='X')
% %     MM = [x(:),y(:),z(:)];    
% % end
% % if (c=='y'||c=='Y')
% %     MM = [y(:),x(:),z(:)];
% % end
% % if (c=='z'||c=='Z')
% %     MM = [z(:),x(:),y(:)];
% % end

%% ORDENAMIENTO
MatOr = sortrows(MM);

%% POS-ORDENAMIENTO

MatCols=cell(col,1);

for ii=1:col
    MatCols{ii}=MatOr(:,ii);
end %% for

% % A=MatOr(:,1);
% % B=MatOr(:,2);
% % C=MatOr(:,3);

%% REACOMODO
if (numCol==1) %%  PRIMER ELEMENTO (LA MATRIZ SE QUEDA IGUAL)
    filaMovida=MatCols{1};
    MOrdenada=filaMovida;
    nnn=1;
    for ii=nnn+1:col
        MOrdenada=[MOrdenada,MatCols{ii}];        
    end %%for 
    
elseif (numCol==col) %% ULTIMO ELEMENTO  MOrdenada = [A(:),B(:),C(:)];
    filaMovida=MatCols{1};
    nnn=2;
    MOrdenada=MatCols{nnn};%% primer elemento MOrdenada = [B(:),C(:),A(:)];
    
    for ii=nnn+1:col
        MOrdenada=[MOrdenada,MatCols{ii}];        
    end %%for 
    MOrdenada=[MOrdenada,filaMovida];
    
else %% POR CUALQUIER POSICION VALIDA (si el num de col. es 4 (xyzw) y el nOrdenamiento es 3 entonces zxyw) %     MOrdenada = [B(:),A(:),C(:)];
    filaMovida=MatCols{1};
    
    MOrdenada=MatCols{2}; %% primer elemento
    cont=2;
    for ii=3:col
        if (cont==numCol)
            MOrdenada=[MOrdenada,filaMovida];
%             disp('movida')
        end
        MOrdenada=[MOrdenada,MatCols{ii}];        
        cont=cont+1;
    end %% for
%     display(MOrdenada)
end %% if



% if (c=='x'||c=='X')     
%     MOrdenada = [A(:),B(:),C(:)];
% end    
% if (c=='y'||c=='Y')
%     MOrdenada = [B(:),A(:),C(:)];
% end
% if (c=='z'||c=='Z')         
%     MOrdenada = [B(:),C(:),A(:)];
% end

% disp(MOrdenada)

end %%FUNCTION

% a=[round(rand()*10),round(rand()*10),round(rand()*10);round(rand()*10),round(rand()*10),round(rand()*10);round(rand()*10),round(rand()*10),round(rand()*10);round(rand()*10),round(rand()*10),round(rand()*10);round(rand()*10),round(rand()*10),round(rand()*10);round(rand()*10),round(rand()*10),round(rand()*10);round(rand()*10),round(rand()*10),round(rand()*10);round(rand()*10),round(rand()*10),round(rand()*10);round(rand()*10),round(rand()*10),round(rand()*10);round(rand()*10),round(rand()*10),round(rand()*10);round(rand()*10),round(rand()*10),round(rand()*10);]

% A = [6,1;8,15;2,6;2,9;8,3;10,5;4,4;7,6;5,15;7,11;6,8;12,5;3,7;1,8;12,5;9,2;10,3;11,4;12,5;11,13;11,8;14,7;15,5;10,4;11,6;2,1]

% A = [6,1,2;8,15,5;2,6,8;2,9,0;8,3,3;10,5,6;4,4,7;7,6,6;5,15,1;7,11,6;6,8,9;12,5,5;3,7,1;1,8,2;12,5,4;9,2,8;10,3,3;11,4,4;12,5,1;11,13,0;11,8,8;14,7,3;15,5,1;10,4,2;11,6,5;2,1,3]