%%*****************************************************************************************
% Funcion para obtener los extremos fisicos (estrellas mas alejadas en los ejes X y Y).
% 
% Developer: 
% Date:  Mayo 2016
% inputs: 
%           XYN:                        Matriz con datos con numero de estrella.
%           centroImg:                  Centro de la imagen (x,y).
% outputs:
%           nXY:                        Matriz con los puntos escalados.
%           fDesplazamiento:            Factor desplazamiento.
%******************************************************************************************

function [ExtremosFisicos]=obtenerExtremosFisicosYDigitales(XYFisicos,XYDigitales,bandMedFis)
% % bandMedFis=true; %% TRABAJAR CON LOS MINIMOS Y MAXIMOS DE LAS MEDICIONES FISICAS
%% OBTENIENDO LOS EXTREMOS DIGITALES <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<!!!!!!!!!!!!!!!!!!!
oX=ordenarMat3d(XYDigitales,'X');
oY=ordenarMat3d(XYDigitales,'Y');
nExtDigitales=[oX(1,1:3);oX(end,1:3);oY(1,1:3);oY(end,1:3)];

%% OBTENIENDO LOS EXTREMOS FISICOS <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<!!!!!!!!!!!!!!!!!!!
oX=ordenarMat3d(XYFisicos,'X');
oY=ordenarMat3d(XYFisicos,'Y');
nExtFisicos=[oX(1,1:3);oX(end,1:3);oY(1,1:3);oY(end,1:3)];

fprintf('---------------------------------------------------------------\n');
fprintf('Extremos digitales: \n');
disp(nExtDigitales);

fprintf('Extremos fisicos: \n');
disp(nExtFisicos);

bandEstremos=true;
if(nExtFisicos~=nExtDigitales)
    warning('Los Extremos NO coinciden.... FAIL');
    disp('Utilizando los extremos digitales para encontrar los extremos fisicos.')
    bandEstremos=false;
else
    disp('Los Extremos coinciden.... OK');
end
fprintf('---------------------------------------------------------------\n');

%% BUSQUEDA DE EXTREMOS
% Los datos mas confiables son los del catlogo, asi que consideraremos los
% extermos de los puntos digitales para extraerlos de los puntos fisicos
if bandEstremos==true || bandMedFis==true
    ExtremosFisicos=nExtFisicos;
else 
    %% Utilizando los extremos digitales para encontrar los extremos fisicos
    ExtremosFisicos=buscaEstrellaMat(XYFisicos,nExtDigitales(1,3));%% Minimo en X
    ExtremosFisicos=[ExtremosFisicos; buscaEstrellaMat(XYFisicos,nExtDigitales(2,3))];%% Maximo en X
    ExtremosFisicos=[ExtremosFisicos; buscaEstrellaMat(XYFisicos,nExtDigitales(3,3))];%% Minimo en X
    ExtremosFisicos=[ExtremosFisicos; buscaEstrellaMat(XYFisicos,nExtDigitales(4,3))];%% Maximo en X
end


end %function
