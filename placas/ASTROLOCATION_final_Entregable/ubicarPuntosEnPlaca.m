%%*****************************************************************************************
% Funcion para ubicar puntos en una placa astro-fotografica
% a partir de coordenadas ecuatoriales, realizando escalamiento
% rotacion y traslacion.
% 
% Developer: Luis González Guzmán
% Date:  Mayo 2016
% 
% inputs:     puntoTang:          Estrella central
%             archivoPunDig:    Archivo con los datos de las estrellas.
%             imgPath:          Ruta de la imagen.
%             ExtremosFisicos:  Matriz con las extrellas extramas.
%             enImagen:         Bandera para mostrar la imagen en la figura.
%             holdActivo:       Bandera para mantener el hold activo
%             despues de ejecutar la funcion.
%************************************************************************************
function [XYprima, fEscala,fDesplazamiento]=ubicarPuntosEnPlaca(XYEstandar,IMAGENPLACA,ExtremosFisicos,holdActivo,multiFigu, bandEspX, bandEspY)
%% ESCALADO
fprintf('Escalado... ');
[XYprima,fEscala]=escalarPuntosPorExtremosFisicos(XYEstandar,ExtremosFisicos); %% ESCALADO Y OBTENCION DEL FACTOR ESCALA EMPLEADO
fprintf('OK    FACTORES{ fX= %d, fY= %d }\n', fEscala(1),fEscala(2));
%% ESPEJEADO POR Y
if (bandEspY==true)
    fprintf('Espejeado en Y ... ');
    [XYprima]=espejearPuntos(XYprima,'y'); %ESPEJEADO POR Y
    fprintf('OK \n');
end %% if
%% ESPEJEADO POR X
if (bandEspX==true)
    fprintf('Espejeado en X ... ');
    [XYprima]=espejearPuntos(XYprima,'x'); %ESPEJEADO POR X
    fprintf('OK \n');
end %%if
%% DESPLAZAMIENTO
% % % % [L,W,~]=size(IMAGENPLACA);
% % % % disp('++++ CENTRO IMAGEN:')
% % % % puntoCentral=[W/2,L/2]
fprintf('Desplazamiento... ');
puntoCentral=obtenerCentroConjPuntos(ExtremosFisicos);              %% GENERAR EL PUNTO CENTRAL  DE LOS EXTREMOS FISICOS
[XYprima,fDesplazamiento]=desplazarPuntosCentroFisico(XYprima,puntoCentral, obtenerCentroConjPuntos(XYprima)); %% DEZPLAZAR TODOS LOS PUNTOS DE LA MATRIZ 
fprintf('OK    FACTORES{ fX= %d, fY= %d } \tCENTRO{ X= %d, Y= %d }\n', fDesplazamiento(1),fDesplazamiento(2), puntoCentral(1),puntoCentral(2));
%% MOSTRAR PUNTOS ESCALADOS, DESPLAZADOS Y ESPEJEADOS FIGURA 1
figure(1+multiFigu) %% <------------------------------------------------------FIGURA 1
imshow(IMAGENPLACA) %% MOSTRAR IMAGEN (PLACA) DE FONDO
hold on;            %% PERMITIR HACER MODIFICACIONES A LA FIGURA
centroDigi=obtenerCentroConjPuntos(XYprima); %% GENERAR EL PUNTO CENTRAL  DE LOS PUNTOS DIGITALES CALCULADOS
XYprima=[XYprima;centroDigi]; %% AGREGANDO EL CENTRO DE LOS PUNTOS COMO UN NUEVO DATO
graficarPuntos(XYprima,' ','pr');   %% MOSTRAR GRAFICAMENTE LA MATRIZ DE DATOS

%% PINTANDO CENTRO EN LA IMAGEN
    plot([puntoCentral(1),centroDigi(1)],[puntoCentral(2),centroDigi(2)], '--r');
    plot(puntoCentral(1),puntoCentral(2),'k')
    text(puntoCentral(1),puntoCentral(2),'0 CEN','Color','k')
    
    
    
    
%% GRAFICAR EXTREMOS FISICOS
graficarPuntos(ExtremosFisicos,' ','pg');
%% GENERANDO LIMITES IMAGINARIOS
graficarLimitesImaginarios(ExtremosFisicos,'g');
%% DEJAR EL HOLD DE LA FIGURA 1 ACTIVO???
if holdActivo == false
    hold off;
end % if
    
    
end % Function
