function [XYDat] = eliminaPosicionMatriz(XYDat, arrPosEliminar)
[f,~]=size(XYDat);  %% MATRIZ DE N FILAS Y 3 COLUMNAS (X, Y, ETIQUETA)
numEli=length(arrPosEliminar);
for jj=1:numEli
    for ii=1:f
        if (XYDat(ii,3)==arrPosEliminar(jj)) %% SI LA ETIQUETA ES IGUAL A LA VARIABLE "arrPosEliminar": BORRA LA FILA ENTERA
            if (ii==1) && (ii+1<=f) %% SI EN LA PRIMERA POSICION A ELIMINAR ESTA EN LA PRIMERA FILA
                XYDat=XYDat(ii+1:end, 1:3);
                break;
            elseif (ii==f && ii-1>=1) %% SI EN LA ULTIMA POSICION A ELIMINAR ESTA EN LA PRIMERA FILA
                XYDat=XYDat(1:ii-1, 1:3);
                break;
            else                %% SI EL CUALQUIER POSICION A ELIMINAR ESTA EN LA PRIMERA FILA
                aux1=XYDat(1:ii-1, 1:3);
                aux2=XYDat(ii+1:end, 1:3);
                XYDat=[aux1;aux2];
                break;
            end %% if
        end % if
    end %for
end %for

end %function